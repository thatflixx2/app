﻿1
00:00:01,326 --> 00:00:05,326
<font color="#00FF00">♪ The Simpsons 26x22 ♪</font>
<font color="#00FFFF">Mathlete's Feat</font>
Original Air Date on May 17, 2015

2
00:00:05,487 --> 00:00:06,687
(grunts)

3
00:00:08,657 --> 00:00:11,158
D'oh!

4
00:00:11,193 --> 00:00:12,826
(grunts)

5
00:00:13,862 --> 00:00:15,128
(all scream)

6
00:00:15,164 --> 00:00:16,630
Oh, my God, Morty,
what did you do?

7
00:00:16,665 --> 00:00:18,365
You killed the Simpsons, Morty!
Oh, my God, no!

8
00:00:18,400 --> 00:00:19,800
No, I-I-I-I didn't mean to!
Oh, no, no!

9
00:00:19,835 --> 00:00:21,301
This is horrible!
I killed the Simpsons!

10
00:00:21,337 --> 00:00:23,870
God, look at the baby one!
Oh, my God, Morty!

11
00:00:23,906 --> 00:00:25,405
You killed the entire
Simpsons, Morty!

12
00:00:25,441 --> 00:00:27,708
They're a beloved
(belches) family, Morty!

13
00:00:27,743 --> 00:00:29,609
They're-they're-they're-they're
a national treasure.

14
00:00:29,645 --> 00:00:31,211
And you killed them.
I-I-I-I'm just a kid!

15
00:00:31,246 --> 00:00:32,245
I'm just a kid!
I don't want to go to jail!

16
00:00:32,281 --> 00:00:33,281
Relax, Morty, calm down.

17
00:00:33,315 --> 00:00:35,415
We'll take care of it.

18
00:00:35,451 --> 00:00:37,150
Okay, I want you to take
that vial of Simpsons' goo

19
00:00:37,186 --> 00:00:39,086
and this picture
to this address.

20
00:00:39,121 --> 00:00:41,321
They'll make us new Simpsons--
you understand me, Morty?

21
00:00:41,323 --> 00:00:43,256
Me?! W-W-What are you gonna do?
Morty,

22
00:00:43,292 --> 00:00:44,558
I got to clean this place up

23
00:00:44,593 --> 00:00:45,926
before somebody comes
snooping around.

24
00:00:45,961 --> 00:00:47,561
You know how many
characters there are

25
00:00:47,596 --> 00:00:48,962
in<i> The Simpsons,</i> Morty?
There's, like, a billion

26
00:00:48,998 --> 00:00:50,664
(belches):
chara... characters.

27
00:00:50,699 --> 00:00:52,632
They did an episode where
George Bush was their neighbor.

28
00:00:52,668 --> 00:00:54,668
All right, can't
argue with that.

29
00:01:02,578 --> 00:01:04,578
(spits)

30
00:01:09,818 --> 00:01:11,818
(speaking in native tongue)

31
00:01:38,213 --> 00:01:39,646
Hi-diddly-ho, neighbor!

32
00:01:39,681 --> 00:01:41,882
Looks like you got
a spaceship in your...

33
00:01:41,917 --> 00:01:44,351
Who are you?

34
00:01:51,026 --> 00:01:52,993
Rick, I'm back!
Wake up! Wake up!

35
00:01:53,028 --> 00:01:54,961
Geez, it's about time, Morty.

36
00:01:54,997 --> 00:01:56,997
Give me those.

37
00:02:00,702 --> 00:02:03,036
(grunting, moaning)

38
00:02:05,908 --> 00:02:07,040
Huh, wow.

39
00:02:07,076 --> 00:02:09,176
Hey, Morty, a little tip.
(belches)

40
00:02:09,211 --> 00:02:11,244
Don't clean DNA vials
with your spit!

41
00:02:11,280 --> 00:02:13,547
Let's go.
I'm driving this time.

42
00:02:15,584 --> 00:02:18,852
Aw, no more guest
animators, man!

43
00:02:27,563 --> 00:02:28,728
Okay, children.

44
00:02:28,764 --> 00:02:29,996
I think it's safe now

45
00:02:30,032 --> 00:02:32,432
to put on your math T-shirts.

46
00:02:33,969 --> 00:02:35,602
Wait here.

47
00:02:35,637 --> 00:02:36,903
(grunts)
Nerd!

48
00:02:36,939 --> 00:02:39,406
Model U.N.,
and step on it!

49
00:02:39,441 --> 00:02:40,707
Ha-ha!

50
00:02:40,742 --> 00:02:43,110
(laughing)

51
00:02:44,980 --> 00:02:46,713
Very good.

52
00:02:46,748 --> 00:02:49,516
That is really... Oh!

53
00:02:49,551 --> 00:02:51,518
Dog in box.

54
00:02:51,553 --> 00:02:53,920
Genius.

55
00:02:57,960 --> 00:03:00,794
All right, let's meet our two
teams of Mathletes, shall we?

56
00:03:00,829 --> 00:03:04,264
First, the Springfield
Elementary Action Fractions!

57
00:03:04,299 --> 00:03:05,799
They rhyme and they...

58
00:03:05,834 --> 00:03:06,967
(groans)

59
00:03:07,002 --> 00:03:09,136
And, uh, from the right side
of the tracks,

60
00:03:09,171 --> 00:03:11,605
the Waverly Hills Elementary

61
00:03:11,640 --> 00:03:13,807
No Equals.
They're spoiled and rich.

62
00:03:13,842 --> 00:03:17,310
Yo! What did the right angle
say to the wider angle?

63
00:03:17,346 --> 00:03:19,980
Well, not knowing these fellows,
I-I couldn't say.

64
00:03:20,015 --> 00:03:21,081
You're obtuse!

65
00:03:21,116 --> 00:03:22,582
(all laughing)

66
00:03:22,618 --> 00:03:24,317
(groans)

67
00:03:24,353 --> 00:03:26,553
Now, let's welcome
our celebrity guests,

68
00:03:26,588 --> 00:03:29,055
former winners
of this very contest

69
00:03:29,091 --> 00:03:31,158
and current high-flying--
yes, you heard me--

70
00:03:31,193 --> 00:03:32,993
I said high-flying,
high flying--

71
00:03:33,028 --> 00:03:35,262
tech entrepreneurs!

72
00:03:35,297 --> 00:03:37,230
Gary, Doug and Benjamin,

73
00:03:37,266 --> 00:03:38,965
who invented Whereditgo.

74
00:03:39,001 --> 00:03:41,768
That's an app that finds
the other apps on your phone.

75
00:03:41,803 --> 00:03:45,505
Enables you to glayvin with
your friends' mobile hoyvin.

76
00:03:45,541 --> 00:03:47,807
One year ago, we
were dateless nerds

77
00:03:47,843 --> 00:03:49,643
living in our
mothers' basements.

78
00:03:49,678 --> 00:03:52,979
Now the basement we live in
is in a giant mansion.

79
00:03:53,015 --> 00:03:54,848
Yeah!
Yeah!

80
00:03:54,883 --> 00:03:57,250
And we own some kind
of sports team.

81
00:03:57,286 --> 00:03:58,585
(laughs)

82
00:03:58,620 --> 00:04:00,153
All right, calm down,
just calm down.

83
00:04:00,189 --> 00:04:02,289
Now, here to support
these fine student scholars

84
00:04:02,324 --> 00:04:04,691
is the man who loves math...

85
00:04:04,726 --> 00:04:06,927
Math?!
I thought you said "meth"!

86
00:04:06,962 --> 00:04:08,662
Drug reference!

87
00:04:09,865 --> 00:04:11,798
Uh, seriously,
what am I here for?

88
00:04:11,833 --> 00:04:13,366
Nobody told you?

89
00:04:13,402 --> 00:04:15,001
They send a limo, I get in.

90
00:04:15,037 --> 00:04:16,369
It takes me somewhere.

91
00:04:16,405 --> 00:04:18,605
Then I watch the news later
to see what I did.

92
00:04:18,640 --> 00:04:21,074
(crowd cheers and applauds)

93
00:04:21,109 --> 00:04:22,142
What a nightmare.

94
00:04:22,177 --> 00:04:23,810
That's fair enough.
Boo-hayvin!

95
00:04:23,845 --> 00:04:25,212
The teams have created

96
00:04:25,247 --> 00:04:27,781
introductory videos
about themselves.

97
00:04:27,816 --> 00:04:31,017
Let's watch them,
shall we, with our eyes?

98
00:04:33,088 --> 00:04:35,322
NARRATOR: <i>Springfield Elementary was</i>
<i>originally designed</i>

99
00:04:35,357 --> 00:04:38,291
<i>as a storage facility</i>
<i>for salt pork.</i>

100
00:04:38,327 --> 00:04:40,293
<i>At some later point,</i>
<i>it was turned into a school.</i>

101
00:04:40,329 --> 00:04:41,661
CHALMERS:
<i>Chalmskinn.</i>

102
00:04:41,697 --> 00:04:42,829
On hot days, pork grease

103
00:04:42,864 --> 00:04:44,030
still comes out of the walls.

104
00:04:44,066 --> 00:04:45,999
Oh, quit your whining.

105
00:04:46,034 --> 00:04:47,734
FRINK:
And now, Waverly Hills.

106
00:04:47,769 --> 00:04:51,171
Hi, I'm movie genius
Michael Bay.

107
00:04:51,206 --> 00:04:52,706
I used to be all like, "Math?

108
00:04:52,741 --> 00:04:54,207
Who needs that noise,
am I right?"

109
00:04:54,243 --> 00:04:56,009
But I wasn't right.

110
00:04:56,044 --> 00:04:58,578
The Waverly Hills math team
made me realize

111
00:04:58,614 --> 00:05:01,381
that quadratic equations
are hotter

112
00:05:01,416 --> 00:05:03,316
than a million Megan Foxes.

113
00:05:03,352 --> 00:05:05,752
Math on!

114
00:05:10,125 --> 00:05:11,558
Yo, Waverly Hills!

115
00:05:11,593 --> 00:05:13,059
You guys rock!

116
00:05:13,095 --> 00:05:14,427
Like my friend The Rock!

117
00:05:14,463 --> 00:05:15,895
I know his real first name!

118
00:05:15,931 --> 00:05:16,931
It's Dwayne!

119
00:05:16,965 --> 00:05:19,366
I win Hollywood!

120
00:05:23,939 --> 00:05:25,905
Celebrate now,
you stuck-up snobs.

121
00:05:25,941 --> 00:05:27,540
(cheering)

122
00:05:27,576 --> 00:05:30,777
But our plucky little school is
going to surprise you.

123
00:05:30,812 --> 00:05:33,213
We will surprise you all!

124
00:05:33,248 --> 00:05:35,015
(cheering)
Well, Lisa,

125
00:05:35,050 --> 00:05:36,716
we didn't score
a single point.

126
00:05:36,752 --> 00:05:38,118
That was surprising.

127
00:05:39,421 --> 00:05:40,954
(crying)

128
00:05:40,989 --> 00:05:44,124
Oh, you'll get them
next time, honey.

129
00:05:44,159 --> 00:05:46,359
No! No, we won't!

130
00:05:46,395 --> 00:05:49,029
That school is so rich!

131
00:05:49,064 --> 00:05:52,365
Every kid has a laptop!

132
00:05:52,401 --> 00:05:55,802
Her crying is sadder
than a child actor

133
00:05:55,837 --> 00:05:58,571
"Where are they now?" story.

134
00:05:58,607 --> 00:06:00,674
LISA:
It's true.

135
00:06:01,410 --> 00:06:03,143
As graduates of
Springfield Elementary,

136
00:06:03,178 --> 00:06:04,811
we want to give back.

137
00:06:04,846 --> 00:06:06,913
We're going to buy
every student a tablet computer

138
00:06:06,948 --> 00:06:08,815
and upgrade your entire school

139
00:06:08,850 --> 00:06:11,484
with the latest
cloud-based technology.

140
00:06:11,520 --> 00:06:13,787
Here's a check.
Take this check.

141
00:06:13,822 --> 00:06:15,221
NARRATOR:
The first black president...

142
00:06:15,257 --> 00:06:17,791
is decades away
from being a reality.

143
00:06:19,494 --> 00:06:21,394
Come back
with my fall semester!

144
00:06:21,430 --> 00:06:22,962
We don't need filmstrips

145
00:06:22,998 --> 00:06:24,831
like<i> Life in These</i>
<i>48 States</i> anymore,

146
00:06:24,866 --> 00:06:27,534
because our school is
going all digital!

147
00:06:27,569 --> 00:06:28,569
(grunts)

148
00:06:28,570 --> 00:06:30,103
GROUNDSKEEPER WILLIE:
Ow, damn it!

149
00:06:30,138 --> 00:06:31,905
The teachers union
won't stand for this.

150
00:06:31,940 --> 00:06:33,406
It means less
work for you.

151
00:06:33,442 --> 00:06:35,508
I didn't know it was
possible to do less work.

152
00:06:35,544 --> 00:06:38,845
How intriguing.

153
00:06:38,880 --> 00:06:42,082
♪ He said I've been
to the year 3000 ♪

154
00:06:42,117 --> 00:06:44,084
♪ Not much has changed ♪
♪ Not much has changed ♪

155
00:06:44,119 --> 00:06:47,954
♪ But they lived underwater ♪
♪ Underwater ♪

156
00:06:47,989 --> 00:06:51,958
♪ And your great-great-great
granddaughter ♪

157
00:06:51,993 --> 00:06:53,460
♪ Is pretty fine ♪

158
00:06:53,495 --> 00:06:57,297
♪ Is pretty
fine ♪

159
00:06:57,332 --> 00:06:59,366
♪ I took a trip
to the year 3000 ♪

160
00:06:59,401 --> 00:07:01,334
♪ This song had gone
multiplatinum ♪

161
00:07:01,370 --> 00:07:04,137
♪ Everybody bought
our seventh album... ♪

162
00:07:04,172 --> 00:07:06,206
And if we're ever invaded,
just click this.

163
00:07:06,241 --> 00:07:08,775
♪ I took a trip
to the year 3000 ♪

164
00:07:08,810 --> 00:07:10,844
♪ This song had gone
multiplatinum ♪

165
00:07:10,879 --> 00:07:13,680
♪ Everybody bought
our seventh album. ♪

166
00:07:13,715 --> 00:07:15,181
Oh, it's wonderful.

167
00:07:15,217 --> 00:07:17,917
We can finally afford
attractive teachers.

168
00:07:17,953 --> 00:07:21,855
(groans) I was the only one here
who understood osmosis.

169
00:07:21,890 --> 00:07:24,257
Spare me, ug-o.

170
00:07:24,292 --> 00:07:25,825
(groans)

171
00:07:25,861 --> 00:07:27,794
Willie, since all our
books have been digitized,

172
00:07:27,829 --> 00:07:30,130
we have no need for the
paper versions-- burn these.

173
00:07:30,165 --> 00:07:31,965
Wouldn't it be easier
just to toss them out?

174
00:07:32,000 --> 00:07:34,467
Nonsense! We now have
this state-of-the-art

175
00:07:34,503 --> 00:07:36,903
digital book burner.
Hmm.

176
00:07:39,040 --> 00:07:41,174
(laughs)
Listen to her hum.

177
00:07:41,209 --> 00:07:42,876
What's that?!

178
00:07:42,911 --> 00:07:44,191
Well, we didn't
want to leave you

179
00:07:44,212 --> 00:07:45,478
out of the digital
revolution.

180
00:07:45,514 --> 00:07:46,913
Willie, meet your
new supervisor.

181
00:07:46,948 --> 00:07:50,183
Aw, I have to take orders
from a machine?

182
00:07:50,218 --> 00:07:51,418
Oh, it can't speak.

183
00:07:51,453 --> 00:07:53,052
But should it ever learn, yes.

184
00:07:53,088 --> 00:07:56,389
("Star Spangled Banner" playing)

185
00:07:59,761 --> 00:08:01,060
(groans)
Will there ever be a technology

186
00:08:01,096 --> 00:08:02,395
that teaches stupid children

187
00:08:02,431 --> 00:08:05,765
how to ding a dang triangle?!

188
00:08:05,801 --> 00:08:09,068
This school has spent the last
50 years mired in the 1960s.

189
00:08:09,104 --> 00:08:12,372
Let us spend the next 50
mired in now!

190
00:08:16,411 --> 00:08:18,545
(singsongy):
Bo-Ron!

191
00:08:19,481 --> 00:08:21,047
And with this ring,

192
00:08:21,082 --> 00:08:23,683
we unite these ancient
warring clans,

193
00:08:23,718 --> 00:08:26,352
who frolic unclothed,
even in winter,

194
00:08:26,388 --> 00:08:29,255
and bring peace to Decapita!

195
00:08:30,892 --> 00:08:33,259
(screams)

196
00:08:34,129 --> 00:08:35,895
Pay cable is awesome!

197
00:08:35,931 --> 00:08:38,264
Who knew they had nipples
in castle times?

198
00:08:38,300 --> 00:08:40,366
You're not supposed to be able
to get outside our network!

199
00:08:40,402 --> 00:08:42,735
You shouldn't have made
your password "password."

200
00:08:42,771 --> 00:08:44,304
Well, it was the name of
the street I grew up on.

201
00:08:44,339 --> 00:08:45,672
Password Drive!

202
00:08:47,642 --> 00:08:49,242
(crackling and buzzing)

203
00:08:52,013 --> 00:08:53,746
Seymour, you plugged
the servers in

204
00:08:53,782 --> 00:08:55,315
with surge protectors,
didn't you?

205
00:08:55,350 --> 00:08:56,716
Oh, yes, power strips.

206
00:08:56,751 --> 00:08:57,951
You fool.

207
00:08:57,986 --> 00:09:00,386
Surge protectors are
always power strips.

208
00:09:00,422 --> 00:09:01,788
But not vice versa!

209
00:09:08,864 --> 00:09:10,396
Was that us?

210
00:09:10,432 --> 00:09:11,664
No, sir.

211
00:09:11,700 --> 00:09:13,666
(chuckles)
I like it when it's not us.

212
00:09:20,384 --> 00:09:22,050
(clock ticking)

213
00:09:31,461 --> 00:09:32,461
Let's see.

214
00:09:32,497 --> 00:09:33,863
Roman numerals.

215
00:09:33,898 --> 00:09:35,197
Photosynthesis.

216
00:09:35,233 --> 00:09:37,266
Uh, Robert E. Lee.

217
00:09:37,301 --> 00:09:39,668
Miss Hoover, are you teaching
or are you just saying

218
00:09:39,704 --> 00:09:42,004
anything that comes
into your head?

219
00:09:42,039 --> 00:09:44,440
Miss Hoover, please report
to the main office.

220
00:09:44,475 --> 00:09:46,642
You just did that in your hand!

221
00:09:46,677 --> 00:09:48,511
Ralph Wiggum,
put your head down.

222
00:09:48,546 --> 00:09:50,780
You're the mouth-hand!

223
00:09:50,815 --> 00:09:52,848
All right, class.
We don't have computers

224
00:09:52,884 --> 00:09:54,683
and we don't have keyboards.

225
00:09:54,719 --> 00:09:58,287
But that doesn't mean
we can't practice our typing.

226
00:09:58,322 --> 00:09:59,722
Now, put your index fingers

227
00:09:59,757 --> 00:10:02,892
on marshmallows F and J, and...

228
00:10:02,927 --> 00:10:04,193
tap-tap-tap.

229
00:10:04,228 --> 00:10:05,528
Tap-tap-tap.

230
00:10:05,563 --> 00:10:07,797
Tap-tap...
Nelson, stop that!

231
00:10:07,832 --> 00:10:08,864
This is my dinner.

232
00:10:08,900 --> 00:10:10,366
I'm eating steak.

233
00:10:11,602 --> 00:10:13,035
Mmm! Mmm!

234
00:10:13,070 --> 00:10:14,670
Needs sauce.

235
00:10:18,843 --> 00:10:21,410
Great news, children.
I found an educational movie

236
00:10:21,446 --> 00:10:23,412
I can play on my phone.

237
00:10:23,448 --> 00:10:25,714
Crowd in--
it's kind of a little phone.

238
00:10:25,750 --> 00:10:27,750
NARRATOR:
Mathematics! Agriculture!

239
00:10:27,785 --> 00:10:29,685
Are there two
more exciting words?

240
00:10:29,720 --> 00:10:31,987
Since the time of the Romans,
they used scythes

241
00:10:32,023 --> 00:10:33,689
to kill each other.

242
00:10:33,724 --> 00:10:36,692
The ancient farmer needed tools
to measure his land.

243
00:10:36,727 --> 00:10:38,561
Ten stick knots right.

244
00:10:38,596 --> 00:10:40,529
Five left.

245
00:10:40,565 --> 00:10:41,697
Hmm.

246
00:10:41,732 --> 00:10:43,332
(humming a tune)

247
00:10:43,367 --> 00:10:44,700
(groans)

248
00:10:44,735 --> 00:10:47,770
Oh, come to mock
ol' Willie, have you?

249
00:10:47,805 --> 00:10:51,540
"Ooh, Willie has to work
in the hot sun all day.

250
00:10:51,576 --> 00:10:55,511
Willie's best friend is
a stick and a string."

251
00:10:55,546 --> 00:10:57,079
I'm not here to mock you.

252
00:10:57,114 --> 00:10:58,914
"Ooh, Willie doesn't know

253
00:10:58,950 --> 00:11:01,684
when someone's being sincere!"

254
00:11:01,719 --> 00:11:02,952
I'm just here to hang out.

255
00:11:02,987 --> 00:11:04,753
What's that device
you're using?

256
00:11:04,789 --> 00:11:06,889
My rummlie scob.
Nothing exciting.

257
00:11:06,924 --> 00:11:10,459
Just a measuring stick dating
back to the ancient druids.

258
00:11:10,495 --> 00:11:13,262
Oh! Could you tell me
how it works?

259
00:11:13,297 --> 00:11:16,365
Each knot marks the length
of a sheep's bladder.

260
00:11:16,400 --> 00:11:20,302
The play field is 75
stomachs by 52 kidneys.

261
00:11:20,338 --> 00:11:24,106
That's 163 square haggises.

262
00:11:24,141 --> 00:11:25,541
Move over, metric system!

263
00:11:25,576 --> 00:11:27,643
I'm learning
the gastric system!

264
00:11:27,678 --> 00:11:29,378
Clever.

265
00:11:29,413 --> 00:11:30,913
Uh, good news, sir,
I have assembled

266
00:11:30,948 --> 00:11:33,782
a page on "the piglims"
at "Rhymouth Pock."

267
00:11:33,818 --> 00:11:36,051
Yes, well, that's
the best we can hope for.

268
00:11:36,087 --> 00:11:38,954
Uh, apply
the transparent tape.

269
00:11:38,990 --> 00:11:41,790
Principal Skin-ner!

270
00:11:41,826 --> 00:11:44,393
Willie has showed me
that losing our technology

271
00:11:44,428 --> 00:11:47,263
doesn't have to be
the end of our learning.

272
00:11:47,298 --> 00:11:50,099
We can turn our school
into a Waldorf school.

273
00:11:50,134 --> 00:11:51,500
You mean like the hotel?

274
00:11:51,536 --> 00:11:53,402
Nope!
In elementary school,

275
00:11:53,437 --> 00:11:55,137
Waldorf education focuses

276
00:11:55,172 --> 00:11:57,840
on hands-on activity
and creative play.

277
00:11:57,875 --> 00:11:59,575
In secondary education...

278
00:11:59,610 --> 00:12:01,243
Ah, not our problem.

279
00:12:01,279 --> 00:12:03,612
After sixth grade, it's
good-bye and good luck.

280
00:12:03,648 --> 00:12:05,481
(both chuckling)
Yes, yes, it is.

281
00:12:05,516 --> 00:12:07,182
Well, it sounds good,
but I have one more question.

282
00:12:07,218 --> 00:12:09,985
Is it based on the book where
you find the guy in the hat?

283
00:12:10,021 --> 00:12:12,454
<i>Where's Waldo?</i>
That's not even the name.

284
00:12:12,490 --> 00:12:15,357
I'm surprised you guys didn't
think it was based on the salad.

285
00:12:15,393 --> 00:12:17,293
BOTH:
There's a Waldorf salad?

286
00:12:17,328 --> 00:12:18,661
(groans)

287
00:12:18,696 --> 00:12:21,430
Behold--
Waldorf education in action!

288
00:12:21,465 --> 00:12:24,600
We're getting our hands dirty
and learning by doing.

289
00:12:24,635 --> 00:12:27,536
So I have to make
360 sloppy Joes,

290
00:12:27,572 --> 00:12:30,439
but I only have one pound
of hamburger meat.

291
00:12:30,474 --> 00:12:34,276
How many cubic feet of Styrofoam
peanuts should I add?

292
00:12:34,312 --> 00:12:36,111
Assuming four peanuts per Joe,

293
00:12:36,147 --> 00:12:38,347
37 cubic feet.

294
00:12:38,382 --> 00:12:40,716
Well, you're
a smart little fatso.

295
00:12:40,751 --> 00:12:44,053
It says here that students
don't have to raise their hands.

296
00:12:44,088 --> 00:12:45,321
They should just
ask every question

297
00:12:45,356 --> 00:12:46,689
that comes to their mind.

298
00:12:46,724 --> 00:12:48,891
Oh. Why are pine needles pointy?

299
00:12:48,926 --> 00:12:50,626
And, um-um-um,
what's the difference

300
00:12:50,661 --> 00:12:52,628
between an asteroid
and a meteor?

301
00:12:52,663 --> 00:12:54,196
And, mm... ooh-ooh!

302
00:12:54,231 --> 00:12:56,532
Can you shrug anything
other than your shoulders?

303
00:12:56,567 --> 00:12:57,700
(sighs)

304
00:12:57,735 --> 00:12:59,568
Does anyone else have
any questions?

305
00:12:59,604 --> 00:13:01,870
Um, if Mommy's purse didn't
belong in the microwave,

306
00:13:01,906 --> 00:13:04,473
why did it fit?

307
00:13:04,508 --> 00:13:07,476
We won't need computers,
we won't need books.

308
00:13:07,511 --> 00:13:09,912
I learned not to drink
out of the crick.

309
00:13:09,947 --> 00:13:12,815
You mean this isn't
crick water?

310
00:13:12,850 --> 00:13:14,249
My daddy raised me

311
00:13:14,285 --> 00:13:17,720
and all my 11 dead siblings
on crick water!

312
00:13:17,755 --> 00:13:19,588
Huh, everyone wears a hat.

313
00:13:19,624 --> 00:13:22,391
Sun hats in the summer,
wool hats in the winter.

314
00:13:22,426 --> 00:13:23,859
Ooh, look at this.

315
00:13:23,894 --> 00:13:26,495
Weekly Friday night
parents' meetings.

316
00:13:26,530 --> 00:13:27,930
Oh... Wait.

317
00:13:27,965 --> 00:13:29,431
There might be a safety hatch.

318
00:13:29,467 --> 00:13:30,933
Does the apostrophe come before

319
00:13:30,968 --> 00:13:33,335
or after the "S" on "parents"?

320
00:13:33,371 --> 00:13:36,238
After, which means both parents.

321
00:13:36,273 --> 00:13:40,242
(groaning)

322
00:13:40,277 --> 00:13:42,044
Well, I'm not wearing a hat.

323
00:13:42,079 --> 00:13:44,079
People might think I'm bald.

324
00:13:45,383 --> 00:13:47,383
(Homer grunting)

325
00:13:48,586 --> 00:13:50,219
Before we share announcements

326
00:13:50,254 --> 00:13:52,221
and see the results
of grade three's

327
00:13:52,256 --> 00:13:54,390
dance study of willows
and drooping trees,

328
00:13:54,425 --> 00:13:57,626
we greet the day
with a school song.

329
00:13:57,662 --> 00:13:59,528
Now I have to sing a song?!

330
00:13:59,563 --> 00:14:00,643
Why did those idiots mix up

331
00:14:00,665 --> 00:14:02,765
power strips
and surge protectors?

332
00:14:02,800 --> 00:14:06,735
♪ Every single living creature ♪

333
00:14:06,771 --> 00:14:10,105
♪ Every cat and every flea ♪

334
00:14:10,141 --> 00:14:14,576
♪ All things
with a facial feature ♪

335
00:14:14,612 --> 00:14:17,980
♪ Have the right
to smile at me ♪

336
00:14:18,015 --> 00:14:21,984
♪ Every family is a unit ♪

337
00:14:22,019 --> 00:14:25,921
♪ Sometimes yelling,
sometimes mad ♪

338
00:14:25,956 --> 00:14:30,459
♪ Divorced or gay
or even foster ♪

339
00:14:30,494 --> 00:14:34,329
♪ Even Billy with three dads. ♪

340
00:14:36,267 --> 00:14:38,233
<i>I feel like Beethoven</i>

341
00:14:38,269 --> 00:14:41,503
<i>when Charles Grodin finally</i>
<i>accepted him as his dog.</i>

342
00:14:47,243 --> 00:14:49,043
This school is so great now.

343
00:14:49,078 --> 00:14:50,878
Kids learn by doing.

344
00:14:50,913 --> 00:14:52,480
If it's so great,
why aren't you helping

345
00:14:52,515 --> 00:14:53,747
your kids do their homework?

346
00:14:53,783 --> 00:14:55,616
Bart's here doing it
right now.

347
00:14:55,651 --> 00:14:57,885
No, no, no!
Not two-thirds, one-half!

348
00:14:57,920 --> 00:14:59,720
Now that's one-third each.

349
00:14:59,755 --> 00:15:02,890
Now you're learning!
(laughs)

350
00:15:02,925 --> 00:15:04,692
Enjoy.

351
00:15:06,162 --> 00:15:08,295
One earthworm...
Gah!

352
00:15:08,331 --> 00:15:09,630
Two halves.

353
00:15:09,665 --> 00:15:11,632
ALL:
Wow!

354
00:15:11,667 --> 00:15:13,134
Willie! Willie!

355
00:15:13,169 --> 00:15:15,402
They want you to coach
our school math team!

356
00:15:15,438 --> 00:15:18,672
I'll have to check
with my supervisor.

357
00:15:18,708 --> 00:15:19,907
(dings twice)

358
00:15:19,942 --> 00:15:21,509
Good to go!

359
00:15:21,544 --> 00:15:24,945
So, if Willie gets paid
three cents per square foot

360
00:15:24,981 --> 00:15:26,947
for seeding, and if the field

361
00:15:26,983 --> 00:15:29,650
is 2,500 square feet,

362
00:15:29,685 --> 00:15:31,685
what is Willie getting?

363
00:15:31,721 --> 00:15:33,954
I'd say "screwed."

364
00:15:33,990 --> 00:15:35,623
(groans)
You're right!

365
00:15:35,658 --> 00:15:37,658
You! Chalmers!

366
00:15:37,693 --> 00:15:39,326
Aah!

367
00:15:39,362 --> 00:15:40,928
Uh-oh.
(tires screech)

368
00:15:40,963 --> 00:15:43,898
Come back here,
you Willie-chiseling cheat!

369
00:15:45,935 --> 00:15:47,134
(grunting)

370
00:15:47,170 --> 00:15:48,402
(tires screeching)

371
00:15:48,437 --> 00:15:51,138
(horn honking)

372
00:15:51,174 --> 00:15:53,140
Hey, who threw that egg

373
00:15:53,176 --> 00:15:56,010
at a most efficient
45-degree angle?

374
00:15:56,045 --> 00:15:58,512
It was me, sir.

375
00:15:58,548 --> 00:16:01,382
Kids, meet your new
math team captain.

376
00:16:01,417 --> 00:16:02,683
It's about time.

377
00:16:02,718 --> 00:16:04,485
But I'm the captain.

378
00:16:04,520 --> 00:16:05,653
Not anymore.

379
00:16:05,688 --> 00:16:07,021
I can't see.

380
00:16:07,056 --> 00:16:08,889
I can't see.

381
00:16:13,930 --> 00:16:16,330
Welcome to
the long-anticipated rematch

382
00:16:16,365 --> 00:16:20,067
between Waverly Hills
and Springfield.

383
00:16:20,102 --> 00:16:21,835
(crowd cheering)

384
00:16:21,871 --> 00:16:23,737
(grunting)

385
00:16:23,773 --> 00:16:27,041
Um, can you tell me which
college I should go to?

386
00:16:27,076 --> 00:16:29,710
(stammers)
It's not a sorting hat!

387
00:16:29,745 --> 00:16:32,846
Please! Early applications
are due next week!

388
00:16:32,882 --> 00:16:34,682
All right.

389
00:16:34,717 --> 00:16:37,318
Uh, Miami of Ohio.

390
00:16:37,353 --> 00:16:39,653
(groans)
Okay.

391
00:16:39,689 --> 00:16:42,356
Our first question is a toss-up.

392
00:16:42,391 --> 00:16:44,592
"What is the least
common multiple

393
00:16:44,627 --> 00:16:47,361
of six, eight and 16?"

394
00:16:47,396 --> 00:16:49,597
48!
Is correct!

395
00:16:49,632 --> 00:16:50,864
(all gasp)

396
00:16:50,900 --> 00:16:53,000
Whoa! No one said
there'd be math!

397
00:16:53,035 --> 00:16:54,668
We said there'd be
nothing but math!

398
00:16:54,704 --> 00:16:57,271
And you're
the math team captain!

399
00:16:57,306 --> 00:16:59,406
I thought I wouldn't
have to do anything.

400
00:16:59,442 --> 00:17:02,109
You know, like an Italian
cruise ship captain.

401
00:17:02,144 --> 00:17:03,744
You're lucky
that captain's not here

402
00:17:03,779 --> 00:17:05,946
right now to answer your insult!

403
00:17:05,982 --> 00:17:08,716
He'd crash a ship
right into your house!

404
00:17:08,751 --> 00:17:10,117
Aah!

405
00:17:10,152 --> 00:17:13,053
("Pi" by Kate Bush playing)

406
00:17:21,631 --> 00:17:24,431
♪ Oh, he love,
he love, he love ♪

407
00:17:24,467 --> 00:17:28,035
♪ He does love his numbers ♪

408
00:17:28,070 --> 00:17:30,738
♪ And they run, they run ♪

409
00:17:30,773 --> 00:17:33,641
♪ They run him in
a great big circle ♪

410
00:17:36,445 --> 00:17:39,947
♪ In a circle of infinity ♪

411
00:17:42,451 --> 00:17:45,019
ESPN isn't covering this.

412
00:17:46,055 --> 00:17:47,421
D'oh!

413
00:17:47,456 --> 00:17:50,291
♪ Point one, four ♪

414
00:17:50,326 --> 00:17:53,527
♪ One, five, nine ♪

415
00:17:57,933 --> 00:18:01,902
♪ Two, six, five, three, five ♪

416
00:18:01,937 --> 00:18:05,005
♪ Eight, nine, seven, nine ♪

417
00:18:05,041 --> 00:18:08,509
♪ Three, two. ♪

418
00:18:08,544 --> 00:18:12,413
And so, with the score
knotted at 29-all,

419
00:18:12,448 --> 00:18:15,949
it comes down
to one last question.

420
00:18:17,553 --> 00:18:20,020
Drawing three straight lines,

421
00:18:20,056 --> 00:18:22,189
construct nine non-overlapping--

422
00:18:22,224 --> 00:18:24,792
that's non-overlapping,
not overlapping,

423
00:18:24,827 --> 00:18:28,062
but non-overlapping--
triangles.

424
00:18:35,871 --> 00:18:38,839
HOMER:
Huh? Ooh.

425
00:18:38,874 --> 00:18:40,507
(chuckles)

426
00:18:40,543 --> 00:18:42,009
I have the answer!

427
00:18:42,044 --> 00:18:43,344
(all gasp)

428
00:18:43,379 --> 00:18:45,045
As someone whose dad's hair is

429
00:18:45,081 --> 00:18:47,514
made of Ms,
it's child's play.

430
00:18:47,550 --> 00:18:50,050
Oh, not this again.

431
00:18:51,053 --> 00:18:53,287
Whoo-hoo! I'm a solution!

432
00:18:53,322 --> 00:18:56,190
The Action Fractions win!

433
00:18:56,225 --> 00:18:58,392
You guys might be richer
and better looking,

434
00:18:58,427 --> 00:19:02,096
but we won a contest
to even the series!

435
00:19:04,166 --> 00:19:06,533
Mm, that'll look great
in our trophy case.

436
00:19:06,569 --> 00:19:07,901
Willie, build a trophy case.

437
00:19:07,937 --> 00:19:09,536
Aye, sir.

438
00:19:09,572 --> 00:19:13,040
But you know what we should
really thank for our success?

439
00:19:14,043 --> 00:19:15,909
Lower standards!

440
00:19:15,945 --> 00:19:17,378
ALL:
Lower standards!

441
00:19:17,413 --> 00:19:18,445
Aye.

442
00:19:24,455 --> 00:19:27,222
You're wonderful, Willie.

443
00:19:27,257 --> 00:19:29,758
You and the ancient
Scottish scientist

444
00:19:29,793 --> 00:19:32,894
that invented
the rummlie scob.

445
00:19:32,930 --> 00:19:36,498
Well, the truth is, it
wasn't invented for science.

446
00:19:36,533 --> 00:19:37,766
Really?

447
00:19:37,801 --> 00:19:39,401
Then what for?

448
00:19:39,436 --> 00:19:42,437
Oh, it was used for
hanging sheep stealers!

449
00:19:42,473 --> 00:19:44,773
The wee knots kept
them alive longer

450
00:19:44,808 --> 00:19:46,908
to make the punishment
more cruel.

451
00:19:46,944 --> 00:19:49,644
(shudders) Maybe I don't want
to know these things.

452
00:19:49,680 --> 00:19:52,147
Can you say something
nice about Scotland?

453
00:19:52,182 --> 00:19:54,916
Well, sometimes
the fog comes in

454
00:19:54,952 --> 00:19:57,519
and covers
everything terrible.

455
00:19:57,554 --> 00:20:00,270
Very atmospheric.
Oh.

456
00:20:04,050 --> 00:20:10,550
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

457
00:21:13,145 --> 00:21:14,945
Shh!

