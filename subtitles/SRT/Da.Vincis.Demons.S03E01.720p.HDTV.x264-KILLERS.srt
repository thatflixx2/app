1
00:00:00,570 --> 00:00:03,808
'The journey before you
will be monumental.'

2
00:00:03,809 --> 00:00:07,411
- Leonardo.
- It speaks with my mother's voice!

3
00:00:07,413 --> 00:00:10,547
Leonardo, please! Please! Don't
take the Book away from my people.

4
00:00:10,549 --> 00:00:11,949
There was no Book.

5
00:00:13,419 --> 00:00:15,286
- Who are you?
- Carlo de' Medici.

6
00:00:15,288 --> 00:00:16,620
We are the Labyrinth.

7
00:00:16,622 --> 00:00:20,958
The servants of the Horned God
will seize their opportunity!

8
00:00:20,960 --> 00:00:22,460
You're the Labyrinth?

9
00:00:22,462 --> 00:00:24,862
- Clarice.
- Carlo? You took long enough.

10
00:00:24,864 --> 00:00:29,266
500,000 florins was
withdrawn from the bank.

11
00:00:29,268 --> 00:00:32,503
Now Carlo has gone, someone
will have to assume the blame.

12
00:00:33,473 --> 00:00:34,872
Bastard!

13
00:00:34,874 --> 00:00:38,609
Clarice has gone, which
makes you, as his mother,

14
00:00:38,611 --> 00:00:40,811
the regent head of
the House of Medici.

15
00:00:42,581 --> 00:00:45,949
Girolamo, our suffering will
not end until the day you die!

16
00:00:45,951 --> 00:00:50,821
'Girolamo Riario, you were
dead, now you're born again.'

17
00:00:50,823 --> 00:00:54,325
- What do you want from me?
- For you to see what we see.

18
00:00:54,327 --> 00:00:56,227
We are one, Girolamo.

19
00:00:56,229 --> 00:01:00,398
The man you know as Pope Sixtus is
not the true Pope but an imposter.

20
00:01:00,400 --> 00:01:02,733
I shall assume the
Papal throne at once.

21
00:01:02,735 --> 00:01:05,236
- Have the decency to spare my
daughters. - Please, please!

22
00:01:05,238 --> 00:01:07,538
From now on, God
values what I value.

23
00:01:08,508 --> 00:01:10,508
I intend to take
my rightful place.

24
00:01:10,510 --> 00:01:13,710
Your only mission was to get
the son of the Sultan to Rome.

25
00:01:13,712 --> 00:01:16,414
Once that was accomplished, the
outcome was all but assured.

26
00:01:16,416 --> 00:01:18,549
If Bayezid is killed,
the result is war.

27
00:01:18,551 --> 00:01:21,785
Dishonored and dismissed,
retaliation will be required.

28
00:01:21,787 --> 00:01:23,587
They will stop at nothing.

29
00:01:23,589 --> 00:01:26,078
'In the Kingdom of Naples,
a place called Otranto

30
00:01:26,079 --> 00:01:28,202
is where you will find what you seek.'

31
00:01:28,402 --> 00:01:31,295
'The Ottomans just brought the
next holy war to my doorstep.'

32
00:01:31,297 --> 00:01:33,880
If the Turks are
victorious, it gives them

33
00:01:33,881 --> 00:01:35,600
a stronghold to take all of Italy.

34
00:01:35,601 --> 00:01:39,170
Unless you enlist a war engineer
to help you mount an offensive.

35
00:01:39,172 --> 00:01:41,138
We are outnumbered by thousands.

36
00:01:42,669 --> 00:01:44,068
We've only got one shot at this.

37
00:02:08,955 --> 00:02:10,087
Fuck!

38
00:02:30,596 --> 00:02:35,332
Open up! They're coming!
Open up! Open up!

39
00:02:35,334 --> 00:02:37,367
Don't shoot! Don't shoot, don't!

40
00:02:37,369 --> 00:02:39,814
I'm not a Turk, you
stupid pricks!

41
00:02:40,539 --> 00:02:43,340
Long live Naples!
Long live Naples!

42
00:02:45,811 --> 00:02:47,344
Ah, thanks!

43
00:03:04,863 --> 00:03:06,830
To the walls!

44
00:03:10,202 --> 00:03:11,702
Ready!

45
00:03:11,704 --> 00:03:14,671
- Hold till they fire!
- Shoot straight!

46
00:03:16,609 --> 00:03:18,308
Keep moving, men!

47
00:03:21,647 --> 00:03:23,113
Get back.

48
00:03:23,115 --> 00:03:25,849
- I'm with Da Vinci.
- Load the cannon.

49
00:03:26,585 --> 00:03:28,552
Load the cannons!

50
00:03:44,136 --> 00:03:47,738
- Secure the barbican. Double archers
on the south wall. - Yes, sir.

51
00:03:50,108 --> 00:03:52,509
I can assure you, your precautions
will not be necessary.

52
00:03:53,044 --> 00:03:54,544
Perhaps.

53
00:03:54,546 --> 00:03:57,814
Perhaps I'm unwilling to put all my
faith and future in some tinkerer.

54
00:03:57,816 --> 00:03:59,115
On my count.

55
00:04:01,427 --> 00:04:03,831
Allah will punish them

56
00:04:03,969 --> 00:04:06,429
by your hands

57
00:04:06,925 --> 00:04:08,191
Three...

58
00:04:08,831 --> 00:04:11,217
and bring them to disgrace!

59
00:04:13,732 --> 00:04:14,865
It cannot be.

60
00:04:14,866 --> 00:04:16,132
...two....

61
00:04:16,194 --> 00:04:17,451
He is with them.

62
00:04:17,811 --> 00:04:19,096
Da Vinci?

63
00:04:20,673 --> 00:04:21,871
Now!

64
00:04:21,873 --> 00:04:23,907
- Leonardo!
- What is it?

65
00:04:23,909 --> 00:04:26,777
The woman on the deck standing
next to the Ottoman prince.

66
00:04:26,779 --> 00:04:28,712
That is your mother.

67
00:04:40,693 --> 00:04:42,225
You cannot fire.

68
00:04:46,387 --> 00:04:49,097
May Allah grant your victory.

69
00:04:51,729 --> 00:04:55,422
Praise be to Allah!

70
00:04:55,907 --> 00:04:57,240
I have to.

71
00:05:00,846 --> 00:05:02,145
I have to.

72
00:05:37,148 --> 00:05:38,782
Leo.

73
00:05:41,086 --> 00:05:43,119
Is it true?

74
00:05:43,121 --> 00:05:45,154
Your mother?

75
00:05:45,156 --> 00:05:47,123
I had no choice.

76
00:06:57,764 --> 00:07:00,071
_

77
00:07:13,879 --> 00:07:15,879
- Count Riario?
- Cardinal Rodrigo.

78
00:07:19,685 --> 00:07:21,718
Where is His Holiness?

79
00:07:21,720 --> 00:07:24,688
His Holiness is... not himself.

80
00:07:26,859 --> 00:07:28,792
Is he of ill humor?

81
00:07:32,063 --> 00:07:34,498
Better that you should
see for yourself.

82
00:07:47,278 --> 00:07:49,212
Deeper, deeper.

83
00:07:52,985 --> 00:07:57,086
It is said that devils
despise the netherworld,

84
00:07:57,088 --> 00:07:59,990
which is why they so gleefully
damn us mortals there.

85
00:08:05,531 --> 00:08:07,898
He would never believe
I would stoop so low,

86
00:08:07,900 --> 00:08:09,933
which is why I must.

87
00:08:09,935 --> 00:08:14,070
To be free, I must go below.

88
00:08:25,651 --> 00:08:27,150
Your Holiness.

89
00:08:28,486 --> 00:08:30,186
I told you, leave me be!

90
00:08:31,189 --> 00:08:33,790
It's Girolamo, Your Holiness.

91
00:08:52,177 --> 00:08:53,810
It is you.

92
00:08:58,550 --> 00:09:00,183
Leave us.

93
00:09:03,689 --> 00:09:05,622
Enjoying the irony, are you?

94
00:09:06,592 --> 00:09:09,693
The captor, now the captive.

95
00:09:10,662 --> 00:09:12,629
I see the prisoner has escaped.

96
00:09:14,566 --> 00:09:17,233
Aided by someone
within my household.

97
00:09:17,235 --> 00:09:19,555
My dear brother wants
my head on a platter

98
00:09:19,556 --> 00:09:21,305
and his wearing the Papal crown.

99
00:09:21,505 --> 00:09:23,539
And would endanger his
very soul to do so,

100
00:09:24,743 --> 00:09:28,144
lying down with those
damnable Turkish dogs.

101
00:09:29,648 --> 00:09:33,016
And now, the Prodigal returns.

102
00:09:38,156 --> 00:09:40,657
Planning your escape, Father?

103
00:09:40,659 --> 00:09:42,626
How dare you call me that?

104
00:09:44,129 --> 00:09:45,929
Holy Father...

105
00:09:47,165 --> 00:09:50,133
...I am so very sorry.

106
00:09:54,572 --> 00:09:56,539
I prostrate myself before you.

107
00:10:39,117 --> 00:10:40,583
My dear boy.

108
00:10:40,585 --> 00:10:42,552
What have you done to yourself?

109
00:10:44,222 --> 00:10:46,189
Father, I...

110
00:10:48,293 --> 00:10:50,326
I despised you so.

111
00:10:50,328 --> 00:10:52,295
Because you despised yourself.

112
00:10:53,631 --> 00:10:55,665
Oh, I have done
monstrous things.

113
00:10:55,667 --> 00:10:57,701
I've killed innocents.

114
00:10:57,703 --> 00:11:01,337
I have scorned
you, betrayed you.

115
00:11:01,339 --> 00:11:04,074
I have sought false idols,
worshiped false gods.

116
00:11:05,443 --> 00:11:07,376
It was all at the
behest of others.

117
00:11:07,378 --> 00:11:09,345
At the behest of me.

118
00:11:15,954 --> 00:11:16,986
Come.

119
00:11:18,223 --> 00:11:21,424
Let us pray together.

120
00:11:50,222 --> 00:11:51,520
Amen.

121
00:12:04,269 --> 00:12:06,069
Whoever you are
looking for, lady,

122
00:12:06,071 --> 00:12:08,037
I can tell you now
he's not here.

123
00:12:10,308 --> 00:12:11,674
Signora de' Medici?

124
00:12:14,946 --> 00:12:16,913
Please, come in.

125
00:12:25,123 --> 00:12:26,956
Please, this way.

126
00:12:30,028 --> 00:12:32,228
I am servant to the
House of the Medici.

127
00:12:32,230 --> 00:12:34,264
They call me Shbeeb.

128
00:12:34,266 --> 00:12:35,698
I know who you are.

129
00:12:35,700 --> 00:12:39,435
And I know the nature of the services
you provided for my husband.

130
00:12:39,437 --> 00:12:41,971
I would do anything for
Lorenzo the Magnificent.

131
00:12:41,973 --> 00:12:44,007
Indeed. Anything his heart -

132
00:12:44,009 --> 00:12:46,676
or other organs
desired, I suppose.

133
00:12:48,346 --> 00:12:52,315
My husband kept a suite
here, I believe.

134
00:12:52,317 --> 00:12:54,717
I-I don't know what
you mean, signora.

135
00:12:54,719 --> 00:12:57,589
A place of respite during those long

136
00:12:57,590 --> 00:13:00,191
banking excursions to Rome, Naples...

137
00:13:02,060 --> 00:13:03,526
I swear to you...

138
00:13:03,528 --> 00:13:06,495
A place where he could bring one
of his whores for a good fuck.

139
00:13:09,167 --> 00:13:10,466
This way.

140
00:13:13,138 --> 00:13:16,206
We haven't had the pleasure
of his company for some time.

141
00:13:21,379 --> 00:13:23,246
Neither have I.

142
00:13:33,124 --> 00:13:35,158
Will he be joining you?

143
00:13:35,160 --> 00:13:38,027
At present, Signor de'
Medici is in Naples...

144
00:13:39,998 --> 00:13:41,965
...killing Turks, I hope.

145
00:13:43,335 --> 00:13:46,302
Unlikely he will walk through
that door anytime soon.

146
00:13:48,006 --> 00:13:49,038
Sit.

147
00:13:53,311 --> 00:13:55,245
I need your help.

148
00:13:56,614 --> 00:13:58,948
Anything for the
House of the Medici.

149
00:13:58,950 --> 00:14:01,117
Good.

150
00:14:01,119 --> 00:14:05,554
I have reason to believe an enemy
of Florence is here in Rome.

151
00:14:05,556 --> 00:14:08,624
He must be found and
brought to justice.

152
00:14:08,626 --> 00:14:12,395
Anyone who aids me in this mission
will be handsomely compensated.

153
00:14:13,431 --> 00:14:14,998
Who is this man?

154
00:14:16,434 --> 00:14:18,467
Lorenzo's uncle...

155
00:14:18,469 --> 00:14:20,436
Carlo de' Medici.

156
00:14:34,152 --> 00:14:39,188
A toast to Leonardo Da
Vinci, the Savior of Naples.

157
00:14:40,288 --> 00:14:43,326
The Savior of Italy.

158
00:14:43,328 --> 00:14:47,830
A gold sovereign for every infidel
you sent to a watery grave.

159
00:14:49,167 --> 00:14:50,866
And more to come,

160
00:14:50,868 --> 00:14:55,338
as you ply your destructive trade
for the Kingdom of Naples.

161
00:14:56,874 --> 00:15:01,377
A generous offer, no doubt, but
I'm afraid too little too late.

162
00:15:01,379 --> 00:15:03,712
Leonardo has been in
my employ now for...

163
00:15:05,917 --> 00:15:08,351
...considerable time.

164
00:15:08,353 --> 00:15:11,154
And in many ways, he is more
brother to me than aide.

165
00:15:13,325 --> 00:15:17,626
I will double whatever Lorenzo the
Magnificent pays his "brother".

166
00:15:23,969 --> 00:15:26,369
- How dare you?
- Hold, Alfonso.

167
00:15:27,939 --> 00:15:30,739
You do not know how
much Leonardo has lost.

168
00:15:30,741 --> 00:15:33,977
- We have all lost.
- Yes.

169
00:15:33,979 --> 00:15:36,912
But not something for which you
have sought your entire life.

170
00:15:40,018 --> 00:15:41,985
Leave him be.

171
00:15:46,324 --> 00:15:49,258
Drink on, the day is ours.
Drink on.

172
00:15:56,468 --> 00:15:58,167
Leo.

173
00:16:00,472 --> 00:16:02,005
Leo.

174
00:16:02,007 --> 00:16:03,973
Hey, come buy me a drink.

175
00:16:12,150 --> 00:16:13,449
Hey.

176
00:16:13,451 --> 00:16:16,386
You still don't know her fate.

177
00:16:16,388 --> 00:16:18,721
You know... your mother
may have survived.

178
00:16:20,492 --> 00:16:23,226
So... you're saying to me...

179
00:16:24,695 --> 00:16:27,997
...that my design failed?

180
00:16:27,999 --> 00:16:29,465
That's not what I meant.

181
00:16:29,467 --> 00:16:31,067
Well...

182
00:16:32,103 --> 00:16:35,471
...then we do know her fate.

183
00:16:37,275 --> 00:16:38,341
Leo...

184
00:16:38,343 --> 00:16:42,011
I designed my weapon to destroy.
Destroy.

185
00:16:42,013 --> 00:16:46,082
That's exactly what it did. I
mean in truth, in truth...

186
00:16:46,084 --> 00:16:49,218
the damn thing works even
better than I could have hoped.

187
00:16:50,055 --> 00:16:52,088
I mean, the cannons...

188
00:16:52,090 --> 00:16:54,123
they showed remarkable range.

189
00:16:54,125 --> 00:16:58,227
The bombards, they kept their internal
charge right up till the moment of impact.

190
00:16:58,229 --> 00:17:01,764
- Leo... - One minute, there's
a flotilla, the next minute...

191
00:17:02,934 --> 00:17:04,833
...just flotsam and jetsam.

192
00:17:04,835 --> 00:17:06,302
Leo...

193
00:17:06,304 --> 00:17:08,271
Come on, have a drink.

194
00:17:10,808 --> 00:17:12,775
Have a drink.

195
00:17:12,777 --> 00:17:14,243
No.

196
00:17:16,681 --> 00:17:18,281
See you later.

197
00:18:27,285 --> 00:18:29,318
I have done as asked...

198
00:18:29,320 --> 00:18:32,988
and have positioned myself in
the Pontiff's good graces.

199
00:18:34,125 --> 00:18:35,891
Has he agreed to
lead the Crusade?

200
00:18:39,297 --> 00:18:42,765
No, I have yet to
broach the subject.

201
00:18:42,767 --> 00:18:45,635
You must allow me to handle
His Holiness in my own way.

202
00:18:45,637 --> 00:18:48,971
His current state of agitation
requires a certain delicacy.

203
00:18:52,143 --> 00:18:55,244
Perhaps if I knew more of
the Architect's plan...

204
00:18:55,246 --> 00:18:57,046
Know your place, brother.

205
00:18:58,816 --> 00:19:01,651
The Architect would never
deign to meet a mere acolyte.

206
00:19:03,954 --> 00:19:05,988
Is this indeed why
you sought us out?

207
00:19:08,660 --> 00:19:10,893
No. No, it is not.

208
00:19:12,163 --> 00:19:13,996
Clarice Orsini is
here in Rome...

209
00:19:18,169 --> 00:19:21,103
...looking for you.

210
00:19:21,105 --> 00:19:23,272
The Medici have their
spies, as do we all.

211
00:19:25,810 --> 00:19:28,277
Did you really think...

212
00:19:28,279 --> 00:19:30,779
you could wander Rome,
spending her money...

213
00:19:32,183 --> 00:19:34,149
...and she would not
get wind of you?

214
00:19:35,753 --> 00:19:38,621
I was ordered to plunder
the coffers of Florence.

215
00:19:38,623 --> 00:19:41,924
Plunder them I did.

216
00:19:41,926 --> 00:19:46,028
I don't suppose you were ordered
to plunder her body as well?

217
00:19:46,030 --> 00:19:49,599
It is common knowledge you bedded
the First Lady of Florence.

218
00:19:49,601 --> 00:19:53,636
I can't imagine your orders
entailed such a blunderous tactic.

219
00:19:53,638 --> 00:19:56,839
- My methods are my own.
- You fool.

220
00:19:56,841 --> 00:19:59,342
All that matters is we
have the funds required.

221
00:19:59,344 --> 00:20:03,011
I am a physician,
a man of stature.

222
00:20:03,013 --> 00:20:05,781
I have been seen
with you in public.

223
00:20:05,783 --> 00:20:08,083
If your spurned lover
is hunting you,

224
00:20:08,085 --> 00:20:10,119
then she will find me.

225
00:20:10,121 --> 00:20:13,055
- I will resolve this.
- Like you resolved Da Vinci?

226
00:20:13,057 --> 00:20:16,024
You will remain here,
keep to your duties.

227
00:20:16,026 --> 00:20:19,228
I will resolve
Signora de' Medici.

228
00:21:54,359 --> 00:21:56,213
The remains of the Ottoman flagship have

229
00:21:56,214 --> 00:21:57,992
been smoldering for nearly 12 hours.

230
00:21:58,262 --> 00:22:03,031
Just now, in a matter of
minutes, the flames grew.

231
00:22:03,033 --> 00:22:06,902
More significantly, the amount of
smoke increased exponentially.

232
00:22:06,904 --> 00:22:08,971
Perhaps you should
get some sleep.

233
00:22:08,973 --> 00:22:11,674
I have toyed with similar
chemistry many times, haven't I?

234
00:22:12,610 --> 00:22:14,042
Yes.

235
00:22:14,044 --> 00:22:15,878
Different substances,
when they're set ablaze,

236
00:22:15,880 --> 00:22:17,646
they can create a
different class of smoke.

237
00:22:17,648 --> 00:22:20,583
It can hug the ground, obscure
the view for hours...

238
00:22:20,585 --> 00:22:21,784
sometimes days.

239
00:22:23,320 --> 00:22:25,888
You give the Ottoman more
credit than they deserve.

240
00:22:25,890 --> 00:22:28,086
This chemistry sounds much too

241
00:22:28,087 --> 00:22:29,826
sophisticated for a horde of heathens.

242
00:22:29,827 --> 00:22:33,796
These "heathens" defeated the Holy
Roman Empire at Constantinople,

243
00:22:33,798 --> 00:22:36,131
the most heavily fortified
city of its day.

244
00:22:36,133 --> 00:22:38,967
I doubt that we have yet seen
what they are truly capable of.

245
00:22:38,969 --> 00:22:42,004
I do not much care whether
they're praying to their gods

246
00:22:42,006 --> 00:22:44,072
or bedding their
beasts of burden.

247
00:22:44,074 --> 00:22:46,041
My fleet is due at sunrise.

248
00:22:47,144 --> 00:22:49,077
They will all be cut to ribbons.

249
00:23:49,273 --> 00:23:51,239
Daughter.

250
00:23:53,978 --> 00:23:55,811
Come forth.

251
00:24:15,766 --> 00:24:17,232
Sit.

252
00:24:17,234 --> 00:24:19,201
You must be starved.

253
00:24:23,040 --> 00:24:26,775
It's not poisoned, if
that's what you fear.

254
00:24:26,777 --> 00:24:30,045
Indeed. You prefer suicide
missions to outright murder.

255
00:24:31,115 --> 00:24:33,883
Ah, the dreaded time has come...

256
00:24:35,920 --> 00:24:39,221
...when the dutiful child
begets a defiant youth.

257
00:24:40,591 --> 00:24:43,826
I haven't come here
to defy you, Father.

258
00:24:46,964 --> 00:24:48,998
I've come here for answers.

259
00:24:49,000 --> 00:24:50,966
Daughter, please.

260
00:24:53,871 --> 00:24:54,904
Sit.

261
00:24:58,943 --> 00:25:02,745
Imprisoned for so many years, I
do hate to waste a good meal.

262
00:25:12,823 --> 00:25:13,923
So...

263
00:25:15,626 --> 00:25:17,593
What news from Otranto?

264
00:25:19,764 --> 00:25:21,797
You haven't heard?

265
00:25:21,799 --> 00:25:23,766
I want to hear it from you.

266
00:25:25,836 --> 00:25:28,871
Leonardo Da Vinci seems to
have thwarted the invasion.

267
00:25:33,343 --> 00:25:36,111
You're not perturbed by this?

268
00:25:36,113 --> 00:25:38,981
The Ottoman Empire may have
one or two surprises...

269
00:25:40,084 --> 00:25:43,085
...for Signor Da Vinci.

270
00:25:43,087 --> 00:25:46,154
How will their success
bring you justice?

271
00:25:46,156 --> 00:25:49,091
Justice, Daughter?

272
00:25:49,093 --> 00:25:52,194
Your rightful place
on the Papal throne.

273
00:25:52,196 --> 00:25:54,362
The One True Pope -
that's what you said.

274
00:25:54,364 --> 00:25:56,732
You must understand, Daughter,

275
00:25:56,734 --> 00:25:59,969
the throne was always
a means to an end.

276
00:25:59,971 --> 00:26:04,773
The Sultan and his armies
too - mere stepping stones.

277
00:26:04,775 --> 00:26:07,375
Then why? Why is the
invasion necessary?

278
00:26:07,377 --> 00:26:09,979
We are like the farmer.

279
00:26:09,981 --> 00:26:12,459
After harvesting the wheat, he uses fire

280
00:26:12,460 --> 00:26:15,803
to clear the field, burn the chaff.

281
00:26:16,003 --> 00:26:17,753
Only then can a new
crop take root.

282
00:26:18,589 --> 00:26:20,555
And the Turks are your fire?

283
00:26:23,995 --> 00:26:27,429
Soon the day will come...
a world without popes...

284
00:26:28,232 --> 00:26:30,198
or sultans or kings.

285
00:26:31,769 --> 00:26:33,736
Only the Sons of Mithras.

286
00:26:34,905 --> 00:26:36,872
What are you saying?

287
00:26:39,309 --> 00:26:41,343
The invasion is
only the beginning.

288
00:26:41,345 --> 00:26:44,013
Sadly, many must die.

289
00:26:45,950 --> 00:26:47,983
Yet the Enlightened
shall survive.

290
00:26:51,355 --> 00:26:55,057
There is a place for you
at my side, Daughter.

291
00:26:59,730 --> 00:27:01,897
Rest by the fire. We
shall speak more...

292
00:27:02,833 --> 00:27:04,800
upon the morning.

293
00:27:26,623 --> 00:27:27,990
What is that?

294
00:27:32,663 --> 00:27:34,629
Look, the smoke's clearing.

295
00:27:36,333 --> 00:27:37,700
Look.

296
00:27:38,502 --> 00:27:40,468
They've been busy.

297
00:27:44,775 --> 00:27:48,376
They've built some form of wooden
platform on the breakwater.

298
00:27:48,378 --> 00:27:50,545
Yeah.

299
00:27:50,547 --> 00:27:52,147
Block and tackle too.

300
00:27:54,318 --> 00:27:56,719
Reminds me of the uh...
thing in your studio.

301
00:27:56,721 --> 00:27:59,855
That's for hauling
marble statuary.

302
00:27:59,857 --> 00:28:01,757
What would they be hauling?

303
00:28:01,759 --> 00:28:04,392
- I don't know.
- Give me that.

304
00:28:04,394 --> 00:28:08,197
Ah, Da Vinci. I'm pleased you
will be here to bear witness.

305
00:28:08,199 --> 00:28:10,232
Bear witness to what?

306
00:28:10,234 --> 00:28:12,267
Our victory at Otranto.

307
00:28:12,269 --> 00:28:15,337
On the horizon - the
Grand Fleet of Naples.

308
00:28:36,927 --> 00:28:38,961
Some form of
underwater explosive,

309
00:28:38,963 --> 00:28:40,996
tethered to the seabed.

310
00:28:42,532 --> 00:28:44,499
A detonation on impact.

311
00:28:45,636 --> 00:28:46,668
How?

312
00:28:47,437 --> 00:28:49,071
My God.

313
00:28:56,881 --> 00:28:58,981
Sound the alarm!

314
00:28:58,983 --> 00:29:01,083
The cannon, Da Vinci!

315
00:29:06,891 --> 00:29:08,390
Get the muzzle!

316
00:29:08,392 --> 00:29:11,359
You two, move! Turn
it to the right!

317
00:29:12,029 --> 00:29:13,628
Shoot now!

318
00:29:21,305 --> 00:29:22,938
Zo, Zo!

319
00:29:22,940 --> 00:29:24,572
Light the balls.

320
00:29:24,574 --> 00:29:26,641
- Light it!
- Bring it down!

321
00:29:26,643 --> 00:29:27,642
Bring it down! Bring it down!

322
00:29:45,963 --> 00:29:47,062
Re-arm!

323
00:29:48,032 --> 00:29:49,264
Re-arm!

324
00:29:52,203 --> 00:29:55,403
- How're they firing so fast?
- I don't know.

325
00:30:15,425 --> 00:30:17,392
It's my cannon.

326
00:30:18,131 --> 00:30:19,051
It's my...

327
00:30:19,263 --> 00:30:21,897
It's my cannon! It's the
one I designed for you!

328
00:30:29,039 --> 00:30:31,340
The city walls.

329
00:30:31,342 --> 00:30:33,242
They're breaching
the city walls!

330
00:30:35,079 --> 00:30:37,445
Zo, Zo. Hey, hey, hey!

331
00:30:37,447 --> 00:30:39,181
With me!

332
00:30:39,183 --> 00:30:41,149
Get inside now!

333
00:30:42,586 --> 00:30:44,019
Come on! Come on!

334
00:30:52,596 --> 00:30:54,096
Swords! Swords!

335
00:30:58,435 --> 00:31:01,136
- I'll take two.
- Long live Naples!

336
00:31:04,141 --> 00:31:06,008
I can fix this.

337
00:31:10,075 --> 00:31:11,332
Shit.

338
00:32:44,839 --> 00:32:48,068
Kill the old and the infirm.
The rest, take as slaves.

339
00:33:16,407 --> 00:33:17,572
Fall back!

340
00:33:29,420 --> 00:33:30,919
No.

341
00:33:32,189 --> 00:33:34,589
Fall back! Fall back!

342
00:34:56,540 --> 00:34:58,507
So I have my answer.

343
00:35:01,411 --> 00:35:03,345
I'm not betraying you, Father.

344
00:35:03,347 --> 00:35:05,313
Indeed, you are not.

345
00:35:06,617 --> 00:35:10,585
You are betraying her...
your sister.

346
00:35:10,587 --> 00:35:13,588
My dear, sweet Amelia...

347
00:35:13,590 --> 00:35:15,923
...murdered by my
monster of a brother.

348
00:35:17,093 --> 00:35:21,196
That is what has
made me what I am.

349
00:35:21,198 --> 00:35:23,165
And you too, I can see.

350
00:35:25,602 --> 00:35:28,069
I just need to go.

351
00:35:28,071 --> 00:35:29,629
Before you depart, let me give you

352
00:35:29,630 --> 00:35:31,813
one more piece of fatherly wisdom.

353
00:35:33,009 --> 00:35:34,643
Fatherly wisdom?

354
00:35:34,645 --> 00:35:36,611
Beware the midway.

355
00:35:37,781 --> 00:35:40,948
For that is the most
dangerous path.

356
00:35:40,950 --> 00:35:42,917
You would forsake
your only daughter?

357
00:36:11,315 --> 00:36:14,082
Give her time.

358
00:36:14,084 --> 00:36:16,150
She will see the
wisdom of her elders.

359
00:36:17,821 --> 00:36:19,787
As will Da Vinci.

360
00:38:01,358 --> 00:38:04,091
'Cardinal Rodrigo?'

361
00:38:04,093 --> 00:38:06,280
He was the Pontiff's right hand since

362
00:38:06,281 --> 00:38:08,150
the departure of Lupo Mercuri.

363
00:38:09,866 --> 00:38:11,633
Who did this?

364
00:38:11,635 --> 00:38:15,036
No one was seen or heard, yet I
must concur with His Holiness.

365
00:38:15,038 --> 00:38:17,171
The Prisoner wants
his brother dead.

366
00:38:17,173 --> 00:38:22,109
If he cannot get to him, he
will get to those around him.

367
00:38:22,111 --> 00:38:25,079
How could the Sons of Mithras
infiltrate the Vatican?

368
00:38:28,251 --> 00:38:30,752
I've doubled the guards
on His Holiness -

369
00:38:30,754 --> 00:38:32,721
men I can trust.

370
00:38:32,723 --> 00:38:34,689
Haven't you learned?

371
00:38:35,759 --> 00:38:37,726
There are none we
can truly trust.

372
00:38:39,796 --> 00:38:41,763
I trust you, my brother.

373
00:38:47,604 --> 00:38:49,637
How is the Pontiff taking this?

374
00:38:49,639 --> 00:38:54,141
I've yet to tell him. I fear his
anxiety might overwhelm him.

375
00:38:54,143 --> 00:38:56,544
Precisely his brother's
plan, I presume.

376
00:38:58,281 --> 00:39:00,715
Well, it's a chance we
shall have to take.

377
00:39:02,719 --> 00:39:04,118
Endanger the Pontiff?

378
00:39:06,155 --> 00:39:08,122
Is that the plan?

379
00:39:09,259 --> 00:39:12,093
The Cardinal's death
provides an opportunity.

380
00:39:12,095 --> 00:39:15,196
Sixtus will be frantic,
terrified to the bone.

381
00:39:15,198 --> 00:39:17,231
Now is your chance
to embolden him.

382
00:39:17,233 --> 00:39:19,200
He's right. No better time.

383
00:39:23,740 --> 00:39:25,807
Are you...

384
00:39:25,809 --> 00:39:27,609
Are you having doubts, Girolamo?

385
00:39:32,783 --> 00:39:35,216
I confess I have suffered
certain nightmares.

386
00:39:36,620 --> 00:39:38,586
Remnants of the past.

387
00:39:40,356 --> 00:39:41,790
Yeah.

388
00:39:41,792 --> 00:39:43,825
You will learn, Girolamo.

389
00:39:43,827 --> 00:39:45,860
On our great journey,

390
00:39:45,862 --> 00:39:49,130
our hardest-fought battles
are with ourselves.

391
00:39:52,135 --> 00:39:54,101
May I ask for more guidance?

392
00:40:30,874 --> 00:40:34,008
We're the horns of the increate.

393
00:40:34,010 --> 00:40:36,845
We're the shadows at the
center of the Labyrinth...

394
00:41:00,704 --> 00:41:02,670
Zo, Zo!

395
00:41:07,110 --> 00:41:08,576
Zo...

396
00:41:08,578 --> 00:41:10,912
Zo, it's over. It's over.
It's over.

397
00:41:11,581 --> 00:41:13,548
It's over.

398
00:41:20,289 --> 00:41:22,256
This is hell.

399
00:41:27,864 --> 00:41:29,831
Otranto's fallen.

400
00:41:32,101 --> 00:41:34,135
We have to get off the streets.

401
00:41:34,137 --> 00:41:35,603
Yeah.

402
00:41:41,177 --> 00:41:43,044
- You alright?
- I am.

403
00:41:57,594 --> 00:41:59,961
Leonardo! In here!

404
00:42:04,333 --> 00:42:06,200
Stand down.

405
00:42:06,202 --> 00:42:08,169
This is Signor Da Vinci.

406
00:42:10,006 --> 00:42:12,506
- You're alive!
- You could call it that.

407
00:42:17,948 --> 00:42:20,682
- Water, rations.
- Something stronger.

408
00:42:22,018 --> 00:42:25,419
We must keep our wits about
us if we are to survive.

409
00:42:25,421 --> 00:42:27,388
That's a very large "if".

410
00:42:52,181 --> 00:42:53,280
Father?

411
00:42:55,384 --> 00:42:57,351
Leonardo, please.

412
00:43:02,291 --> 00:43:03,825
Not now.

413
00:43:11,367 --> 00:43:13,467
I am a banker.

414
00:43:14,738 --> 00:43:16,704
Money is my currency.

415
00:43:19,475 --> 00:43:21,542
There is nothing that
cannot be bought.

416
00:43:22,512 --> 00:43:25,246
I say we make them an offer.

417
00:43:25,248 --> 00:43:29,684
A ransom for the survivors,
the remaining populace.

418
00:43:29,686 --> 00:43:31,519
I mean...

419
00:43:31,521 --> 00:43:33,587
even godless infidels
must have a price.

420
00:43:33,589 --> 00:43:35,556
With all due respect,

421
00:43:37,160 --> 00:43:39,127
Lorenzo the Magnificent...

422
00:43:41,497 --> 00:43:43,464
...you are in Naples now.

423
00:43:46,402 --> 00:43:49,570
And in Naples, we do
not stand idly by

424
00:43:49,572 --> 00:43:52,540
as our women are
raped and murdered.

425
00:43:52,542 --> 00:43:54,676
Or our men and boys
sold into slavery,

426
00:43:54,678 --> 00:43:57,411
or our kings sent
to an early grave.

427
00:43:57,413 --> 00:44:00,214
You are out of your mind.

428
00:44:00,216 --> 00:44:03,685
The only way to save lives
is through negotiation.

429
00:44:03,687 --> 00:44:06,187
I am truly sorry that
you've lost your liege.

430
00:44:06,189 --> 00:44:07,722
Sorry?

431
00:44:10,426 --> 00:44:13,828
Alfonso the King of Naples is
lying out there in pieces,

432
00:44:13,830 --> 00:44:17,231
being violated by all manner
of infidel, I am sure.

433
00:44:18,434 --> 00:44:21,435
Men of Naples...

434
00:44:21,437 --> 00:44:24,538
...we must gather the troops
and attack at first light.

435
00:44:24,540 --> 00:44:28,509
I cannot allow this. We must
negotiate an acceptable peace.

436
00:44:28,511 --> 00:44:32,279
Jesus Christ, you are
both... fucking insane.

437
00:44:40,623 --> 00:44:41,756
Seriously?

438
00:44:41,758 --> 00:44:43,791
Did you not see what we saw?

439
00:44:43,793 --> 00:44:45,827
Did you not just get the...

440
00:44:45,829 --> 00:44:48,730
shit, piss and fucking tears...

441
00:44:49,899 --> 00:44:52,233
...flogged out of you
like we just did?

442
00:44:52,235 --> 00:44:54,202
You can't stop them.

443
00:44:55,671 --> 00:44:57,638
You can't pay them off.

444
00:45:00,376 --> 00:45:03,410
We have to turn around,
and we have to run.

445
00:45:03,412 --> 00:45:04,812
No. Leonardo...

446
00:45:05,815 --> 00:45:07,548
Leonardo, you can fix this.

447
00:45:09,185 --> 00:45:11,152
No, listen. Leonardo...

448
00:45:16,192 --> 00:45:17,558
Fix this?

449
00:45:21,264 --> 00:45:23,231
I assure you, you...

450
00:45:24,901 --> 00:45:26,344
...you would have more success asking

451
00:45:26,345 --> 00:45:28,407
the Turks to fix this than me.

452
00:45:28,638 --> 00:45:30,604
Can't you see?

453
00:45:31,875 --> 00:45:33,841
Can't you... see...

454
00:45:35,344 --> 00:45:37,678
...what it is that you
are asking me to fix....

455
00:45:38,848 --> 00:45:41,816
...who you are asking me
to wage war against?

456
00:45:44,420 --> 00:45:48,289
They have my cannons,
they have my detonators,

457
00:45:48,291 --> 00:45:50,724
they have my explosives,
they have my bombard fire.

458
00:45:53,830 --> 00:45:55,863
I have no idea how...

459
00:45:55,865 --> 00:46:00,334
but somehow they
have my weapons.

460
00:46:00,336 --> 00:46:03,737
They have my work,
they have my designs.

461
00:46:07,476 --> 00:46:09,443
Holy hell.

462
00:46:10,316 --> 00:46:11,206
What?

463
00:46:11,406 --> 00:46:12,780
What?

464
00:46:14,184 --> 00:46:15,649
What's that?

465
00:46:16,752 --> 00:46:18,319
Oh, my God.

466
00:46:19,755 --> 00:46:23,524
It's that thing you designed,
that monstrous armored cart.

467
00:46:36,906 --> 00:46:38,206
How...

468
00:46:46,182 --> 00:46:48,382
We cannot stop
that unholy thing.

469
00:46:48,384 --> 00:46:50,617
- We must retreat.
- We can't just leave.

470
00:46:50,619 --> 00:46:53,520
Watch us.

471
00:46:53,522 --> 00:46:56,790
- They're headed this way.
- We cannot fight that monstrosity!

472
00:46:56,792 --> 00:46:59,160
We must at least try. If
we can stall its progress,

473
00:46:59,162 --> 00:47:00,694
then we could save
hundreds of lives.

474
00:47:02,832 --> 00:47:04,698
Fuck, they must have heard you.

475
00:47:07,503 --> 00:47:10,204
That's good.

476
00:47:10,206 --> 00:47:12,406
How the fuck can that be good?

477
00:47:12,408 --> 00:47:13,674
We can fight back.

478
00:47:16,312 --> 00:47:18,346
- I need liquor.
- You and I both.

479
00:47:18,348 --> 00:47:21,115
Barrels - barrels of liquor.
Here, here, accelerant.

480
00:47:22,886 --> 00:47:24,919
Stack them. Stack
them up over here.

481
00:47:24,921 --> 00:47:27,355
Now, angle the trajectory
facing the Turks.

482
00:47:27,357 --> 00:47:29,223
What the hell are you up to?

483
00:47:29,225 --> 00:47:30,892
When they fire upon
us, the impact-

484
00:47:30,894 --> 00:47:31,984
the impact from the bombard will

485
00:47:31,985 --> 00:47:33,362
ignite the contents of the barrels,

486
00:47:33,363 --> 00:47:36,730
which will create an intense
backdraft that should stop them,

487
00:47:36,732 --> 00:47:38,699
stop them in their tracks.

488
00:47:38,701 --> 00:47:41,835
- Okay, here.
- To me. To me.

489
00:47:41,837 --> 00:47:43,905
Just get Leonardo
whatever he needs.

490
00:47:51,347 --> 00:47:53,114
Come on. On top of here.

491
00:47:55,484 --> 00:47:56,850
That's it.

492
00:48:07,897 --> 00:48:10,697
- Leo, time to go!
- Everyone, take cover.

493
00:48:27,246 --> 00:48:29,280
You did it, Leonardo.

494
00:48:29,282 --> 00:48:30,948
You did it!

495
00:48:39,158 --> 00:48:40,191
No.

496
00:48:40,927 --> 00:48:42,893
We need to be sure.

497
00:48:59,679 --> 00:49:01,579
Oh, my God.

498
00:49:06,018 --> 00:49:07,184
Hey.

499
00:49:08,521 --> 00:49:10,221
Hey, Leo!

500
00:49:10,223 --> 00:49:11,856
Leo!

501
00:49:24,270 --> 00:49:26,303
How can I fight...

502
00:49:26,305 --> 00:49:28,105
myself?

