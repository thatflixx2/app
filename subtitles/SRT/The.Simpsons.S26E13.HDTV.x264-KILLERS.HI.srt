﻿1
00:00:04,352 --> 00:00:05,519
(giggles)

2
00:00:07,555 --> 00:00:09,890
D'oh!

3
00:00:09,924 --> 00:00:11,558
(grunts)

4
00:00:11,582 --> 00:00:15,582
<font color="#00FF00">♪ The Simpsons 26x13 ♪</font>
<font color="#00FFFF">Walking Big & Tall</font>
Original Air Date on February 8, 2015

5
00:00:20,268 --> 00:00:21,902
Woo-hoo!

6
00:00:21,936 --> 00:00:23,737
(screams)

7
00:00:23,772 --> 00:00:25,439
D'oh!

8
00:00:25,463 --> 00:00:31,963
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

9
00:00:38,186 --> 00:00:41,388
And I regret to inform you
that our Soviet sister city,

10
00:00:41,423 --> 00:00:44,758
Springograd, has disappeared
from the map.

11
00:00:44,793 --> 00:00:48,562
Now, I'd like to welcome
our beloved four-time mayor,

12
00:00:48,596 --> 00:00:50,064
Hans Moleman!

13
00:00:50,098 --> 00:00:53,367
(applause)

14
00:00:53,401 --> 00:00:55,235
Springfielders, rejoice.

15
00:00:55,270 --> 00:00:58,338
What is the one thing our
blessed little town has lacked?

16
00:00:58,373 --> 00:00:59,773
A human zoo?

17
00:00:59,808 --> 00:01:01,909
What we lack is a town anthem.

18
00:01:01,943 --> 00:01:04,311
And you're in luck,
because I have written one.

19
00:01:04,345 --> 00:01:06,113
Mr. Largo, if you please.

20
00:01:06,147 --> 00:01:07,381
Mm-hmm.

21
00:01:07,415 --> 00:01:09,283
♪ ♪

22
00:01:09,317 --> 00:01:11,118
♪ There's a special
little place ♪

23
00:01:11,152 --> 00:01:13,253
♪ A special smile
on every face ♪

24
00:01:13,288 --> 00:01:16,723
♪ A town called Springfield ♪

25
00:01:16,758 --> 00:01:19,159
♪ Special buildings,
special sky ♪

26
00:01:19,194 --> 00:01:21,128
♪ A unique place
to live and die ♪

27
00:01:21,162 --> 00:01:25,265
♪ Only Springfield ♪

28
00:01:25,300 --> 00:01:29,470
♪ Of all the cities
on the mappy ♪

29
00:01:29,504 --> 00:01:33,407
♪ You're the one
that makes me happy ♪

30
00:01:33,441 --> 00:01:37,611
♪ Only Springfield,
only Springfield... ♪

31
00:01:37,645 --> 00:01:39,346
Stop the anthem!

32
00:01:39,380 --> 00:01:41,014
What is it, Moe?

33
00:01:41,049 --> 00:01:43,851
I was in Tuscaloosa on vacation.

34
00:01:43,885 --> 00:01:45,219
Started out great.

35
00:01:45,253 --> 00:01:48,021
They got a joint there
called Moe's Original Barbeque.

36
00:01:48,056 --> 00:01:49,990
But then I heard
<i>their</i> city anthem!

37
00:01:50,024 --> 00:01:51,425
Give a listen!

38
00:01:51,459 --> 00:01:55,295
♪ Of all the cities
on the mappy ♪

39
00:01:55,330 --> 00:01:59,366
♪ You're the one
that makes me happy ♪

40
00:01:59,400 --> 00:02:05,005
♪ Only Tuscaloosa,
only Tuscaloosa. ♪

41
00:02:05,039 --> 00:02:06,273
That's our song.

42
00:02:06,307 --> 00:02:08,742
They just changed
"Springfield" to "Tuscaloosa."

43
00:02:11,746 --> 00:02:13,180
(growls)

44
00:02:13,214 --> 00:02:15,349
Guns are for celebrating.

45
00:02:15,383 --> 00:02:17,551
What do you do with them
when you're angry?

46
00:02:17,585 --> 00:02:19,319
Now, calm down.

47
00:02:19,354 --> 00:02:23,390
This could just be
an amazing musical coincidence.

48
00:02:23,424 --> 00:02:28,028
There ain't no coincidences
in popular songs, bony.

49
00:02:28,062 --> 00:02:30,030
♪ Only Austin... ♪

50
00:02:30,064 --> 00:02:31,999
♪ Only Oakland... ♪

51
00:02:32,033 --> 00:02:34,001
♪ Only Calgary... ♪

52
00:02:34,035 --> 00:02:36,003
♪ Only Provo... ♪

53
00:02:36,037 --> 00:02:38,038
(singing in foreign language)

54
00:02:38,072 --> 00:02:39,706
♪ Area 51... ♪

55
00:02:39,741 --> 00:02:41,208
(crowd clamoring)

56
00:02:41,242 --> 00:02:44,178
We've been singing this song
like it only belonged to us,

57
00:02:44,212 --> 00:02:47,181
when every city in America's
had its lips on it!

58
00:02:47,215 --> 00:02:50,150
Even Des Moines.

59
00:02:50,185 --> 00:02:51,852
Wait a minute.

60
00:02:51,886 --> 00:02:55,822
This song was supposedly
written by former Mayor Moleman!

61
00:02:55,857 --> 00:02:57,824
(crowd murmuring)

62
00:02:57,859 --> 00:02:59,860
Oh, I didn't write it.

63
00:02:59,894 --> 00:03:01,528
I bought it from a salesman

64
00:03:01,563 --> 00:03:04,965
who was selling it
to half the towns in America.

65
00:03:04,999 --> 00:03:07,334
I didn't think you'd find out,

66
00:03:07,368 --> 00:03:11,638
because I never thought any of
us would ever go anywhere.

67
00:03:11,673 --> 00:03:14,508
Well, then what do I do
with this?!

68
00:03:14,542 --> 00:03:18,045
Now I can't be buried
in a Jewish cemetery!

69
00:03:18,079 --> 00:03:20,380
Please be merciful.

70
00:03:20,415 --> 00:03:21,548
Hyah!

71
00:03:21,583 --> 00:03:22,482
(horse neighs)

72
00:03:22,517 --> 00:03:23,850
Boy, sure is nice

73
00:03:23,885 --> 00:03:26,453
not to be the one
on the horse for a change.

74
00:03:26,487 --> 00:03:29,456
As mayor, I balanced the budget
eight times!

75
00:03:29,490 --> 00:03:31,358
I still believe in this town--

76
00:03:31,392 --> 00:03:34,127
a town that deserves
its own original song.

77
00:03:34,162 --> 00:03:37,364
And as an aspiring musician
with off-Broadway dreams,

78
00:03:37,398 --> 00:03:38,865
I'm the one to write it.

79
00:03:38,900 --> 00:03:40,067
So ordered.

80
00:03:40,101 --> 00:03:41,201
I'll write that song.

81
00:03:41,236 --> 00:03:42,903
Pharrell Williams?!

82
00:03:42,937 --> 00:03:44,304
I'm sorry, everyone,

83
00:03:44,339 --> 00:03:46,306
but the eight-year-old girl
got there first.

84
00:03:46,341 --> 00:03:47,975
I understand.

85
00:03:48,009 --> 00:03:50,644
♪ It might seem crazy
what I'm about to say ♪

86
00:03:50,678 --> 00:03:53,814
♪ Sunshine, she's here,
you can take a break... ♪

87
00:03:53,848 --> 00:03:55,749
Hyah!

88
00:03:55,783 --> 00:03:57,351
Shelbyville rules!

89
00:03:57,385 --> 00:04:00,821
Springfield drools...!

90
00:04:02,590 --> 00:04:04,258
Let's see.

91
00:04:04,292 --> 00:04:05,792
What rhymes with "Jebediah"?

92
00:04:05,827 --> 00:04:07,427
"Tire fire"?

93
00:04:07,462 --> 00:04:09,796
How about "Patty and Selma"?
"Fatty and smell ya."

94
00:04:09,831 --> 00:04:11,431
"Mr. Teeny"?
"Sister wienie."

95
00:04:11,466 --> 00:04:13,267
You've got a gift!

96
00:04:13,301 --> 00:04:14,768
Well, don't be so surprised.

97
00:04:14,802 --> 00:04:16,803
I did write that
"Lisa, It's Your Birthday" song.

98
00:04:16,838 --> 00:04:18,305
Yeah, with that mental patient

99
00:04:18,339 --> 00:04:20,040
who thought he was
Michael Jackson.

100
00:04:20,074 --> 00:04:22,943
Whoa. Thinking back,
I'm kind of surprised

101
00:04:22,977 --> 00:04:26,346
Mom and Dad let a crazy man
spend all night in my bedroom.

102
00:04:26,381 --> 00:04:28,315
Simpler time.

103
00:04:28,349 --> 00:04:31,718
Bart, I need your help to write
a new anthem for Springfield.

104
00:04:31,753 --> 00:04:33,320
Will you team up with me?

105
00:04:33,354 --> 00:04:35,389
Hmm. What's in it for me?

106
00:04:35,423 --> 00:04:37,457
Uh, I'll make brownies later.

107
00:04:37,492 --> 00:04:40,193
Whoa, that is the best deal
any songwriter ever got.

108
00:04:40,228 --> 00:04:42,029
And that's true even if
I don't get the brownies.

109
00:04:42,063 --> 00:04:43,463
I'm in!

110
00:04:43,498 --> 00:04:45,966
I think we might just
make a good team,

111
00:04:46,000 --> 00:04:48,068
like Maggie and Grampa.

112
00:04:48,102 --> 00:04:50,103
♪ ♪

113
00:05:07,755 --> 00:05:09,256
What are you doing?

114
00:05:09,290 --> 00:05:11,158
What's it look like?
Writing a song.

115
00:05:11,192 --> 00:05:12,793
(scoffs)
Artists.

116
00:05:12,827 --> 00:05:14,294
(makes farting noises)

117
00:05:14,329 --> 00:05:16,096
♪ ♪

118
00:05:20,768 --> 00:05:23,503
We did it!
We wrote an awesome song!

119
00:05:23,538 --> 00:05:25,439
We make a great brother
and sister team,

120
00:05:25,473 --> 00:05:27,174
like Andy and
Lana Wachowski.

121
00:05:27,208 --> 00:05:28,542
Should we hug?

122
00:05:28,576 --> 00:05:30,577
Mm, how about a fist
bump through a towel?

123
00:05:30,611 --> 00:05:32,612
That works.

124
00:05:36,517 --> 00:05:38,518
(laughs)

125
00:05:38,553 --> 00:05:40,087
I saved us seats.

126
00:05:40,121 --> 00:05:42,122
Oh, thank you, Homie.

127
00:05:43,925 --> 00:05:46,026
Thank you, roll of masking tape.

128
00:05:48,096 --> 00:05:50,330
Uh, gonna be a tight fit.

129
00:05:50,365 --> 00:05:52,532
(grunting)

130
00:05:55,670 --> 00:05:56,803
(sighs)

131
00:05:58,172 --> 00:06:01,842
♪ Springfield's the only home
that we've got ♪

132
00:06:01,876 --> 00:06:05,212
♪ But to be frank,
there's not a lot ♪

133
00:06:05,246 --> 00:06:08,515
♪ To recommend it ♪

134
00:06:08,549 --> 00:06:13,120
♪ We've got a
big sinkhole ♪

135
00:06:13,154 --> 00:06:17,557
♪ And they shut down
the think hole ♪

136
00:06:17,592 --> 00:06:20,360
♪ Hashtag: SpringfieldPride ♪

137
00:06:20,395 --> 00:06:23,130
♪ Has never ever trended... ♪

138
00:06:23,164 --> 00:06:24,898
This is not boosterism!

139
00:06:24,932 --> 00:06:26,433
Warm up the horse.

140
00:06:26,467 --> 00:06:30,604
♪ But when you think
of the things we lack ♪

141
00:06:30,638 --> 00:06:34,408
♪ 'Stead of
the stuff we've got ♪

142
00:06:34,442 --> 00:06:37,244
♪ Why Springfield? ♪

143
00:06:37,278 --> 00:06:40,647
♪ Why not? ♪

144
00:06:42,116 --> 00:06:45,552
♪ We've only had
a hurricane once ♪

145
00:06:45,586 --> 00:06:49,289
♪ We haven't had
a circus fire in months ♪

146
00:06:49,323 --> 00:06:52,125
♪ Springfield ♪

147
00:06:52,160 --> 00:06:56,797
♪ Why not? ♪

148
00:06:56,831 --> 00:07:00,567
♪ Ooh, we're just
off the interstate ♪

149
00:07:00,601 --> 00:07:04,504
♪ Ah, the second
right off exit 8... ♪

150
00:07:04,539 --> 00:07:05,806
He didn't
blow his line.

151
00:07:05,840 --> 00:07:07,808
Sarah, let's
have another!

152
00:07:07,842 --> 00:07:11,611
♪ You may find
our culture lacking ♪

153
00:07:11,646 --> 00:07:15,582
♪ We finally outlawed
our snake whacking ♪

154
00:07:15,616 --> 00:07:18,652
♪ Sure, our cops
are easily bought ♪

155
00:07:18,686 --> 00:07:22,255
♪ And our dentists
are all self-taught ♪

156
00:07:22,290 --> 00:07:26,526
♪ But hooray for Springfield ♪

157
00:07:26,561 --> 00:07:28,462
♪ Give two cheers ♪

158
00:07:28,496 --> 00:07:31,965
♪ Smallpox-free
for seven years ♪

159
00:07:31,999 --> 00:07:34,835
♪ Why Springfield? ♪

160
00:07:34,869 --> 00:07:38,405
♪ Why not? ♪

161
00:07:38,439 --> 00:07:39,973
(crowd cheering)

162
00:07:40,007 --> 00:07:41,641
What a song!

163
00:07:41,676 --> 00:07:46,112
I feel as if I'm in the lobby
of the Brill Building!

164
00:07:46,147 --> 00:07:48,014
(cheering continues)

165
00:07:50,418 --> 00:07:53,553
Homer, it's a standing
ovation. Get up.

166
00:07:53,588 --> 00:07:56,056
Our kids just did
something amazing.

167
00:07:56,090 --> 00:07:57,691
Well, not Maggie.

168
00:07:59,393 --> 00:08:01,661
Get up!
Okay, okay.

169
00:08:01,696 --> 00:08:03,530
Standing "O" or die!

170
00:08:03,564 --> 00:08:06,032
(grunting loudly)

171
00:08:09,704 --> 00:08:10,704
Woo-hoo!

172
00:08:10,705 --> 00:08:12,372
(laughter)

173
00:08:15,610 --> 00:08:16,743
Stop laughing at me!

174
00:08:16,777 --> 00:08:17,844
(crowd screams)

175
00:08:19,413 --> 00:08:20,413
(groans)

176
00:08:20,448 --> 00:08:22,082
(screams)

177
00:08:22,116 --> 00:08:23,650
(sighs)

178
00:08:23,684 --> 00:08:26,052
Occupied.

179
00:08:28,623 --> 00:08:30,423
(crowd screaming)

180
00:08:30,458 --> 00:08:32,893
Stop fearing me!

181
00:08:32,927 --> 00:08:34,427
(sobbing)

182
00:08:37,732 --> 00:08:39,132
Damn it!

183
00:08:42,236 --> 00:08:44,004
Please keep
spinning it.

184
00:08:44,038 --> 00:08:45,672
I don't want
to look at it.

185
00:08:45,706 --> 00:08:48,608
Can't you say something
to make me feel better?

186
00:08:48,643 --> 00:08:50,210
(sighs)

187
00:08:50,244 --> 00:08:52,412
I'm sorry, but I can't.

188
00:08:52,446 --> 00:08:55,549
I'm tired of you saying
planes have gotten smaller

189
00:08:55,583 --> 00:08:58,718
and two presidents
were fatter than you, and...

190
00:08:58,753 --> 00:08:59,753
(sighs)

191
00:08:59,754 --> 00:09:01,254
Fine, I got it.

192
00:09:01,289 --> 00:09:03,590
Starting right now, a
yearlong juice cleanse.

193
00:09:03,624 --> 00:09:05,425
Every morning,
I get a colonic,

194
00:09:05,459 --> 00:09:07,961
and I sleep in a
sauna every night.

195
00:09:07,995 --> 00:09:11,298
Oh, that's not
a healthy way to lose weight.

196
00:09:11,332 --> 00:09:15,135
It's not about health, Marge,
it's about going crazy.

197
00:09:15,169 --> 00:09:17,037
You don't have to
do this alone.

198
00:09:17,071 --> 00:09:19,105
What the...?

199
00:09:19,140 --> 00:09:20,140
Hmm.

200
00:09:33,921 --> 00:09:36,189
(sighs)
This is it.

201
00:09:36,223 --> 00:09:41,194
My name is Roy, and this
week I gained seven pounds.

202
00:09:42,263 --> 00:09:43,597
Yes!

203
00:09:43,631 --> 00:09:44,898
No shame there!

204
00:09:44,932 --> 00:09:46,833
You go, girth!

205
00:09:46,867 --> 00:09:48,835
Huh? Oh, I'm sorry.

206
00:09:48,869 --> 00:09:51,605
I thought you were
Over-feeders Anonymous.

207
00:09:51,639 --> 00:09:53,673
No, no. In fact,
quite the opposite.

208
00:09:53,708 --> 00:09:55,675
We are Big Is
Beautiful.

209
00:09:55,710 --> 00:09:58,445
So you guys are proud
of what you are?

210
00:09:58,479 --> 00:10:01,982
We do not cower in the shadows,
we make the shadows.

211
00:10:02,016 --> 00:10:03,350
Do you guys serve snacks?

212
00:10:03,384 --> 00:10:04,818
We've got ice
cream hoagies,

213
00:10:04,852 --> 00:10:07,621
deep-fried sugar bags,
Dorsnickos, Snickeritos,

214
00:10:07,655 --> 00:10:11,157
Milky Snicks, Porksicles,
and of course diet soda.

215
00:10:11,192 --> 00:10:12,659
Oh, got to have diet soda.
Oh, yeah, of course.

216
00:10:12,693 --> 00:10:14,494
Yes, mustn't forget
the diet soda.

217
00:10:14,528 --> 00:10:16,630
I have so many questions
for you.

218
00:10:16,664 --> 00:10:18,665
First of all,
is this floor reinforced?

219
00:10:20,004 --> 00:10:22,316
You know,
maybe I shouldn't be here.

220
00:10:22,317 --> 00:10:24,718
I promised my wife
I'd lose weight.

221
00:10:24,753 --> 00:10:25,753
Huh?

222
00:10:25,754 --> 00:10:26,987
MAN:
If I may.

223
00:10:28,123 --> 00:10:29,556
Ooh, labels.

224
00:10:29,591 --> 00:10:33,627
Our loved ones, though precious,
can often be a hurdle.

225
00:10:33,662 --> 00:10:35,562
Who are you?
I'm the founder

226
00:10:35,597 --> 00:10:37,431
of our little
group, Albert.

227
00:10:37,465 --> 00:10:39,133
Ooh, like Fat Albert!

228
00:10:39,167 --> 00:10:41,568
We never use
the word "fat" here.

229
00:10:41,603 --> 00:10:43,804
Sorry.
My dear fat friend,

230
00:10:43,838 --> 00:10:46,540
all your wife needs
is a little education.

231
00:10:46,574 --> 00:10:47,808
Now repeat after me.

232
00:10:47,842 --> 00:10:49,243
I am big.

233
00:10:49,277 --> 00:10:50,577
I am big.

234
00:10:50,612 --> 00:10:52,446
I am beautiful.

235
00:10:52,480 --> 00:10:55,349
I have a beauty
of a sort to some.

236
00:10:55,383 --> 00:10:58,585
No one can make me feel bad
about who I am

237
00:10:58,620 --> 00:11:01,689
because this is who I am!

238
00:11:01,723 --> 00:11:02,556
Ditto!

239
00:11:02,590 --> 00:11:04,291
You know,
I've always wanted

240
00:11:04,326 --> 00:11:06,093
to blindly follow somebody,

241
00:11:06,127 --> 00:11:09,129
and I think
you just might be the guy.

242
00:11:09,164 --> 00:11:10,631
Marge, kids,

243
00:11:10,665 --> 00:11:13,634
tonight was a turning
point in my life.

244
00:11:13,668 --> 00:11:15,002
Well, that's great, Homie.

245
00:11:15,036 --> 00:11:17,237
Just let me finish
uploading this photo.

246
00:11:17,272 --> 00:11:18,472
HOMER:
No, Marge,

247
00:11:18,506 --> 00:11:19,807
you have to listen!

248
00:11:19,841 --> 00:11:22,910
That place you sent me to
changed my life.

249
00:11:22,944 --> 00:11:24,244
Thank you, Lord!

250
00:11:24,279 --> 00:11:26,613
And I'll give up
online scrapbooking,

251
00:11:26,648 --> 00:11:27,881
like I promised.

252
00:11:29,117 --> 00:11:31,452
They taught me that I don't
need to lose weight.

253
00:11:31,486 --> 00:11:33,620
I should just be proud
of what I am.

254
00:11:33,655 --> 00:11:34,855
What?

255
00:11:34,889 --> 00:11:36,590
Restore, restore!

256
00:11:36,624 --> 00:11:38,592
(buzzer sounds)

257
00:11:38,626 --> 00:11:40,027
I met a guy...

258
00:11:40,061 --> 00:11:41,662
a wonderful guy...

259
00:11:41,696 --> 00:11:43,297
Oh, Lord.

260
00:11:43,331 --> 00:11:44,531
It's not what
you think.

261
00:11:44,566 --> 00:11:45,833
His name is Albert,

262
00:11:45,867 --> 00:11:48,235
and he taught me
not to buy into the lies

263
00:11:48,269 --> 00:11:50,237
taught by Big Nutrition.

264
00:11:50,271 --> 00:11:52,306
That's worse than
what I thought.

265
00:11:52,340 --> 00:11:55,776
Marge, you're my wife
of ten years and I love you,

266
00:11:55,810 --> 00:11:58,912
but I must observe
the teachings of this man

267
00:11:58,947 --> 00:12:00,581
I just met tonight.

268
00:12:00,615 --> 00:12:03,484
Now, the first thing
I have to do is make amends.

269
00:12:03,518 --> 00:12:05,419
With the bathroom scale.

270
00:12:05,453 --> 00:12:08,155
I'm so sorry
I threw you across the room

271
00:12:08,189 --> 00:12:09,823
and called you a liar.

272
00:12:09,858 --> 00:12:12,593
When you told me
I was 260 pounds,

273
00:12:12,627 --> 00:12:14,862
you were just encouraging me.

274
00:12:14,896 --> 00:12:18,132
It was a poem you were writing
about my potential.

275
00:12:18,166 --> 00:12:20,534
I'm gonna start celebrating
my size,

276
00:12:20,568 --> 00:12:22,870
and I need you with me
as a partner.

277
00:12:22,904 --> 00:12:24,304
Now, if you'll excuse me,

278
00:12:24,339 --> 00:12:26,940
I need to talk
to the broken porch swing.

279
00:12:26,975 --> 00:12:28,642
HOMER:
<i>So, from now on,</i>

280
00:12:28,676 --> 00:12:31,345
<i>you guys can no longer say</i>
<i>these hateful words:</i>

281
00:12:31,379 --> 00:12:33,647
"Chubby, chunky, blobbo, slobbo,

282
00:12:33,681 --> 00:12:37,151
"fat bastard, Michelin Man,
Stay Puft, Chumbawumba,

283
00:12:37,185 --> 00:12:38,519
"It is balloon!

284
00:12:38,553 --> 00:12:40,654
"Papa Grande, Augustus Gloop,

285
00:12:40,688 --> 00:12:43,123
"beached whale, big-boned,
Wisconsin skinny,

286
00:12:43,158 --> 00:12:46,527
"butterball, dump truck,
"Jelly Belly, pudgy-wudgy,

287
00:12:46,561 --> 00:12:48,796
"lard-ass, blubberino,
Buddha belly,

288
00:12:48,830 --> 00:12:50,664
"Hurry Eat Tubman,
one-ton soup,

289
00:12:50,698 --> 00:12:53,400
"Blob Saget, Chub Hub,
Calvin Cool Whip,

290
00:12:53,435 --> 00:12:55,636
"Manfred Mannboobs,
21 Lump Street,

291
00:12:55,670 --> 00:12:58,639
"walking 'before' picture,
fatso, Harvey Milk Chocolate,

292
00:12:58,673 --> 00:13:01,642
"Obese Want Cannoli,
Mahatma Gumbo,

293
00:13:01,676 --> 00:13:04,011
"Salvador Deli, Elmer Pantry,

294
00:13:04,045 --> 00:13:07,915
"KFC and the Sponge Cake Band,
Snackie Onassis,

295
00:13:07,949 --> 00:13:09,817
"The Foody Blues,
Hoagie Carmichael

296
00:13:09,851 --> 00:13:11,051
and wide load."

297
00:13:11,085 --> 00:13:12,820
What about
Mr. Two Belts?

298
00:13:12,854 --> 00:13:14,054
Good, good!

299
00:13:14,088 --> 00:13:16,323
By which I mean,
"Bad, bad!"

300
00:13:16,357 --> 00:13:18,091
You know, as long as
we're opening this up--

301
00:13:18,126 --> 00:13:19,493
and I'm glad you are--

302
00:13:19,527 --> 00:13:22,229
I want to tell you guys that
when you call me a gargoyle,

303
00:13:22,263 --> 00:13:25,999
a troll or a homunculus,
it kind of hurts my feelings.

304
00:13:26,034 --> 00:13:27,401
What? You're kidding.

305
00:13:27,435 --> 00:13:28,769
We never dreamed.

306
00:13:28,803 --> 00:13:30,637
Who knew goblins
had feelings?

307
00:13:30,672 --> 00:13:32,506
You see? That's what
I'm talking about. Because...

308
00:13:32,540 --> 00:13:34,575
CROWD (chanting):
We're big! We're proud!

309
00:13:34,609 --> 00:13:36,577
Two of us can make a crowd!

310
00:13:36,611 --> 00:13:39,947
Sorry, fellas. There's
a movement I have to join.

311
00:13:39,981 --> 00:13:43,350
I've learned there's something
more important than drinking:

312
00:13:43,384 --> 00:13:45,319
eating.

313
00:13:45,353 --> 00:13:49,056
Thank you for the
easy financing!

314
00:13:49,090 --> 00:13:50,824
What the...?!

315
00:13:50,859 --> 00:13:53,861
Your store is forcing
unattainable body images

316
00:13:53,895 --> 00:13:55,262
on our young people.

317
00:13:55,296 --> 00:13:58,432
I say, end the thinsanity!

318
00:13:58,466 --> 00:14:00,367
You, my friend, are wrong.

319
00:14:00,401 --> 00:14:04,404
Every girl should look like
a sexy praying mantis from Milan

320
00:14:04,439 --> 00:14:05,939
whose hips are narrower

321
00:14:05,974 --> 00:14:08,609
than an Italian
parliamentary majority.

322
00:14:08,643 --> 00:14:09,943
Whew.

323
00:14:09,978 --> 00:14:12,179
(siren whoops)

324
00:14:12,213 --> 00:14:13,981
Okay, people,
show's over.

325
00:14:14,015 --> 00:14:15,349
Nothing to
eat here.

326
00:14:15,383 --> 00:14:16,617
Now move along.

327
00:14:16,651 --> 00:14:18,018
If you can.

328
00:14:18,052 --> 00:14:20,187
If not, we're gonna
have to take you in.

329
00:14:20,221 --> 00:14:22,122
Clancy, what are you doing?

330
00:14:22,156 --> 00:14:23,357
You're one of us!

331
00:14:23,391 --> 00:14:24,825
You're right.

332
00:14:24,859 --> 00:14:27,094
Take me in, Lou.

333
00:14:27,128 --> 00:14:28,662
Ow, ah.

334
00:14:28,696 --> 00:14:29,863
(grunting)

335
00:14:29,898 --> 00:14:30,964
Hey! Why?

336
00:14:30,999 --> 00:14:33,333
Ah, geez, Lou.

337
00:14:33,368 --> 00:14:34,835
You seem to be enjoying this.

338
00:14:34,869 --> 00:14:36,670
Just doing
my job, Chief.

339
00:14:36,704 --> 00:14:37,771
(Taser crackles)

340
00:14:37,805 --> 00:14:39,306
Oh, that's some nice
Tase work, Lou.

341
00:14:39,340 --> 00:14:41,341
(grunting)

342
00:14:43,745 --> 00:14:45,012
Homer Simpson?

343
00:14:45,046 --> 00:14:46,213
HOMER:
Right here!

344
00:14:46,247 --> 00:14:47,814
Excuse me. Pardon me.

345
00:14:47,849 --> 00:14:49,316
Coming through.

346
00:14:49,350 --> 00:14:50,817
Coming back. Excuse me.

347
00:14:50,852 --> 00:14:52,185
Pardon me. There we go.

348
00:14:52,220 --> 00:14:53,453
Uh, coming up.

349
00:14:53,488 --> 00:14:54,688
What can I do for you?

350
00:14:54,722 --> 00:14:55,989
You're free
on bail.

351
00:14:57,825 --> 00:15:00,127
People, give thanks
to that woman,

352
00:15:00,161 --> 00:15:03,363
standing up for all of us,
even though she's a lighty.

353
00:15:03,398 --> 00:15:04,665
Thank you.

354
00:15:04,699 --> 00:15:06,333
Although, I think
the worst thing

355
00:15:06,367 --> 00:15:09,536
that ever happened to my husband
was joining your group.

356
00:15:09,571 --> 00:15:10,571
(crowd gasps, murmurs)

357
00:15:10,605 --> 00:15:11,605
(clears throat)

358
00:15:11,639 --> 00:15:13,006
I'll handle this.

359
00:15:13,041 --> 00:15:16,343
What is the point of a long life
if it's not enjoyed?

360
00:15:16,377 --> 00:15:17,544
(drooling moan)

361
00:15:17,579 --> 00:15:19,446
Homer, you have to choose.

362
00:15:19,480 --> 00:15:21,982
And I think
the choice is clear.

363
00:15:30,558 --> 00:15:32,993
Guys, I'm coming
back in!

364
00:15:33,027 --> 00:15:34,428
Suck in your guts.

365
00:15:34,462 --> 00:15:36,697
(all inhaling)

366
00:15:36,731 --> 00:15:39,032
(sighs) Home.

367
00:15:39,067 --> 00:15:40,167
(groans)

368
00:15:44,851 --> 00:15:46,218
(laughing)

369
00:15:46,252 --> 00:15:48,887
Whee! Whee!

370
00:15:48,921 --> 00:15:51,289
Who's he giving
a piggyback ride to?

371
00:15:51,324 --> 00:15:52,557
We don't know.

372
00:15:52,592 --> 00:15:54,559
Mom, what's wrong?

373
00:15:54,594 --> 00:15:56,128
How come Dad's
not with you?

374
00:15:56,162 --> 00:15:58,497
Are they setting bail
by the pound? (laughs)

375
00:15:58,531 --> 00:16:02,234
He chose to spend the night
in a jail with strangers

376
00:16:02,268 --> 00:16:03,468
instead of me.

377
00:16:03,503 --> 00:16:04,836
Mom, Lisa and I learned

378
00:16:04,871 --> 00:16:07,673
that we can solve any problem
through song.

379
00:16:07,707 --> 00:16:10,842
Lisa, let's go write something
that'll change Dad's mind.

380
00:16:10,877 --> 00:16:12,044
Do you really think

381
00:16:12,078 --> 00:16:13,945
that we can write a song
that does that?

382
00:16:13,980 --> 00:16:15,781
I just wanted to get
out of the room.

383
00:16:15,815 --> 00:16:17,683
She was really
bumming me out.

384
00:16:20,453 --> 00:16:21,720
Ah, geez.

385
00:16:21,754 --> 00:16:23,789
I thought writing another
hit song would be easier.

386
00:16:23,823 --> 00:16:25,157
Well, it
would've helped

387
00:16:25,191 --> 00:16:26,825
if you hadn't crumpled
up all the paper

388
00:16:26,859 --> 00:16:29,594
before we wrote
anything on it.

389
00:16:29,629 --> 00:16:30,996
No more judgment!

390
00:16:31,030 --> 00:16:32,364
No more jokes!

391
00:16:32,398 --> 00:16:35,033
We will not be made
to feel less than

392
00:16:35,068 --> 00:16:38,136
because we are greater than!

393
00:16:38,171 --> 00:16:40,439
I was distinctly
promised no math!

394
00:16:40,473 --> 00:16:44,509
Homer, I want to say something
extremely important

395
00:16:44,544 --> 00:16:47,646
the way a woman does--
subtly, through someone else.

396
00:16:47,680 --> 00:16:48,880
Kids...

397
00:16:48,915 --> 00:16:51,550
♪ ♪

398
00:16:51,584 --> 00:16:53,485
Take it, Bart!
You take it!

399
00:16:53,519 --> 00:16:54,853
I gave it to you!

400
00:16:54,887 --> 00:16:56,188
I refuse
to accept it!

401
00:16:56,222 --> 00:16:57,789
LENNY:
Just start the song!

402
00:16:57,824 --> 00:16:59,124
We couldn't write a song.

403
00:16:59,158 --> 00:17:00,692
We're one-hit wonders.

404
00:17:00,727 --> 00:17:02,127
But is that such a crime?

405
00:17:02,161 --> 00:17:03,695
Look at J.D. Salinger.

406
00:17:03,730 --> 00:17:04,996
<i>Franny and Zooey.</i>

407
00:17:05,031 --> 00:17:06,131
Rubik's Cube!

408
00:17:06,165 --> 00:17:07,299
Rubik's Snake!

409
00:17:07,333 --> 00:17:09,401
Charles M. Schulz.

410
00:17:09,435 --> 00:17:10,569
You've got me there.

411
00:17:10,603 --> 00:17:11,837
It's her fault.

412
00:17:11,871 --> 00:17:13,772
She was sucking all the gangsta
out of everything.

413
00:17:13,806 --> 00:17:15,440
Well, you're the only
"gangsta" I know

414
00:17:15,475 --> 00:17:17,175
with a 9:00 bedtime.

415
00:17:17,210 --> 00:17:19,811
9:30, starting in summer.
Mom said.

416
00:17:19,846 --> 00:17:21,580
Ah, kids,
stop arguing.

417
00:17:21,614 --> 00:17:23,482
Or keep arguing.

418
00:17:23,516 --> 00:17:24,850
I don't care.

419
00:17:24,884 --> 00:17:28,153
Homer, I'm sorry I tried
to help you control your weight.

420
00:17:28,187 --> 00:17:30,655
(sighs) I'm not sure
of anything anymore.

421
00:17:30,690 --> 00:17:32,858
Sorry I ever
opened my mouth.

422
00:17:32,892 --> 00:17:34,559
Marge, it's not
your fault

423
00:17:34,594 --> 00:17:37,796
that you can't win against
a superior being like Albert.

424
00:17:37,830 --> 00:17:39,131
(slurps)

425
00:17:40,166 --> 00:17:41,233
Mmm.

426
00:17:41,267 --> 00:17:42,734
But, Marge, I
don't want you

427
00:17:42,769 --> 00:17:44,369
to ever stop
caring about me.

428
00:17:44,403 --> 00:17:45,670
How can you
follow a leader

429
00:17:45,705 --> 00:17:47,539
who won't even get
up out of his chair?

430
00:17:47,573 --> 00:17:49,241
Marge, I believe
you're forgetting

431
00:17:49,275 --> 00:17:53,078
America's greatest wartime
wheelchair-bound leader:

432
00:17:53,112 --> 00:17:55,347
Professor X
of the X-Men.

433
00:17:55,381 --> 00:17:58,016
It's not that Professor X
wouldn't get up,

434
00:17:58,050 --> 00:17:59,518
it's that he couldn't!

435
00:17:59,552 --> 00:18:02,420
Well, I'll show you
who can get up!

436
00:18:02,455 --> 00:18:04,790
(triumphant music playing)

437
00:18:07,460 --> 00:18:10,095
(footsteps thudding heavily)

438
00:18:11,964 --> 00:18:14,199
(roars)

439
00:18:14,233 --> 00:18:15,433
That's right!

440
00:18:15,468 --> 00:18:17,035
I don't need
this scooter!

441
00:18:17,069 --> 00:18:18,270
All of you,

442
00:18:18,304 --> 00:18:20,939
follow me to the future!

443
00:18:20,973 --> 00:18:22,641
(groans)

444
00:18:24,143 --> 00:18:25,710
Oh, dear God.

445
00:18:25,745 --> 00:18:27,879
Stick a forklift
in him, he's dead.

446
00:18:27,914 --> 00:18:29,314
(laughs)

447
00:18:30,716 --> 00:18:32,717
(organ playing solemn music)

448
00:18:45,598 --> 00:18:47,465
I've never written
a eulogy before,

449
00:18:47,500 --> 00:18:49,467
but this time, I did.

450
00:18:49,502 --> 00:18:53,071
Unfortunately,
I left it at home.

451
00:18:53,105 --> 00:18:54,873
Thanks, lady.

452
00:18:54,907 --> 00:18:58,410
What Albert taught us
is that all people have pride

453
00:18:58,444 --> 00:19:01,479
and no group should ever
be insulted.

454
00:19:01,514 --> 00:19:02,914
He knew what
was important

455
00:19:02,949 --> 00:19:05,917
was how you lived
your life every day.

456
00:19:05,952 --> 00:19:08,486
Till he was taken
from us too soon,

457
00:19:08,521 --> 00:19:10,522
at the age of...

458
00:19:10,556 --> 00:19:11,790
(whispering)

459
00:19:11,824 --> 00:19:13,158
Twenty-three?!

460
00:19:13,192 --> 00:19:15,760
People, for God's sake,
join a gym!

461
00:19:15,795 --> 00:19:17,229
(crowd murmuring)

462
00:19:17,263 --> 00:19:20,332
Kumiko, would you still love me
if I lost weight?

463
00:19:20,366 --> 00:19:22,334
Much more!
(sighs)

464
00:19:22,368 --> 00:19:24,669
Marge, I'm sorry
I was proud of myself.

465
00:19:24,704 --> 00:19:26,304
That's not really what I was...

466
00:19:26,339 --> 00:19:28,039
It will never
happen again!

467
00:19:28,074 --> 00:19:30,408
Now come on.
Let's walk home.

468
00:19:30,443 --> 00:19:32,410
Absolutely.

469
00:19:32,445 --> 00:19:33,979
Can I ask you
something?

470
00:19:34,013 --> 00:19:36,147
What is it that
keeps you with me?

471
00:19:36,182 --> 00:19:38,917
It's because
everything you love,

472
00:19:38,951 --> 00:19:41,286
you love so much.

473
00:19:41,320 --> 00:19:42,687
Because you love me,

474
00:19:42,722 --> 00:19:46,258
I will not stop yo-yo dieting
till I get it right.

475
00:19:46,292 --> 00:19:47,492
Oh...

476
00:20:05,144 --> 00:20:06,544
At last.

477
00:20:06,579 --> 00:20:07,979
Well done, Dad.

478
00:20:08,014 --> 00:20:10,715
You finally reached
emotional maturity.

479
00:20:10,750 --> 00:20:12,183
Unlike Bart.

480
00:20:12,786 --> 00:20:14,474
Bart's very mature.

481
00:20:14,474 --> 00:20:16,431
Take it from the little
boy in his tummy.

482
00:20:16,432 --> 00:20:18,850
Now, when do I get
to be the head again?

483
00:20:18,850 --> 00:20:22,120
Soon.
It's always "soon."

484
00:20:27,579 --> 00:20:29,617
Whoa!

485
00:20:29,618 --> 00:20:31,252
(horse neighs)

486
00:20:31,285 --> 00:20:34,788
Oh. I wish I had
said that earlier.

487
00:20:34,822 --> 00:20:41,322
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

488
00:21:20,646 --> 00:21:21,935
Shh!

