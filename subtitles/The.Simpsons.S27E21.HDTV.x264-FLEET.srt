﻿1
00:00:01,631 --> 00:00:05,631
<b><font color="#00FF00">♪ The Simpsons 27x21 ♪</font></b>
<font color="#00FFFF">Simprovised</font>
Original Air Date on May 15, 2016

2
00:00:05,632 --> 00:00:09,000
WIGGUM: <i>Yeah, I</i> know <i>Ralphie's birthday</i>
<i>is coming up.</i>

3
00:00:09,036 --> 00:00:11,002
Of course
I'm gonna get him a present.

4
00:00:11,038 --> 00:00:14,172
I'm at the...
at the toy store right now.

5
00:00:14,207 --> 00:00:16,474
Let's see,
present for Ralph,

6
00:00:16,510 --> 00:00:18,043
present for Ralph...

7
00:00:18,078 --> 00:00:19,844
Ooh, what do we have here?

8
00:00:19,880 --> 00:00:22,814
Boy, Ralph would kill himself
with this in two seconds.

9
00:00:22,849 --> 00:00:24,783
And somehow it'd be <i>my</i> fault.

10
00:00:24,818 --> 00:00:26,117
Mm!

11
00:00:27,487 --> 00:00:29,955
This money has been sitting here
since 1998,

12
00:00:29,990 --> 00:00:32,157
not doing anybody any good.

13
00:00:32,192 --> 00:00:34,192
I want to get Ralphie
something nice.

14
00:00:34,227 --> 00:00:37,062
Yeah, a few thousand should do.
Hm-mm.

15
00:00:38,665 --> 00:00:41,433
(whistling a tune)

16
00:00:41,468 --> 00:00:42,867
Uh, you heard me
whistling there, right?

17
00:00:42,903 --> 00:00:44,769
That indicates innocence.

18
00:00:44,805 --> 00:00:47,639
Uh, proclaiming your innocence
indicates guilt.

19
00:00:47,674 --> 00:00:49,207
Uh, yeah?
Well, what does this mean?

20
00:00:49,242 --> 00:00:51,042
♪ Skiddily bop and bah! ♪

21
00:00:51,078 --> 00:00:52,844
(whooping)

22
00:00:52,879 --> 00:00:55,447
(engine revving,
tires squealing)

23
00:00:55,482 --> 00:00:57,048
(kids shouting playfully,
noisemaker honking)

24
00:00:57,084 --> 00:00:59,718
Okay, so, at a Ralph party,
always get to the cake before...

25
00:00:59,753 --> 00:01:00,518
Too late.

26
00:01:00,554 --> 00:01:01,920
(both gasp)

27
00:01:03,357 --> 00:01:06,591
HOMER: That's the best damn
treehouse I've ever seen!

28
00:01:08,128 --> 00:01:10,895
(heavenly music plays)

29
00:01:10,931 --> 00:01:13,198
Fine. I'll rub my eyes
the other way.

30
00:01:14,267 --> 00:01:16,568
(inspiring orchestral
fanfare plays)

31
00:01:16,603 --> 00:01:17,669
D'oh!

32
00:01:17,704 --> 00:01:20,105
(lively chatter, laughing)

33
00:01:22,242 --> 00:01:23,842
Whee!

34
00:01:23,877 --> 00:01:25,276
Whee...!

35
00:01:26,346 --> 00:01:27,346
Whoa!

36
00:01:27,381 --> 00:01:29,447
The September issue!

37
00:01:33,553 --> 00:01:36,421
(gentle classical music plays)

38
00:01:45,866 --> 00:01:48,233
Whoo! Free wood!

39
00:01:49,536 --> 00:01:50,536
(Homer whoops)

40
00:01:50,570 --> 00:01:53,605
(groans): Oh...
my treehouse sucks.

41
00:01:53,640 --> 00:01:54,706
Haw-haw!

42
00:01:54,741 --> 00:01:56,808
(singsongy):
You have class envy!

43
00:01:56,843 --> 00:01:57,742
(slurring):
Nelson, honey,

44
00:01:57,778 --> 00:02:00,445
I told you to stop
sayin' "haw-haw."

45
00:02:00,480 --> 00:02:02,280
Give me a dollar
and I'll stop.

46
00:02:02,315 --> 00:02:04,082
I don't got a dollar.

47
00:02:04,117 --> 00:02:05,550
Haw-haw!

48
00:02:05,585 --> 00:02:06,918
(Homer hums happily)

49
00:02:06,953 --> 00:02:09,120
Do you want to practice
your speech on me?

50
00:02:09,156 --> 00:02:11,022
(chuckles)
No need, honey.

51
00:02:11,058 --> 00:02:12,724
Same speech
I give every year.

52
00:02:12,759 --> 00:02:15,927
The opening joke about
Lenny's grandma always kills.

53
00:02:15,962 --> 00:02:17,295
You can't joke about her--

54
00:02:17,330 --> 00:02:19,230
they just put her
on life support.

55
00:02:19,266 --> 00:02:21,599
What?! No!
She's the linchpin!

56
00:02:21,635 --> 00:02:23,601
It's okay, it's okay.

57
00:02:23,637 --> 00:02:26,871
Everyone is terrified
of public speaking.

58
00:02:26,907 --> 00:02:29,674
But just in case, I'll
defrost a failure ham.

59
00:02:29,710 --> 00:02:33,611
(dramatic musical stinger)

60
00:02:33,647 --> 00:02:36,114
You don't have faith in me!
I have savers.

61
00:02:36,149 --> 00:02:38,917
If I get heckled, I'll say,
"Get a half-life!"

62
00:02:40,654 --> 00:02:43,254
I'm sure that'll
be funny to them.

63
00:02:43,290 --> 00:02:44,789
Mm! (kisses)

64
00:02:44,825 --> 00:02:46,124
(whimpers)

65
00:02:46,159 --> 00:02:48,059
Calm down, Homer.

66
00:02:48,095 --> 00:02:49,761
Just leave your body.

67
00:02:50,964 --> 00:02:52,530
Oh, no!

68
00:02:55,869 --> 00:02:58,236
What the hell is this?!

69
00:02:58,271 --> 00:03:00,105
(humming happily)

70
00:03:00,140 --> 00:03:01,306
(startled grunt)

71
00:03:01,341 --> 00:03:04,409
Ooh, throwing away
your "No Girls" sign?

72
00:03:04,444 --> 00:03:07,512
Is it time for you to have
"the talk" with your dad?

73
00:03:07,547 --> 00:03:10,615
Because he's gonna have
to read a few things first.

74
00:03:10,650 --> 00:03:12,150
No. I'm tearing it down.

75
00:03:12,185 --> 00:03:14,686
Ralph has a cool treehouse,
and mine sucks.

76
00:03:14,721 --> 00:03:17,021
Well, Bart, your
father built it,

77
00:03:17,057 --> 00:03:19,524
and he did the
very best he could.

78
00:03:19,559 --> 00:03:20,925
(grunting)

79
00:03:20,961 --> 00:03:23,595
I did <i>my</i> job.
Now it's your turn, tree.

80
00:03:23,630 --> 00:03:25,697
Get growing.

81
00:03:25,732 --> 00:03:29,300
I'll tell you what, why don't
I spruce it up for you. Huh?

82
00:03:29,336 --> 00:03:31,302
But you're
an inside grown-up.

83
00:03:31,338 --> 00:03:33,505
Moms can't build
treehouses.

84
00:03:33,540 --> 00:03:37,642
You realize that saying that is gonna
make this mom work her keister off

85
00:03:37,677 --> 00:03:40,979
to make you the best darn
treehouse you ever saw!

86
00:03:42,182 --> 00:03:44,015
Sounds good.

87
00:03:44,050 --> 00:03:45,283
(Marge screams)

88
00:03:45,318 --> 00:03:46,384
Don't worry.

89
00:03:46,419 --> 00:03:48,153
You forget, most of that's hair.

90
00:03:48,188 --> 00:03:49,487
BURNS (chuckles):
<i>So I said,</i>

91
00:03:49,523 --> 00:03:51,022
"Get a half-life!"

92
00:03:51,057 --> 00:03:52,090
(laughter)

93
00:03:57,097 --> 00:03:58,296
(chuckles)

94
00:03:58,331 --> 00:03:59,697
Funny and handsome.

95
00:03:59,733 --> 00:04:01,232
And I hear he's loaded.

96
00:04:01,268 --> 00:04:02,667
(chuckles):
Yes.

97
00:04:02,702 --> 00:04:05,170
Now, to end this perfect
day on the perfect note,

98
00:04:05,205 --> 00:04:07,438
Mr. Homer Simpson.

99
00:04:08,508 --> 00:04:11,743
(footsteps echoing)

100
00:04:15,715 --> 00:04:18,016
(footsteps echoing louder)

101
00:04:20,720 --> 00:04:22,153
(man coughs)

102
00:04:22,189 --> 00:04:23,922
You're gonna do great, Homer.
You're gonna kill.

103
00:04:23,957 --> 00:04:26,958
You're gonna-- oh, where did
all these people come from?!

104
00:04:28,295 --> 00:04:29,828
(gunshot)
Aah!

105
00:04:29,863 --> 00:04:31,329
Uh, uh...

106
00:04:31,364 --> 00:04:34,699
Webster's Dictionary
defines... a speech

107
00:04:34,734 --> 00:04:39,204
as a... series of words that...

108
00:04:39,239 --> 00:04:41,840
(murmuring)
elo... quently...

109
00:04:41,875 --> 00:04:43,741
I've never seen anyone
bomb like that.

110
00:04:43,777 --> 00:04:46,211
Yeah. I really feel
badly for the guy.

111
00:04:46,246 --> 00:04:47,745
Boo!
Boo!

112
00:04:47,781 --> 00:04:50,148
(audience booing)

113
00:04:52,052 --> 00:04:53,718
(whimpering, shuddering)

114
00:04:53,753 --> 00:04:55,420
I'm a failure.

115
00:04:55,455 --> 00:04:56,921
Shall I release
the hounds, sir?

116
00:04:56,957 --> 00:04:59,891
Mm, the therapy hounds.

117
00:05:03,964 --> 00:05:06,231
Oh, oh, I feel a little better.

118
00:05:06,266 --> 00:05:08,099
Now release the real hounds.

119
00:05:08,134 --> 00:05:09,801
(dogs barking)

120
00:05:09,836 --> 00:05:11,135
(Homer screaming)

121
00:05:11,171 --> 00:05:12,611
It's a pretty good
seminar this year.

122
00:05:13,800 --> 00:05:16,576
Dad, what's wrong?
Did your speech go badly?

123
00:05:16,676 --> 00:05:18,076
How do you know
something's wrong?

124
00:05:18,111 --> 00:05:20,244
You're drinking
from a can of corn.

125
00:05:20,280 --> 00:05:21,879
Hmm? Eh.

126
00:05:24,818 --> 00:05:26,684
Aw, Dad, do you know
Barbra Streisand

127
00:05:26,720 --> 00:05:28,920
once forgot
the words to a song

128
00:05:28,955 --> 00:05:32,323
and didn't perform in public
again for nearly three decades?

129
00:05:32,358 --> 00:05:35,226
Yeah, but she still had
James Brolin to cuddle.

130
00:05:35,261 --> 00:05:38,329
So, on a scale of
ten to ten, how'd you do?

131
00:05:38,364 --> 00:05:39,897
Um...

132
00:05:39,933 --> 00:05:41,766
(water dripping)

133
00:05:44,370 --> 00:05:46,437
♪ Drip drop, you flop ♪

134
00:05:46,473 --> 00:05:48,506
♪ Drip drop, you flop ♪

135
00:05:48,541 --> 00:05:50,508
♪ Drip drop, you flop ♪

136
00:05:50,543 --> 00:05:52,610
♪ Drip drop, you flop ♪
♪ Fail, fail, fail ♪

137
00:05:52,645 --> 00:05:54,312
♪ Drip drop, you flop... ♪
♪ Fail, fail... ♪

138
00:05:54,347 --> 00:05:56,314
(squeaky voice): What the hell
is your problem, idiot!

139
00:05:56,349 --> 00:05:58,049
(kettle whistling,
Homer gasping)

140
00:05:58,084 --> 00:05:59,150
I don't know!

141
00:05:59,185 --> 00:06:01,552
(sobbing):
I don't know!

142
00:06:04,390 --> 00:06:06,023
(Homer whimpers)

143
00:06:06,059 --> 00:06:07,859
I know what will cheer you up.

144
00:06:07,894 --> 00:06:10,294
We'll go to the comedy club
downtown.

145
00:06:10,330 --> 00:06:13,464
Downtown? With all
those desperate addicts?

146
00:06:13,500 --> 00:06:16,367
Oh, the city cleaned them up
and made them comics.

147
00:06:16,402 --> 00:06:17,602
Cool!

148
00:06:17,637 --> 00:06:20,538
Homie, you're gonna chuckle
your blues away.

149
00:06:20,573 --> 00:06:22,673
This is the best
kind of comedy.

150
00:06:22,709 --> 00:06:24,709
No writers.
Amen to that, Marge.

151
00:06:24,744 --> 00:06:26,677
And I appreciate this,
but I really resent

152
00:06:26,713 --> 00:06:28,179
the two-drink minimum!

153
00:06:28,214 --> 00:06:30,214
You always drink more
than two drinks.

154
00:06:30,250 --> 00:06:32,183
But no one <i>makes</i> me.

155
00:06:32,218 --> 00:06:34,719
Three Long Island
iced teas, please.

156
00:06:35,822 --> 00:06:38,356
Okay, we're gonna perform
a little improv.

157
00:06:38,391 --> 00:06:39,957
First we need a location.

158
00:06:39,993 --> 00:06:42,326
Uh, 40.7 degrees
north latitude,

159
00:06:42,362 --> 00:06:44,662
74 degrees
west longitude.

160
00:06:44,697 --> 00:06:47,131
Ah, yes, New York City.
Now we need a relationship

161
00:06:47,167 --> 00:06:48,499
for me and Cathy here.

162
00:06:48,535 --> 00:06:49,867
Loveless marriage!

163
00:06:49,903 --> 00:06:51,969
(laughs):
Okay, I heard loveless marriage.

164
00:06:52,005 --> 00:06:53,404
These guys are pros.

165
00:06:53,439 --> 00:06:55,640
All they did was ask
for two premises.

166
00:06:55,675 --> 00:06:58,309
That's two more
than <i>you've</i> asked for.

167
00:06:58,344 --> 00:06:59,877
(sighs):
Oh, boy.

168
00:06:59,913 --> 00:07:02,079
All right, all we need now
is an object.

169
00:07:02,115 --> 00:07:03,347
Anyone.

170
00:07:04,551 --> 00:07:06,918
(quietly):
Fear of public speaking

171
00:07:06,953 --> 00:07:08,052
Sorry, didn't hear that.

172
00:07:08,087 --> 00:07:11,055
Oh... fear of public speaking.

173
00:07:11,090 --> 00:07:13,057
Maybe he has trouble
talking in the dark.

174
00:07:13,092 --> 00:07:15,059
Hey, Jerry,
bring up number seven.

175
00:07:15,094 --> 00:07:16,160
(shrieks)

176
00:07:16,196 --> 00:07:17,361
No! Jerry, no!

177
00:07:17,397 --> 00:07:19,096
Aah! No! Aah!

178
00:07:19,132 --> 00:07:20,565
Jerry, stop it! No!

179
00:07:20,600 --> 00:07:21,833
Don't... Aah!

180
00:07:21,868 --> 00:07:24,535
Oh, no! Oh-ho-ho! Aah! Oh!

181
00:07:24,571 --> 00:07:27,572
Okay, okay, okay, um...
fear of public speaking.

182
00:07:27,607 --> 00:07:29,073
Well, that's
not really an object,

183
00:07:29,108 --> 00:07:30,541
but we'll make it work.

184
00:07:30,577 --> 00:07:32,343
New York City,
loveless marriage,

185
00:07:32,378 --> 00:07:34,078
fear of public speaking.

186
00:07:34,113 --> 00:07:37,682
They've pulled back the bow--
now let the arrow take flight.

187
00:07:37,717 --> 00:07:39,884
You know, I'm gonna
move over a seat.

188
00:07:40,920 --> 00:07:42,620
We now take you to an apartment

189
00:07:42,655 --> 00:07:45,623
on 68th and
Columbus Avenue.

190
00:07:45,658 --> 00:07:48,726
(trembling):
Oh... eeh...

191
00:07:48,761 --> 00:07:51,429
aah... aah... ooh...

192
00:07:51,464 --> 00:07:53,364
What? What?
Cathy, what's wrong?

193
00:07:53,399 --> 00:07:54,799
I-I... I-I...

194
00:07:54,834 --> 00:07:57,735
I... ah-ah-ah... ah...

195
00:07:57,770 --> 00:07:59,937
Aw, for heaven's sakes, Cathy,
when we got married,

196
00:07:59,973 --> 00:08:01,739
you used to speak for hours.

197
00:08:01,774 --> 00:08:04,141
But since we moved
to New York City, nothing!

198
00:08:04,177 --> 00:08:06,611
(audience laughing)
Wow. It all magically fits.

199
00:08:06,646 --> 00:08:09,447
Come on, Cathy,
say something! Anything!

200
00:08:09,482 --> 00:08:11,616
(Italian accent):
Fuggedaboutit!

201
00:08:11,651 --> 00:08:13,451
(laughter, applause)

202
00:08:15,188 --> 00:08:17,455
And scene!

203
00:08:17,490 --> 00:08:20,057
<i>I</i> said "fear of
public speaking."

204
00:08:20,093 --> 00:08:23,227
Yes, you did. Very nice.
Return to your seat immediately.

205
00:08:23,263 --> 00:08:26,097
Wow. Wow. These guys
do everything I can't.

206
00:08:26,132 --> 00:08:27,765
Maybe they can teach me.

207
00:08:27,800 --> 00:08:30,001
I don't know that
they're interested in that.

208
00:08:30,036 --> 00:08:32,169
$500 for the first
eight classes.

209
00:08:32,205 --> 00:08:33,938
That's kind of expensive.

210
00:08:33,973 --> 00:08:36,841
Couldn't people just form
their own groups for free?

211
00:08:36,876 --> 00:08:38,776
And scene.

212
00:08:40,146 --> 00:08:42,980
Uh, uh... excuse me,
is this the, uh,

213
00:08:43,016 --> 00:08:44,615
improv class?

214
00:08:44,651 --> 00:08:46,684
Yes, <i>and...</i> come in.

215
00:08:46,719 --> 00:08:47,685
Sit anywhere?

216
00:08:47,720 --> 00:08:50,054
Yes, <i>and...</i> be quiet.

217
00:08:51,124 --> 00:08:52,957
HOMER:
<i>This is it.</i>

218
00:08:52,992 --> 00:08:56,494
<i>I feel my mind exploding</i>
<i>with premises.</i>

219
00:08:56,529 --> 00:08:58,930
♪ ♪

220
00:09:08,174 --> 00:09:09,674
<i>So many ideas,</i>

221
00:09:09,709 --> 00:09:12,343
<i>but how do I turn them</i>
<i>into comedy?</i>

222
00:09:12,378 --> 00:09:16,080
♪ Oh, kiss me ♪

223
00:09:16,115 --> 00:09:18,549
♪ Beneath the milky twilight-- ♪

224
00:09:18,584 --> 00:09:20,051
(chuckling)

225
00:09:20,086 --> 00:09:22,186
Homer, could-could you
stay with us, please?

226
00:09:22,221 --> 00:09:23,454
I don't know how.

227
00:09:23,489 --> 00:09:25,456
The secret is
to lose yourself

228
00:09:25,491 --> 00:09:28,225
and become, say,
a suicidal auctioneer.

229
00:09:28,261 --> 00:09:31,462
I could not possibly imagine
what such a person would say.

230
00:09:31,497 --> 00:09:34,065
Well, just remember,
it's-it's not <i>you.</i>

231
00:09:34,100 --> 00:09:36,334
<i>Don't you get it?</i>
<i>The secret to life</i>

232
00:09:36,369 --> 00:09:38,536
<i>has been right in front of you</i>
<i>all along:</i>

233
00:09:38,571 --> 00:09:40,972
<i>Don't be yourself!</i>

234
00:09:41,007 --> 00:09:42,974
Uh, uh, okay.

235
00:09:43,009 --> 00:09:44,976
What am I bid for this noose?

236
00:09:45,011 --> 00:09:46,177
Do hear $100?

237
00:09:46,212 --> 00:09:47,178
No?

238
00:09:47,213 --> 00:09:48,479
Going... going...

239
00:09:48,514 --> 00:09:49,547
(chokes)

240
00:09:49,582 --> 00:09:52,216
(laughter)

241
00:09:52,251 --> 00:09:55,353
That's great! Really took
my mind off my sick grandmother.

242
00:09:55,388 --> 00:09:56,754
(electronic chime)
Oh.

243
00:09:56,789 --> 00:09:59,056
Looks like the hospital
left a message.

244
00:10:00,360 --> 00:10:02,159
(voice breaking): Gotta go.
HOMER: See ya!

245
00:10:02,195 --> 00:10:03,928
(laughing):
It made me laugh.

246
00:10:03,963 --> 00:10:05,730
So, then, you pay me?
No, never!

247
00:10:05,765 --> 00:10:08,432
And if you open your own school,
we'll break your legs!

248
00:10:08,468 --> 00:10:11,168
You want to give him the
one we can never crack?

249
00:10:11,204 --> 00:10:13,904
Queen of Norway buying a car.

250
00:10:15,441 --> 00:10:18,776
(high-pitched):
Oh, dear! I can't a-fjord it!

251
00:10:18,811 --> 00:10:20,978
A star is born.

252
00:10:21,014 --> 00:10:24,548
(grunting)

253
00:10:24,584 --> 00:10:26,250
Excuse me.

254
00:10:26,285 --> 00:10:28,285
I'm from the city.
Are you aware that you live

255
00:10:28,321 --> 00:10:30,521
in a historical treehouse
preservation district?

256
00:10:30,556 --> 00:10:32,656
♪ ♪

257
00:10:38,898 --> 00:10:40,598
May I see your permits?

258
00:10:40,633 --> 00:10:42,299
I don't have permits.

259
00:10:42,335 --> 00:10:43,034
(screams)

260
00:10:43,069 --> 00:10:44,035
Don't worry.

261
00:10:44,070 --> 00:10:45,036
D-Don't worry.

262
00:10:45,071 --> 00:10:46,037
I'll take care of it.

263
00:10:46,072 --> 00:10:48,105
(grunting)

264
00:10:51,144 --> 00:10:52,977
I've got to hand it
to you, Dad.

265
00:10:53,012 --> 00:10:54,912
You went from
fear of public speaking

266
00:10:54,947 --> 00:10:57,648
to starting
your own improv troupe.

267
00:10:57,683 --> 00:10:59,250
Improv is exciting,

268
00:10:59,285 --> 00:11:01,052
but totally safe.

269
00:11:01,087 --> 00:11:02,920
Like driving a helicopter
on the ground.

270
00:11:02,955 --> 00:11:04,288
Um, that's not safe.

271
00:11:04,323 --> 00:11:05,556
Don't deny the premise.

272
00:11:05,591 --> 00:11:06,957
That's anti-improv.

273
00:11:06,993 --> 00:11:09,727
Note for new character,
"Auntie Improv."

274
00:11:09,762 --> 00:11:12,396
(high-pitched):
May I hear a suggestion?

275
00:11:12,432 --> 00:11:14,632
People, don't freak out,

276
00:11:14,667 --> 00:11:17,635
but the improv critic from the
<i>Springfield Shopper</i>

277
00:11:17,670 --> 00:11:19,170
is in the audience.

278
00:11:19,205 --> 00:11:21,338
(gasps)
Steve Thurlson?

279
00:11:21,374 --> 00:11:23,674
No, Thurlson is
their improv reporter.

280
00:11:23,709 --> 00:11:25,309
Grant Hood is
their improv critic.

281
00:11:25,344 --> 00:11:27,144
Then who's Jennifer Whitehead?

282
00:11:27,180 --> 00:11:28,946
Oh, she writes
improv think pieces.

283
00:11:28,981 --> 00:11:30,514
You know, trends,
big picture stuff.

284
00:11:30,550 --> 00:11:31,916
Who does the top ten list

285
00:11:31,951 --> 00:11:33,918
at end-of-year
"Best of Improv" issue?

286
00:11:33,953 --> 00:11:35,219
They each write their own.

287
00:11:35,254 --> 00:11:36,620
(all murmuring assent)
Oh, yeah.

288
00:11:36,656 --> 00:11:38,289
I forgot about that.
That's true.

289
00:11:38,324 --> 00:11:40,157
Huh!

290
00:11:40,193 --> 00:11:41,625
(piano riff playing)

291
00:11:41,661 --> 00:11:43,360
(applause)

292
00:11:43,396 --> 00:11:44,929
Hi, everybody.

293
00:11:44,964 --> 00:11:46,831
We are "Premises, Premises."

294
00:11:46,866 --> 00:11:51,035
Yes, it says on Google that
there are 5,012 improv groups

295
00:11:51,070 --> 00:11:53,104
with that name.

296
00:11:53,139 --> 00:11:54,505
LENNY:
Take it, Homer.

297
00:11:54,540 --> 00:11:57,975
(nervously):
Uh, okay, I need a location.

298
00:11:58,010 --> 00:12:00,711
Uh, Jackson Square
in New Orleans.

299
00:12:00,746 --> 00:12:03,547
And a type of person
that might be there.

300
00:12:03,583 --> 00:12:05,483
Someone with confidence.

301
00:12:05,518 --> 00:12:07,118
Mm, I...

302
00:12:07,153 --> 00:12:09,053
(Cajun accent):
I ga-ron-tee it!

303
00:12:09,088 --> 00:12:12,056
(applause and cheering)

304
00:12:13,426 --> 00:12:14,892
So, Tuesday is the
treehouse warming.

305
00:12:14,927 --> 00:12:16,861
Nelson's gonna
show us his mom's bra.

306
00:12:16,896 --> 00:12:18,129
(gasps)

307
00:12:18,164 --> 00:12:20,197
That's the thing
that boobs touch.

308
00:12:20,233 --> 00:12:23,634
Whoa, automatic blinds.

309
00:12:23,669 --> 00:12:25,069
Yeah, I think my
mom put those in.

310
00:12:25,104 --> 00:12:26,070
No need to thank her.

311
00:12:26,105 --> 00:12:27,271
She's just doing her job.

312
00:12:28,774 --> 00:12:29,974
(grunts)

313
00:12:33,746 --> 00:12:35,980
The Keebler Elves are real!

314
00:12:36,682 --> 00:12:38,482
Pass the gravy, Bart.

315
00:12:38,518 --> 00:12:39,717
Yo.

316
00:12:39,752 --> 00:12:40,985
Thank you.

317
00:12:41,020 --> 00:12:42,720
Geez, who ordered the crab?

318
00:12:42,755 --> 00:12:43,988
(gasps)
There's crab?

319
00:12:44,023 --> 00:12:45,322
There's no crab.

320
00:12:45,358 --> 00:12:47,892
There's no "thank you's",
no appreciation.

321
00:12:47,927 --> 00:12:49,193
Nothing.

322
00:12:51,364 --> 00:12:53,597
Uh, who was that directed at?

323
00:12:53,633 --> 00:12:54,698
Bart!

324
00:12:54,734 --> 00:12:56,734
Whew!
Pass the gravy, please.

325
00:12:57,970 --> 00:12:59,203
(phone chimes)

326
00:12:59,238 --> 00:13:01,839
(gasps)
Dad! Dad!

327
00:13:01,874 --> 00:13:05,276
The Springfield Fringe Festival
just invited us to perform.

328
00:13:05,311 --> 00:13:07,411
Huh? What's a "Fringe Festival"?

329
00:13:07,446 --> 00:13:10,014
My guess would be it's a
three-day series of performances

330
00:13:10,049 --> 00:13:11,916
by alternative
comedy and music acts,

331
00:13:11,951 --> 00:13:13,517
including, but not limited to,

332
00:13:13,553 --> 00:13:16,220
improv, stand-up, light circus
work and ironic burlesque.

333
00:13:16,255 --> 00:13:19,456
We're gonna be on the
main stage on closing night!

334
00:13:19,492 --> 00:13:20,858
This is going in my log.

335
00:13:20,893 --> 00:13:21,859
Yay!

336
00:13:21,894 --> 00:13:24,195
(chuckles)
More crab for me.

337
00:13:24,230 --> 00:13:25,930
MARGE:
There's no crab!

338
00:13:25,965 --> 00:13:27,097
Mm!

339
00:13:27,133 --> 00:13:28,933
(humming happily)

340
00:13:28,968 --> 00:13:29,700
(grunts, slurps)

341
00:13:29,735 --> 00:13:31,702
Mmm!

342
00:13:31,737 --> 00:13:35,506
(Marge grunting)

343
00:13:35,541 --> 00:13:37,408
Aw, Marge,
don't take it to heart.

344
00:13:37,443 --> 00:13:39,643
Kids are ungrateful,
that's their job.

345
00:13:39,679 --> 00:13:42,479
You can cheer up watching me
at the Fringe Festival.

346
00:13:42,515 --> 00:13:43,948
Good for you.

347
00:13:43,983 --> 00:13:46,951
The main stage, closing night,
all eyes on you.

348
00:13:46,986 --> 00:13:48,552
Wh-Wh-Wh-What...
what are you saying?

349
00:13:48,588 --> 00:13:51,455
<i>Oh, she's making me</i>
<i>nervous again.</i>

350
00:13:51,490 --> 00:13:53,090
<i>But now I'm a trained</i>
<i>comedy amateur.</i>

351
00:13:53,125 --> 00:13:54,658
<i>No one can get in my head.</i>

352
00:13:54,694 --> 00:13:56,560
<i>Marge Simpson,</i>

353
00:13:56,596 --> 00:13:59,997
<i>you don't want to accidentally</i>
<i>undermine him like last time.</i>

354
00:14:00,032 --> 00:14:02,633
<i>Oh, thank God he doesn't know</i>
<i>what I'm thinking.</i>

355
00:14:02,668 --> 00:14:04,635
<i>I know exactly</i>
<i>what she's thinking.</i>

356
00:14:04,670 --> 00:14:07,471
<i>That if I mess this up,</i>
<i>I'll be worse off than ever.</i>

357
00:14:07,506 --> 00:14:08,472
<i>He does know.</i>

358
00:14:08,507 --> 00:14:09,673
<i>She knows I know!</i>

359
00:14:09,709 --> 00:14:10,709
<i>(Homer and Marge scream)</i>

360
00:14:12,260 --> 00:14:14,585
Homer?
(muffled): Aw, what?

361
00:14:14,685 --> 00:14:16,518
I slept on it, and
I'm madder than ever.

362
00:14:16,554 --> 00:14:17,786
Go get Bart.

363
00:14:17,822 --> 00:14:19,021
Oh my God, Marge,

364
00:14:19,056 --> 00:14:20,956
you woke up with
morning-would-be-mad.

365
00:14:20,991 --> 00:14:23,392
Okay, I'll put this
in your new language.

366
00:14:23,427 --> 00:14:25,361
Location: Bart's room.

367
00:14:25,396 --> 00:14:27,496
Action: bring him here.

368
00:14:27,531 --> 00:14:28,831
Can I get
a character, please?

369
00:14:28,866 --> 00:14:30,132
Fine. Um...

370
00:14:30,167 --> 00:14:32,568
A near-sighted Frankenstein.

371
00:14:32,603 --> 00:14:33,836
Okay.

372
00:14:33,871 --> 00:14:37,373
(growling)

373
00:14:37,408 --> 00:14:40,142
Bride! Bride!

374
00:14:40,177 --> 00:14:42,745
I do not look like
the Bride of Frankenstein.

375
00:14:42,780 --> 00:14:44,546
(as monster):
Don't deny premise.

376
00:14:44,582 --> 00:14:45,914
(growls)

377
00:14:45,950 --> 00:14:49,251
(knocking on door)

378
00:14:49,286 --> 00:14:50,319
(grunts)

379
00:14:50,354 --> 00:14:52,988
BART:
Mom, can I come in?

380
00:14:53,023 --> 00:14:54,812
You like your eggs
a little runny don't you?

381
00:14:54,836 --> 00:14:55,592
What?

382
00:14:55,593 --> 00:14:57,993
And your toast set at four,
with a little bit of butter?

383
00:14:58,028 --> 00:14:59,228
Yeah!

384
00:14:59,263 --> 00:15:01,830
Hash browns might
be a little burnt.

385
00:15:01,866 --> 00:15:04,833
I like 'em however
you make 'em, sweetie.

386
00:15:04,869 --> 00:15:06,335
I just wanted to say

387
00:15:06,370 --> 00:15:07,903
I'm sorry.

388
00:15:07,938 --> 00:15:10,305
We all forget just how many
wonderful things you do.

389
00:15:10,341 --> 00:15:11,273
Oh...

390
00:15:11,308 --> 00:15:12,674
(gasps)

391
00:15:12,710 --> 00:15:15,444
(voice-breaking):
Y-You're making me cry.

392
00:15:15,479 --> 00:15:16,745
(sobs)
And best of all,

393
00:15:16,781 --> 00:15:19,448
you're as hot
as the day I met you.

394
00:15:19,483 --> 00:15:20,449
(sobs)

395
00:15:20,484 --> 00:15:22,251
Thank you!

396
00:15:22,286 --> 00:15:26,088
That apology speech you wrote
worked like a charm, Pop.

397
00:15:26,123 --> 00:15:28,090
Your mother can't resist
an apology

398
00:15:28,125 --> 00:15:30,125
that comes
straight from the heart...

399
00:15:30,161 --> 00:15:31,360
of this box.

400
00:15:34,465 --> 00:15:36,098
Oh, dear God,

401
00:15:36,133 --> 00:15:39,067
this is not a
Renaissance Faire, is it?

402
00:15:39,103 --> 00:15:40,302
Uh, that's in two weeks,

403
00:15:40,337 --> 00:15:42,638
Henry the Weight Problem. Ha!

404
00:15:42,673 --> 00:15:46,208
Gee, I don't know if I belong
here with all this talent.

405
00:15:46,243 --> 00:15:47,643
(in goofy voice):
Hey, Reverend.

406
00:15:47,678 --> 00:15:49,545
Why didn't the dinosaurs
make it on Noah's Ark?

407
00:15:49,580 --> 00:15:51,113
(in normal voice):
Ooh, I give up.

408
00:15:51,148 --> 00:15:53,048
(in goofy voice):
Because they didn't exist.

409
00:15:53,083 --> 00:15:54,316
(laughter)

410
00:16:00,124 --> 00:16:03,325
Dad, this festival
encourages experimentation.

411
00:16:03,360 --> 00:16:04,593
Even failure.

412
00:16:04,628 --> 00:16:06,562
Oh, what if I don't fail?

413
00:16:06,597 --> 00:16:08,397
Desperate times call
for desperate measures.

414
00:16:08,432 --> 00:16:10,132
I am getting a helium balloon.

415
00:16:10,167 --> 00:16:11,600
That makes anyone funny.

416
00:16:11,635 --> 00:16:13,635
(in high helium voice):
Be right back!

417
00:16:13,671 --> 00:16:16,004
(Homer retching)

418
00:16:16,040 --> 00:16:18,340
Homer, are you okay?

419
00:16:18,375 --> 00:16:21,777
You look exactly like
Barbara Streisand did in 1967.

420
00:16:21,812 --> 00:16:23,946
I can't think of anything funny.

421
00:16:23,981 --> 00:16:26,014
Homer, Homer, relax,
you're a riot.

422
00:16:26,050 --> 00:16:29,518
Remember how funny you was when
you was the confident Cajun?

423
00:16:29,553 --> 00:16:31,787
Look, just let me
feed you the prompt.

424
00:16:31,822 --> 00:16:34,156
Wait, you mean cheat at improv?

425
00:16:34,191 --> 00:16:35,791
What would Del Close say?

426
00:16:35,826 --> 00:16:38,827
He would say, "Do like Moe
says, and shut the hell up."

427
00:16:38,863 --> 00:16:40,529
Who's Del Close?

428
00:16:40,564 --> 00:16:44,099
Only the author of the best book
I intend to read someday.

429
00:16:44,134 --> 00:16:46,535
Homer, the cemeteries
are filled with people

430
00:16:46,570 --> 00:16:48,370
who <i>didn't</i> cheat at improv.

431
00:16:48,405 --> 00:16:50,205
Hmm...

432
00:16:56,413 --> 00:16:59,047
Okay, let's go over what you're
gonna call out one more time.

433
00:16:59,083 --> 00:17:00,949
Uh, ethnicity: Cajun.

434
00:17:00,985 --> 00:17:02,684
Location: back alley.

435
00:17:02,720 --> 00:17:03,952
Good, good.

436
00:17:03,988 --> 00:17:05,320
The back alley Cajun bit.

437
00:17:05,356 --> 00:17:06,788
LISA:
Dad!

438
00:17:06,824 --> 00:17:09,791
Are you cheating by
planting suggestions?

439
00:17:09,827 --> 00:17:10,526
Maybe.

440
00:17:10,561 --> 00:17:12,160
(stunned gasp)

441
00:17:12,196 --> 00:17:14,630
Have you learned nothing
from owning an unread copy

442
00:17:14,665 --> 00:17:16,064
of <i>Truth in Comedy?</i>

443
00:17:16,100 --> 00:17:18,000
It's either that
or quit the show.

444
00:17:18,035 --> 00:17:19,968
You can't let
your troupe down.

445
00:17:20,004 --> 00:17:22,804
They need your space work,
your strong choices,

446
00:17:22,840 --> 00:17:24,673
and scene-building skills.

447
00:17:24,708 --> 00:17:26,341
Hey, lay off your dad, huh?

448
00:17:26,377 --> 00:17:28,577
Everything that's
supposedly spontaneous

449
00:17:28,612 --> 00:17:30,078
has already been planned, okay?

450
00:17:30,114 --> 00:17:33,181
Reality shows, uh,
lip-sync singing, even...

451
00:17:33,217 --> 00:17:34,516
(in dramatic voice):
awards shows.

452
00:17:34,552 --> 00:17:35,918
No!

453
00:17:35,953 --> 00:17:38,387
Yes, that's why the losers
don't show up.

454
00:17:38,422 --> 00:17:40,389
B-b-b-but they have
scheduling conflicts.

455
00:17:40,424 --> 00:17:41,690
Yeah, right.

456
00:17:41,725 --> 00:17:44,593
Dave Franco has a
scheduling conflict.

457
00:17:44,628 --> 00:17:46,061
No, no, I've heard enough.

458
00:17:46,096 --> 00:17:49,464
I refuse to use
performance enhancing "sugs."

459
00:17:51,435 --> 00:17:52,701
AUDIENCE (muttering):
It's Homer Simpson.

460
00:17:52,736 --> 00:17:54,169
Homer Simpson's here.

461
00:17:54,204 --> 00:17:56,038
(audience continues to
mutter in excitement)

462
00:17:56,073 --> 00:17:58,240
Okay, everyone,
I'm Homer Simpson

463
00:17:58,275 --> 00:18:00,108
and I need an occupation.

464
00:18:00,144 --> 00:18:01,577
Cadaver salesman?

465
00:18:01,612 --> 00:18:03,378
Drive-thru cashier.

466
00:18:03,414 --> 00:18:05,180
Uh, frog gigger.

467
00:18:05,215 --> 00:18:06,415
Finger kisser! Mwah!

468
00:18:06,450 --> 00:18:07,616
Nurse.

469
00:18:07,651 --> 00:18:09,284
You'll have to be more specific.

470
00:18:09,320 --> 00:18:11,286
I need a nurse!

471
00:18:11,322 --> 00:18:12,854
Back alley Cajun!

472
00:18:12,890 --> 00:18:13,855
Mm-hmm!

473
00:18:13,891 --> 00:18:15,190
Ah...

474
00:18:15,225 --> 00:18:17,593
A father I can look up to.

475
00:18:17,628 --> 00:18:20,629
Oh. Um, uh...

476
00:18:20,664 --> 00:18:23,365
I hear... drive-thru cashier!

477
00:18:23,400 --> 00:18:25,200
BOTH:
You wha... ?

478
00:18:25,235 --> 00:18:27,502
Hello, welcome to
"Down and Out Burger"

479
00:18:27,538 --> 00:18:28,670
Would you like fries with that?

480
00:18:28,706 --> 00:18:30,038
(laughter)
A thousand?

481
00:18:30,074 --> 00:18:32,374
Sure thing, Mr. Brando.

482
00:18:32,409 --> 00:18:34,810
By the way, this is 1992.

483
00:18:34,845 --> 00:18:36,178
(Homer chuckles,
audience laughs)

484
00:18:38,147 --> 00:18:41,816
>>> AND NOW WE'RE GOING TO DO
SOMETHING A LITTLE SPECIAL.

485
00:18:41,950 --> 00:18:45,153
>> OUR FATHER IS GOING TO IMPROV
LIVE ANSWERS TO QUESTIONS FROM

486
00:18:45,287 --> 00:18:49,891
THE TV AUDIENCE.
IT ONLY TOOK US 27 YEARS TO DO

487
00:18:50,025 --> 00:18:53,961
WHAT THEY COULD DO IN 1954.
>> HOMER, TIME TO BOMB.

488
00:18:54,096 --> 00:18:57,732
>> HELLO!
I'VE GATHERED YOU HERE BECAUSE I

489
00:18:57,866 --> 00:19:04,672
HAVE AN IMPORTANT ANNOUNCEMENT.
THIS IS THE LAST EPISODE OF "THE

490
00:19:04,806 --> 00:19:07,508
SIMPSONS."
IT'S BEEN A GREAT RUN.

491
00:19:07,643 --> 00:19:12,313
JUST KIDDING.
"THE SIMPSONS" WILL NEVER END.

492
00:19:12,447 --> 00:19:15,816
ON SATURDAY NIGHT LIVE LAST
NIGHT, DRAKE WAS TERRIBLE.

493
00:19:15,951 --> 00:19:19,987
NOW TO TAKE YOUR CALLS.
LET'S GO TO HANNAH.

494
00:19:20,122 --> 00:19:25,326
HANNAH, YOU'RE TALKING TO HOMER.
>> HI THERE, HOMER.

495
00:19:25,460 --> 00:19:30,164
MY QUESTION FOR YOU IS WHO DO
YOU LIKE MORE -- LENNY OR

496
00:19:30,299 --> 00:19:33,467
CARL -- AND WHY?
>> LET'S SEE.

497
00:19:33,602 --> 00:19:38,372
I LIKE LENNY BECAUSE HE'S THE
BLACK GUY AND -- WAIT A MINUTE,

498
00:19:38,507 --> 00:19:39,941
NO.
CARL'S THE -- WAIT.

499
00:19:40,075 --> 00:19:43,144
LET ME GET BACK TO YOU WHEN I
FIGURE OUT WHO'S WHO.

500
00:19:43,278 --> 00:19:47,915
LET'S GO TO THE NEXT QUESTION.
AMANDA?

501
00:19:48,050 --> 00:19:51,319
>> I WAS WONDERING IF YOU COULD
GIVE ME ANY TIPS OR TRIPS FOR

502
00:19:51,453 --> 00:19:55,623
MAKING IT LOOK LIKE I'M HARD AT
WORK BUT I'M RELAXING OR TAKING

503
00:19:55,757 --> 00:19:58,559
A NAP?
>> ALWAYS WEAR GLASSES WITH EYES

504
00:19:58,694 --> 00:20:01,896
GLUED ONTO THEM.
NEXT QUESTION.

505
00:20:02,030 --> 00:20:04,865
GEORGE, I THINK.
HELLO, GEORGE.

506
00:20:05,000 --> 00:20:08,002
>> HELLO, HOMER.
>> WHAT'S YOUR QUESTION?

507
00:20:08,136 --> 00:20:12,106
>> SO MY QUESTION IS
PIZZA-RELATED.

508
00:20:12,241 --> 00:20:14,508
>> PIZZA?
>> DO YOU PREFER CHICAGO DEEP

509
00:20:14,643 --> 00:20:16,978
DISH OR NEW YORK-STYLE?
>> LET'S SEE.

510
00:20:17,112 --> 00:20:22,883
I PREFER CHICAGO DEEP DISH
BECAUSE I LIKE ITALIAN BETTER

511
00:20:23,018 --> 00:20:27,021
THAN CHINESE.
AND NOW LET'S GO TO A PLANTED

512
00:20:27,155 --> 00:20:32,660
CALL WITH A PLANTED QUESTION.
LET'S GO TO -- HELLO?

513
00:20:32,794 --> 00:20:36,030
>> HOMER, HOW ARE YOU DOING?
>> I'M DOING ALL RIGHT.

514
00:20:36,164 --> 00:20:39,133
IS THAT YOUR QUESTION?
>> I WAS WONDERING WHAT KIND OF

515
00:20:39,268 --> 00:20:42,637
CAR DO YOU DRIVE?
>> OH, I DRIVE A HYBRID WHICH IS

516
00:20:42,771 --> 00:20:45,206
A COMBINATION OF OLD AND
TERRIBLE.

517
00:20:45,340 --> 00:20:48,142
NEXT CALLER.
CHRIS.

518
00:20:48,277 --> 00:20:51,112
YES, CHRIS.
WHAT'S YOUR QUESTION?

519
00:20:51,246 --> 00:20:55,683
OR COMMENT.
>> MY QUESTION IS WHAT'S YOUR

520
00:20:55,817 --> 00:20:58,986
FAVORITE JOB?
WHAT WAS YOUR FAVORITE JOB?

521
00:20:59,121 --> 00:21:02,590
>> MY FAVORITE JOB WOULD HAVE
BEEN BEING AN ASTRONAUT BECAUSE

522
00:21:02,724 --> 00:21:05,793
EVERYTHING WAS DONE FOR ME.
AND ALSO I COULD GET AWAY FROM

523
00:21:05,927 --> 00:21:08,260
THE BOY.
WELL, THAT'S IT.

524
00:21:08,261 --> 00:21:10,255
THAT'S IT, LADIES AND GENTLEMEN.

525
00:21:10,256 --> 00:21:12,164
WE HAVE COME TO MY CLOSING
REMARKS.

526
00:21:12,165 --> 00:21:14,063
IT ONLY LASTED THREE MINUTES,

527
00:21:14,064 --> 00:21:16,449
LIKE EATING CHEESEBURGERS AND
MAKING LOVE.

528
00:21:16,848 --> 00:21:18,319
IF YOUR CALL HASN'T BEEN TAKEN

529
00:21:18,419 --> 00:21:20,482
YET, PLEASE CONTINUE TO HOLD.

530
00:21:20,729 --> 00:21:22,028
THE CAST OF EMPIRE WILL BE

531
00:21:22,097 --> 00:21:24,346
ANSWERING QUESTIONS WEDNESDAY
NIGHT.

532
00:21:24,601 --> 00:21:26,067
SOMEONE WILL LET THEM KNOW.

533
00:21:27,255 --> 00:21:29,072
FLASHING BY ARE THE CREDITS OF THE

534
00:21:29,073 --> 00:21:31,522
PEOPLE WHO WORKED LONG AND HARD
ON THIS.

535
00:21:31,830 --> 00:21:33,701
I HAVE NO IDEA WHO THEY ARE.

536
00:21:33,893 --> 00:21:35,585
NOW THE SHOW IS OVER.

537
00:21:35,685 --> 00:21:38,091
THE SPOTLIGHT DIMS, THE LAUGHTER
FADES.

538
00:21:38,142 --> 00:21:39,608
SOMEONE CALL UBER.

539
00:21:39,816 --> 00:21:41,182
IF BART WOULD JUST RETURN MY

540
00:21:41,250 --> 00:21:43,665
PANTS SO I CAN MOVE FROM BEHIND
THIS DESK.

541
00:21:45,221 --> 00:21:46,554
DOO, DOO, DOO.

542
00:21:46,622 --> 00:21:48,289
WAITING ON THE PANTS.

543
00:21:48,357 --> 00:21:49,990
OH, BART, NOT CULOTTES.

544
00:21:50,715 --> 00:21:52,026
NO.

