﻿1
00:00:01,000 --> 00:00:05,000
<b><font color="#00FF00">♪ The Simpsons 27x22 ♪</font></b>
<font color="#00FFFF">Orange is the New Yellow</font>
Original Air Date on Ma

2
00:00:05,001 --> 00:00:10,001
== sync, corrected by <font color="#00FF00">elderman</font> ==
<font color="#00FFFF">@elder_man</font>

3
00:00:24,297 --> 00:00:26,297
Oh, not again!

4
00:00:46,419 --> 00:00:47,618
D'oh!

5
00:00:56,262 --> 00:00:57,795
Eh, hello?

6
00:00:59,966 --> 00:01:01,799
Bart!

7
00:01:01,835 --> 00:01:03,267
Hell no!

8
00:01:03,303 --> 00:01:04,335
D'oh!

9
00:01:10,977 --> 00:01:12,210
Mmm.

10
00:01:17,116 --> 00:01:18,316
D'oh!

11
00:01:20,486 --> 00:01:21,719
Homer.

12
00:01:21,754 --> 00:01:23,588
Do something.

13
00:01:49,415 --> 00:01:51,048
<i>Quittin' time!</i>

14
00:01:51,084 --> 00:01:52,283
<i>Quittin' time.</i>

15
00:01:52,318 --> 00:01:53,551
<i>Thank God it's Friday.</i>

16
00:01:53,586 --> 00:01:54,785
<i>Thursday.</i>

17
00:01:54,821 --> 00:01:56,087
<i>Same thing. See you Monday.</i>

18
00:01:59,659 --> 00:02:01,225
Marge, baby, I'm out the door.

19
00:02:01,261 --> 00:02:02,793
Are you sure?

20
00:02:02,829 --> 00:02:04,996
Because sometimes people say
they're out the door

21
00:02:05,031 --> 00:02:06,898
when they really
haven't left yet.

22
00:02:06,933 --> 00:02:09,767
Those people are horrible,
horrible liars.

23
00:02:09,802 --> 00:02:11,002
See you soon.

24
00:02:11,037 --> 00:02:12,937
Simpson, not so fast.

25
00:02:12,972 --> 00:02:15,273
Hold up one end of this poster.

26
00:02:15,308 --> 00:02:17,008
Now use it to conceal this.

27
00:02:19,379 --> 00:02:20,678
Excellent.

28
00:02:20,713 --> 00:02:22,580
Now, let me just
crank up the plumb bob

29
00:02:22,615 --> 00:02:24,115
to determine the vertical.

30
00:02:25,718 --> 00:02:27,752
With this mechanical marvel,

31
00:02:27,787 --> 00:02:30,655
we'll have this poster
level in under three hours.

32
00:02:35,395 --> 00:02:36,961
Ooh, and for a
little excitement,

33
00:02:36,996 --> 00:02:39,196
how about a plumb bob song?

34
00:02:39,232 --> 00:02:41,098
Uh...

35
00:02:41,134 --> 00:02:43,668
All right.
Um...

36
00:02:43,703 --> 00:02:45,436
♪ One must never, never ♪

37
00:02:45,471 --> 00:02:46,971
♪ Never rush ♪

38
00:02:47,006 --> 00:02:48,673
♪ The plumb bob ♪

39
00:02:48,708 --> 00:02:50,341
♪ Slow is the way ♪

40
00:02:50,376 --> 00:02:53,444
♪ The only way to go ♪

41
00:02:53,479 --> 00:02:55,713
♪ Clear your schedule ♪

42
00:02:55,748 --> 00:02:59,150
♪ Before you use the plumb bob ♪

43
00:02:59,185 --> 00:03:01,285
♪ Yes, the plumb bob ♪

44
00:03:01,321 --> 00:03:04,221
♪ Is mighty, mighty ♪

45
00:03:04,257 --> 00:03:06,824
♪ Mighty ♪

46
00:03:06,859 --> 00:03:10,695
♪ Slow... ♪

47
00:03:15,234 --> 00:03:16,567
<i>Okay.</i>

48
00:03:16,602 --> 00:03:17,835
Maggie's had her bath,

49
00:03:17,870 --> 00:03:20,137
dinner's on the stove.
Hmm.

50
00:03:20,173 --> 00:03:22,039
Dare I pop a cork?

51
00:03:23,509 --> 00:03:24,909
Give me a spin, Marge.

52
00:03:24,944 --> 00:03:26,177
You're so deft.

53
00:03:26,212 --> 00:03:28,713
- Not like...
- him.

54
00:03:28,748 --> 00:03:30,247
Mom. Mom!

55
00:03:30,283 --> 00:03:32,917
My costume for the
science play is all wrong!

56
00:03:32,952 --> 00:03:34,685
You said you were a seahorse.

57
00:03:34,721 --> 00:03:35,987
A <i>male</i> seahorse.

58
00:03:36,022 --> 00:03:37,254
With a pouch.

59
00:03:37,290 --> 00:03:39,023
Males have pouches?

60
00:03:39,058 --> 00:03:42,560
Male seahorse's nurturing is one
of the wonders of the world.

61
00:03:42,595 --> 00:03:44,495
You said you read the script.

62
00:03:44,530 --> 00:03:46,263
Bart gave me a synopsis.

63
00:03:46,299 --> 00:03:47,565
I...

64
00:03:47,600 --> 00:03:49,333
Oh, boy.

65
00:03:49,369 --> 00:03:51,435
Oh, now what?

66
00:03:59,612 --> 00:04:01,445
Eat your carrot and pea medley.

67
00:04:05,118 --> 00:04:06,550
Mom, I'll clean
that up for you.

68
00:04:06,586 --> 00:04:07,918
Oh, thank God, some help.

69
00:04:07,954 --> 00:04:10,021
Where's the mop?
In the mop closet.

70
00:04:10,056 --> 00:04:12,890
Where's the bucket?
Under the mop.

71
00:04:12,925 --> 00:04:14,959
Other closet.

72
00:04:14,994 --> 00:04:16,260
There's stuff in front of it.

73
00:04:18,264 --> 00:04:19,497
Let me help.

74
00:04:21,567 --> 00:04:23,768
<i>Ay, caramba!</i>

75
00:04:23,803 --> 00:04:26,404
Hey, there's wood
underneath this linoleum.

76
00:04:26,439 --> 00:04:29,707
Oh!
Just go play outside.

77
00:04:29,742 --> 00:04:30,741
Outside?
Your loss.

78
00:04:36,849 --> 00:04:39,116
I do not get what kids
see in these places.

79
00:04:44,490 --> 00:04:45,790
It's the genie of the sub.

80
00:04:45,825 --> 00:04:47,158
Genie? I wish.

81
00:04:47,193 --> 00:04:48,659
You get a lamp and a carpet,

82
00:04:48,694 --> 00:04:50,327
which is more than
I've got in here.

83
00:04:50,363 --> 00:04:52,396
Hmm, didn't know this place
was filled with such losers.

84
00:04:52,432 --> 00:04:53,631
Hey, Martin.

85
00:04:53,666 --> 00:04:55,132
Bartholomew!

86
00:04:55,168 --> 00:04:58,302
This playground has safely
stimulated my imagination.

87
00:04:58,337 --> 00:05:00,571
What the hell are
you talking about?

88
00:05:00,606 --> 00:05:02,740
Martin.

89
00:05:02,775 --> 00:05:04,775
Who's your new friend?

90
00:05:04,811 --> 00:05:06,977
Mother, don't blow this for me.

91
00:05:07,013 --> 00:05:10,081
Fine, I'll just go sit
and talk to his mother.

92
00:05:10,116 --> 00:05:11,315
Where is she?

93
00:05:11,350 --> 00:05:12,650
I'm here by myself.

94
00:05:12,685 --> 00:05:14,618
But I have a safety number.

95
00:05:14,654 --> 00:05:15,853
Moe's Tavern.

96
00:05:15,888 --> 00:05:17,121
Homer ain't here.

97
00:05:17,156 --> 00:05:18,389
And for once, that's the truth.

98
00:05:24,730 --> 00:05:27,665
Simpson, are you
here unsupervised?

99
00:05:27,700 --> 00:05:29,500
Yeah.
And so what?

100
00:05:29,535 --> 00:05:33,003
I'd lose the attitude,
"Sylvester Alone."

101
00:05:34,373 --> 00:05:36,507
Finally, a laugh out of Lou.

102
00:05:36,542 --> 00:05:39,243
Son, kids aren't allowed
on their own anymore.

103
00:05:39,278 --> 00:05:41,011
Now, who said you
could come here?

104
00:05:41,047 --> 00:05:42,146
My mom.

105
00:05:42,181 --> 00:05:44,982
Ugh.
Always the mom.

106
00:05:51,591 --> 00:05:52,857
She's got needles.

107
00:05:58,931 --> 00:06:01,432
What did Bart do now?

108
00:06:01,467 --> 00:06:02,766
I was playing
nicely in the park.

109
00:06:02,802 --> 00:06:04,768
Bart, how could you... What?

110
00:06:04,804 --> 00:06:06,670
Your children
need to be supervised.

111
00:06:06,706 --> 00:06:08,339
Take her away, boys.

112
00:06:08,374 --> 00:06:10,875
If you take me away, then
who's gonna watch my kids?

113
00:06:10,910 --> 00:06:12,276
You should've thought of that

114
00:06:12,311 --> 00:06:14,545
<i>before</i> we showed up
unannounced.

115
00:06:14,580 --> 00:06:16,680
What, Marge?

116
00:06:16,716 --> 00:06:18,082
You're being arrested?

117
00:06:18,117 --> 00:06:20,050
I'm afraid so, Mr. Simpson.

118
00:06:20,086 --> 00:06:22,887
A mother at the park saw
something she disapproved of.

119
00:06:22,922 --> 00:06:25,923
And luckily for your son,
she overreacted.

120
00:06:30,263 --> 00:06:32,730
I, uh...
I get carsick in the front.

121
00:06:38,832 --> 00:06:41,440
Our top story,
a Springfield mother

122
00:06:41,441 --> 00:06:44,208
has been arrested for
an outrageous "negligée."

123
00:06:44,244 --> 00:06:45,510
The...

124
00:06:45,545 --> 00:06:47,411
Sorry, I'm being told
it's negligence.

125
00:06:47,447 --> 00:06:49,781
Which is very boring.

126
00:06:49,816 --> 00:06:51,415
Judge, I don't understand.

127
00:06:51,451 --> 00:06:53,918
When I was a kid,
we used to go out and play

128
00:06:53,953 --> 00:06:55,787
and not come home until dark.

129
00:06:55,822 --> 00:06:57,121
I see.

130
00:06:57,157 --> 00:06:58,790
Bailiff, incarcerate
Marge Simpsons' mother.

131
00:06:59,759 --> 00:07:02,126
Thanks for
ratting me out, Marge

132
00:07:04,164 --> 00:07:05,830
With all due respect, Judge,

133
00:07:05,865 --> 00:07:07,331
this is wackadoodle.

134
00:07:07,367 --> 00:07:10,101
Nobody cares about
their kids more than I do.

135
00:07:10,136 --> 00:07:11,335
Liar.

136
00:07:11,371 --> 00:07:12,804
Marge is right, Your Honor.

137
00:07:12,839 --> 00:07:14,939
My e-mail password
is "bad dad."

138
00:07:14,974 --> 00:07:16,240
Yeah, Judge.

139
00:07:16,276 --> 00:07:17,608
If you want to know
who should be in jail,

140
00:07:17,644 --> 00:07:21,145
he weighs 240
and smells like onions.

141
00:07:21,181 --> 00:07:23,014
It is not within
the purview of this court

142
00:07:23,049 --> 00:07:25,583
to determine how fat
and smelly your father may be.

143
00:07:25,618 --> 00:07:26,584
Woo-hoo!

144
00:07:26,619 --> 00:07:28,152
Mrs. Simpsons, 90 days.

145
00:07:33,459 --> 00:07:34,992
This is Kafkaesque.

146
00:07:35,028 --> 00:07:36,294
Kafkaesque!

147
00:07:36,329 --> 00:07:37,628
I've got my eye on you.

148
00:07:37,664 --> 00:07:39,197
Now it's Orwellian.

149
00:07:44,804 --> 00:07:46,003
Hey, kitties.

150
00:07:46,039 --> 00:07:47,238
You got a new
ball of yarn.

151
00:07:48,508 --> 00:07:51,943
Oh, that guard
is awfully slammy.

152
00:07:51,978 --> 00:07:53,444
Newbies on top.

153
00:07:53,479 --> 00:07:55,513
Oh.
Where's the ladder?

154
00:07:57,050 --> 00:07:59,350
Have you been claimed yet?

155
00:07:59,385 --> 00:08:00,117
Claimed?

156
00:08:00,153 --> 00:08:01,352
By one of the book clubs.

157
00:08:02,755 --> 00:08:04,455
Yeah, we read
a little, dig a little,

158
00:08:04,490 --> 00:08:06,624
read a little, dig a little,

159
00:08:06,659 --> 00:08:08,326
kiss a little,
dig a little.

160
00:08:13,266 --> 00:08:16,234
Homer, I think you're supposed
to cook sausage.

161
00:08:16,269 --> 00:08:17,535
What's the point?

162
00:08:18,838 --> 00:08:20,638
Well, hi there, Homer.

163
00:08:20,673 --> 00:08:24,008
I know a time like this is when
a man really needs his neighbor.

164
00:08:24,043 --> 00:08:26,244
Yeah, well,
thanks for the check-in.

165
00:08:26,279 --> 00:08:28,980
Well, this is
more than a check-in.

166
00:08:29,015 --> 00:08:31,983
It's a chance to do good
for us boys in the hood!

167
00:08:32,018 --> 00:08:33,517
Just call us NWA,

168
00:08:33,553 --> 00:08:36,220
"Neighbors With Appetizers."

169
00:08:37,790 --> 00:08:39,323
Wow, wow.

170
00:08:39,359 --> 00:08:42,059
I really am
the richest man in town.

171
00:08:42,095 --> 00:08:43,527
Not rich in money,

172
00:08:43,563 --> 00:08:46,530
but in what really matters:
pity.

173
00:08:46,566 --> 00:08:49,901
And what happened to Marge
is a wake up call for us all.

174
00:08:49,936 --> 00:08:52,904
There's no greater crime
than half-assed parenting.

175
00:08:52,939 --> 00:08:54,605
Kirk, where's Milhouse?

176
00:08:54,641 --> 00:08:56,574
On the leash, baby.
On the leash.

177
00:08:56,609 --> 00:08:57,842
He's too far.

178
00:08:57,877 --> 00:09:00,244
Retract.
Retract!

179
00:09:00,280 --> 00:09:02,747
Whoa!
Why'd you pull me back?

180
00:09:02,782 --> 00:09:06,083
Some new kids were just
about to give me a chance.

181
00:09:06,119 --> 00:09:07,885
Don't try anything new, son.

182
00:09:07,921 --> 00:09:10,922
I could've married a woman
who didn't look exactly like me.

183
00:09:10,957 --> 00:09:14,091
But that would've been crazy.

184
00:09:15,495 --> 00:09:17,461
Hey, newbie.

185
00:09:17,497 --> 00:09:19,463
Nobody touches
the new James Patterson

186
00:09:19,499 --> 00:09:22,600
until Solar reads it first.

187
00:09:27,640 --> 00:09:29,640
She wants you
to pick it up.

188
00:09:29,676 --> 00:09:32,310
You scared, huh?

189
00:09:43,489 --> 00:09:45,456
Oh! Whoa, whoa, whoa. Whoa.

190
00:09:45,491 --> 00:09:48,459
Anybody else want a
taste of Blue Thunder?

191
00:09:48,494 --> 00:09:50,661
Uh, I do. Yeah.

192
00:09:50,697 --> 00:09:52,897
Now, if you ladies
recall what this place

193
00:09:52,932 --> 00:09:54,732
is supposed
to be for...

194
00:09:54,767 --> 00:09:56,734
Mm... Mm...
Smokin' weed?

195
00:09:56,769 --> 00:09:58,736
Reading!

196
00:10:03,209 --> 00:10:06,010
Dad, if this is what they send
when Mom goes to jail,

197
00:10:06,045 --> 00:10:09,737
just imagine what happens
when you kick the bucket.

198
00:10:09,739 --> 00:10:12,332
Oh, man, that's gonna
be so awesome.

199
00:10:12,363 --> 00:10:14,596
Dad, Dad! You're
eating a teddy bear!

200
00:10:14,632 --> 00:10:16,999
That's
my stomach's problem.

201
00:10:17,034 --> 00:10:19,101
Okay, Homer, I just changed
all the linens,

202
00:10:19,136 --> 00:10:20,936
diapered the dog
for modesty,

203
00:10:20,971 --> 00:10:22,705
and replaced the batteries
in the smoke detectors,

204
00:10:22,740 --> 00:10:24,340
which were all bad.

205
00:10:24,375 --> 00:10:26,975
One was just a candy dish
with a red light painted on.

206
00:10:27,011 --> 00:10:28,344
Was there any candy in it?

207
00:10:28,379 --> 00:10:29,345
There is now.

208
00:10:29,380 --> 00:10:31,113
Oh, Flanders.

209
00:10:31,148 --> 00:10:34,149
It turns out there's
a good side to you after all.

210
00:10:34,185 --> 00:10:36,685
Well, sir, maybe I know a little
bit about what it's like

211
00:10:36,721 --> 00:10:38,987
to lose the lady of the house.

212
00:10:39,023 --> 00:10:41,323
Oh, I think I heard
a dryer ding.

213
00:10:41,359 --> 00:10:42,825
Thank you, Mr. Flanders.

214
00:10:42,860 --> 00:10:44,860
Tell your boys
I want to hang.

215
00:10:44,895 --> 00:10:46,261
Flanders is great.

216
00:10:46,297 --> 00:10:48,230
I've always said that.

217
00:10:48,265 --> 00:10:49,865
But we can't forget your mother.

218
00:10:51,235 --> 00:10:53,969
Oh...

219
00:11:00,911 --> 00:11:03,545
<i>Oh, my first prison flower.</i>

220
00:11:03,581 --> 00:11:07,182
<i>Can it be? Am I starting</i>
<i>to like it in here?</i>

221
00:11:07,218 --> 00:11:09,017
Exercise time!

222
00:11:09,053 --> 00:11:10,285
Ooh.

223
00:11:10,321 --> 00:11:12,488
I never have time
to exercise.

224
00:11:12,523 --> 00:11:15,524
Is this a prison or a spa?

225
00:11:15,559 --> 00:11:18,527
Aah! It's a prison.

226
00:11:23,367 --> 00:11:26,502
Hey, uh, are there any more
chocolate chip muffins?

227
00:11:26,537 --> 00:11:29,705
Sorry, Dad. The only ones left
have caraway seeds.

228
00:11:29,740 --> 00:11:32,374
Oh. I miss your mother
so much.

229
00:11:32,410 --> 00:11:35,878
Shockingly, these gift baskets
haven't solved anything.

230
00:11:35,913 --> 00:11:38,280
Wait, Dad. That one that
looks like blueberry

231
00:11:38,315 --> 00:11:39,615
actually has M&M'S.

232
00:11:39,650 --> 00:11:42,618
Oh. Oh, thank God.
Everything's okay.

233
00:11:42,653 --> 00:11:44,553
But for how long?

234
00:11:44,588 --> 00:11:47,890
Mom, I really, really miss you.

235
00:11:47,925 --> 00:11:49,124
Also, I have a field trip form

236
00:11:49,160 --> 00:11:50,893
that needs to be signed
for school.

237
00:11:50,928 --> 00:11:52,895
I think if I mail it to you
at the prison it's still

238
00:11:52,930 --> 00:11:54,396
easier than getting it from Dad.

239
00:11:54,432 --> 00:11:55,697
I want to talk to Mom.

240
00:11:55,733 --> 00:11:57,433
Can you send me a shiv
for show and tell?

241
00:11:57,468 --> 00:11:58,967
Preferably with blood on it.

242
00:11:59,003 --> 00:12:00,769
Hey, give me the phone.

243
00:12:00,805 --> 00:12:02,871
Are my blue pants done
at the prison laundry?

244
00:12:02,907 --> 00:12:04,273
Remember to sign my form.

245
00:12:04,308 --> 00:12:05,741
Shiv with blood.

246
00:12:05,776 --> 00:12:07,109
Press my pants.

247
00:12:07,144 --> 00:12:09,611
The electric chair.

248
00:12:09,647 --> 00:12:11,914
And then he thought it was
the electric chair.

249
00:12:14,185 --> 00:12:16,084
8:00.
Lights out.

250
00:12:16,120 --> 00:12:19,121
Really? 8:00?
I get to go to bed?

251
00:12:19,156 --> 00:12:21,457
I don't have to clean
a sink full of dishes,

252
00:12:21,492 --> 00:12:25,427
or write a paragraph with
topic sentence for Homer?

253
00:12:31,635 --> 00:12:33,602
Aw, you're tired too.

254
00:12:33,637 --> 00:12:36,538
Everyone goes to sleep
so easily here.

255
00:12:37,942 --> 00:12:40,642
Marge, your positivity
is contagious.

256
00:12:40,678 --> 00:12:43,445
I'm starting to believe
I really <i>will</i> show everyone.

257
00:12:43,481 --> 00:12:45,481
Simpson, you
got a visitor.

258
00:12:45,516 --> 00:12:48,951
Oh, gee, now?
It's a shame to go inside.

259
00:12:51,222 --> 00:12:53,222
Okay, let's
move in the body now.

260
00:12:58,028 --> 00:12:59,795
Marge, I got great news.

261
00:12:59,830 --> 00:13:02,030
We hired the one
good lawyer in town.

262
00:13:02,066 --> 00:13:04,967
Yes, Mrs. Simpson, I got
you off on a technicality.

263
00:13:05,002 --> 00:13:07,603
Since your husband never
filed for a birth certificate,

264
00:13:07,638 --> 00:13:08,971
Bart isn't
legally your son.

265
00:13:09,006 --> 00:13:10,172
Mm? Mm?

266
00:13:10,207 --> 00:13:11,473
How about that?

267
00:13:11,509 --> 00:13:13,041
Why aren't you
saying anything, Marge?

268
00:13:13,077 --> 00:13:14,610
You're free. Free.

269
00:13:14,645 --> 00:13:16,645
Now, it's not the world
you remember.

270
00:13:16,680 --> 00:13:18,347
The girl at the coffee place
that left?

271
00:13:18,382 --> 00:13:19,848
Came back.

272
00:13:19,884 --> 00:13:21,817
So you'll have that
to get used to.

273
00:13:21,852 --> 00:13:23,318
I'm free?

274
00:13:23,354 --> 00:13:24,887
Oh, and just in time.

275
00:13:24,922 --> 00:13:27,022
Bart's claiming
he's in another dimension,

276
00:13:27,057 --> 00:13:29,224
but I think he's just
hiding in the closet.

277
00:13:29,260 --> 00:13:32,194
I had 90 days.

278
00:13:32,229 --> 00:13:34,229
I was promised 90 days.

279
00:13:37,067 --> 00:13:39,668
Now you have to give me
more time.

280
00:13:39,703 --> 00:13:41,403
Oh, no you don't.

281
00:13:41,438 --> 00:13:43,338
I can't go to another
school meeting.

282
00:13:43,374 --> 00:13:44,840
I can't!

283
00:13:44,875 --> 00:13:47,409
Everyone just asks about
their own kid.

284
00:13:47,444 --> 00:13:49,244
Hey, you like shopping?
'Cause you just

285
00:13:49,280 --> 00:13:51,146
bought yourself
two more months.

286
00:13:52,550 --> 00:13:55,217
Sorry, Homie.
I can't go back yet.

287
00:13:55,252 --> 00:13:57,085
Just tell me where the soap
for the dishwasher goes

288
00:13:57,121 --> 00:13:58,487
and how do I...

289
00:14:01,290 --> 00:14:04,625
Marge would rather stay in
prison than come home to me.

290
00:14:04,792 --> 00:14:07,225
I have to reexamine
my entire life.

291
00:14:07,261 --> 00:14:09,561
Yeah, maybe you can start by not
bringing your kids to the bar.

292
00:14:10,998 --> 00:14:13,565
Yeah, it's kind of funny-- Homer
takes his kids to a saloon,

293
00:14:13,600 --> 00:14:16,335
but Marge is in jail
for being a bad parent.

294
00:14:16,370 --> 00:14:18,537
I guess somebody up there
likes me.

295
00:14:18,572 --> 00:14:20,405
Do you like him?

296
00:14:20,441 --> 00:14:22,341
Uh, not really, no.

297
00:14:22,376 --> 00:14:25,210
I've got to change, show Marge
I can help around the house.

298
00:14:25,245 --> 00:14:28,180
More than just turning up
the TV when she vacuums.

299
00:14:28,215 --> 00:14:31,850
I've got to become
the perfect homemaker.

300
00:14:54,375 --> 00:14:55,841
Oh...

301
00:15:00,314 --> 00:15:02,080
Oh, I was doing so great,

302
00:15:02,116 --> 00:15:04,383
but it turned out
I was a secret alcoholic.

303
00:15:04,418 --> 00:15:06,385
Yeah, good thing
it's just your imagination.

304
00:15:08,288 --> 00:15:09,888
Oh, yeah.

305
00:15:09,923 --> 00:15:11,790
When we said we'd take turns
watching all the kids,

306
00:15:11,825 --> 00:15:14,059
I never thought
it would be my turn.

307
00:15:14,094 --> 00:15:17,396
Why did I get
the choke chain?

308
00:15:17,431 --> 00:15:19,031
Go around
this side of the tree.

309
00:15:19,066 --> 00:15:21,933
This side of the tree.
This side of the tree!

310
00:15:21,969 --> 00:15:24,369
No, Ralph. No!

311
00:15:24,405 --> 00:15:26,204
Hi, Mr. Bobcat.

312
00:15:27,975 --> 00:15:29,107
You're in charge, boy.

313
00:15:30,344 --> 00:15:31,576
Now, listen, bobcat.

314
00:15:35,015 --> 00:15:36,948
Are you as sick as I am
of having grown-ups

315
00:15:36,984 --> 00:15:39,151
everywhere you go?

316
00:15:39,186 --> 00:15:42,387
I say we sneak off to the park
and have fun by ourselves.

317
00:15:42,423 --> 00:15:45,223
Great idea.
I'll text my mom.

318
00:15:45,259 --> 00:15:48,627
My Jitterbug
senior phone!

319
00:15:48,662 --> 00:15:50,162
Jitterbug call center.

320
00:15:50,197 --> 00:15:53,098
If you've fallen
and need assistance, press one.

321
00:15:53,133 --> 00:15:56,768
If you're lonely and want
to talk, press disconnect.

322
00:16:18,125 --> 00:16:21,259
Oh, I really miss my family.

323
00:16:21,295 --> 00:16:22,661
I thought they were
letting you out.

324
00:16:22,696 --> 00:16:25,030
I just wasn't ready
for the outside.

325
00:16:25,065 --> 00:16:27,632
I didn't realize how
much I needed a break.

326
00:16:27,668 --> 00:16:30,035
But maybe not
a prison break.

327
00:16:30,070 --> 00:16:33,472
Prison break?
Prison break! Yeah!

328
00:16:37,377 --> 00:16:39,811
All right, all our parents

329
00:16:39,847 --> 00:16:42,114
think we're playing after-school
rugby with Willie.

330
00:16:42,149 --> 00:16:44,516
What... Where's me scrum?

331
00:16:44,551 --> 00:16:46,384
Time for unsupervised play!

332
00:16:46,420 --> 00:16:49,054
I'm going down
the hot slide in shorts.

333
00:16:49,089 --> 00:16:50,722
I'm gonna freckle.

334
00:16:50,757 --> 00:16:54,392
I'm gonna ride the hobbyhorse
English style.

335
00:16:57,397 --> 00:17:00,899
Finally-- kids having fun
the way they were meant to.

336
00:17:00,934 --> 00:17:03,335
It just proves
that danger is not the rule,

337
00:17:03,370 --> 00:17:04,870
but the exception.

338
00:17:04,905 --> 00:17:07,139
Tornado!

339
00:17:11,879 --> 00:17:14,312
This is Kent Brockman covering
two of the biggest stories

340
00:17:14,348 --> 00:17:17,182
a tornado
and a prison break,

341
00:17:17,217 --> 00:17:20,485
while I report safely from the
Channel Six Emmy Watch bunker.

342
00:17:20,521 --> 00:17:22,320
Arnie Pye, what's the situation?

343
00:17:22,356 --> 00:17:24,222
I'm-I'm about to die, Kent.

344
00:17:24,258 --> 00:17:26,992
And worst of all
is the fact that your voice

345
00:17:27,027 --> 00:17:29,261
is the last one I'll ever hear,

346
00:17:29,296 --> 00:17:31,930
you pompous snow monkey!

347
00:17:35,802 --> 00:17:38,170
Homer, what are you
doing here?

348
00:17:38,205 --> 00:17:40,438
I had to do whatever it
takes to get you to leave.

349
00:17:40,474 --> 00:17:42,707
So I dressed up as
a prison guard.

350
00:17:42,743 --> 00:17:44,376
Now I'd better not
blow my cover.

351
00:17:44,411 --> 00:17:46,344
Get back. Get back.

352
00:17:46,380 --> 00:17:48,013
Okay, a little forward.

353
00:17:48,048 --> 00:17:50,515
Now back! Back! Back!

354
00:17:50,551 --> 00:17:51,850
Now forward.

355
00:17:51,885 --> 00:17:53,885
And back! Back! Back!

356
00:17:53,921 --> 00:17:57,122
It's very sweet
that you came for me.

357
00:17:57,157 --> 00:17:59,624
I know things won't always
be perfect----

358
00:17:59,660 --> 00:18:01,860
but they'll be better than
fighting off prison inmates

359
00:18:01,895 --> 00:18:03,895
in a tornado.

360
00:18:03,931 --> 00:18:06,531
I believe you.

361
00:18:06,567 --> 00:18:08,533
And I want to go home.

362
00:18:11,405 --> 00:18:12,971
Mwah.

363
00:18:15,676 --> 00:18:17,742
Come on, man. Take the shot.

364
00:18:17,778 --> 00:18:20,579
No, I can't. I'll fire
a warning shot at her hair.

365
00:18:21,882 --> 00:18:23,648
Oh, she uses
too much product.

366
00:18:23,684 --> 00:18:26,051
Don't worry. All the kids
are accounted for

367
00:18:26,086 --> 00:18:29,120
except for, uh... Raife Waggum.

368
00:18:29,156 --> 00:18:31,256
Well, I have a sad call to make.

369
00:18:34,261 --> 00:18:36,094
Huh. Busy.

370
00:18:36,129 --> 00:18:37,996
<i>Bart, would you like</i>
<i>some extra bacon?</i>

371
00:18:38,031 --> 00:18:39,097
Sure would, Mom.

372
00:18:39,132 --> 00:18:41,399
Okay, but you owe me.

373
00:18:41,435 --> 00:18:43,468
Sorry, sorry,
force of habit.

374
00:18:43,503 --> 00:18:45,637
It's okay, Mom. Whatever you
want, just glad you're back.

375
00:18:45,672 --> 00:18:48,306
Geez, ever since I got sprung
from the hoosegow,

376
00:18:48,342 --> 00:18:50,609
you kids have been
kind of clingy.

377
00:18:50,644 --> 00:18:53,111
No, Mom, we're just really
glad to have you back.

378
00:18:53,146 --> 00:18:54,946
And we're not nuts or
anything, but please

379
00:18:54,982 --> 00:18:57,782
don't let the refrigerator
door block my view of you.

380
00:18:57,818 --> 00:18:59,618
Aah!

381
00:18:59,653 --> 00:19:01,953
Hmm, I need some spices
from the pantry.

382
00:19:01,989 --> 00:19:03,154
I'm coming, too.

383
00:19:03,190 --> 00:19:05,757
Oh, you're pathetic.

384
00:19:05,792 --> 00:19:07,525
Let me in.

385
00:19:07,561 --> 00:19:10,795
Now, kids, give your mother
a little peace and...

386
00:19:10,831 --> 00:19:12,797
Aw.

387
00:19:16,036 --> 00:19:18,503
Aw.

388
00:19:18,538 --> 00:19:20,338
<i>I want to say two things:</i>

389
00:19:20,374 --> 00:19:23,008
<i>I love you guys and</i>
<i>we're out of peanut butter.</i>

390
00:20:08,189 --> 00:20:14,689
== sync, corrected by <font color="#00FF00">elderman</font> ==
<font color="#00FFFF">@elder_man</font>

391
00:20:53,901 --> 00:20:55,094
Shh!

