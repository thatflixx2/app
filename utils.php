<?php
	
	function parseMoovieImg($_file){
		preg_match_all('/"image" : ".+"/', $_file, $image, PREG_OFFSET_CAPTURE, 0);
		$img = $image[0][0][0];
		$pos2 = strpos($img, '"image" :"');
		$posend2 = strpos($img, '"', $pos2+11);
		return substr($img, $pos2+11, $posend2-$pos2-11);
	}
	function parseName($name){
		return preg_replace("/[^a-zA-Z0-9]+/", "", $name);
	}
	function parseOnlyNumbers($str){
		preg_match_all('/[0-9]+/', $str, $strMatched, PREG_OFFSET_CAPTURE, 0);
		$res = 0;
		try {
			$res = $strMatched[0][0][0];
		} catch (Exception $e) {
			
		}
		return $res;
	}
	function get_data($file_url, $save_to){
		//echo $file_url;
		//echo $save_to;
		$content = file_get_contents($file_url);
		file_put_contents($save_to, $content);
		//var_dump($content);
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 0); 
		curl_setopt($ch,CURLOPT_URL,$file_url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$file_content = curl_exec($ch);
		curl_close($ch);
 
		$downloaded_file = fopen($save_to, 'w');
		fwrite($downloaded_file, $file_content);
		fclose($downloaded_file);	*/	 
	}

	function file_get_contents2($url, $opt2){
		$ch = curl_init();
		//$url = urlencode($url);
		$header=array(
			'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12',
			'Accept: text/html',
			'Accept-Language:pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
			//'Accept-Encoding: gzip, deflate, sdch',
			'Accept-Charset: utf-8',
			'Keep-Alive: 115',
			'Host:www.alluc.ee',
			//'Upgrade-Insecure-Requests:1',
			'Connection: close',
		);
		//curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_COOKIEFILE,'cookies.txt');
		curl_setopt($ch, CURLOPT_COOKIEJAR,'cookies.txt');
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);

		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
		//curl_setopt($ch, CURLOPT_PROXY, '51.254.132.238:80');

		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	} 

	function file_post_contents($url, $data){
		$options = array(
		    'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		return file_get_contents($url, false, $context);
	} 
