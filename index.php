<?php
	include "utils.php";
	include "config.php";

	$uniqid = "";
	$start = round(microtime(true));
	//unset( $_COOKIE[$cookieName] );
	/*setcookie(
		$cookieName,
		$uniqid,
		time() - 1000000
	);	*/

	//var_dump($_SESSION);
	if (isset($_SESSION["USERNAME"])){
		if (!isset( $_COOKIE[$cookieName] )){
			$uniqid = $_SESSION["COOKIEID"];
			setcookie(
				$cookieName,
				$uniqid,
				time() + (60 * 60 * 24 * 365 * 20)
			);		
			//echo "DEFINE COOKIE";
		} else {
			//echo "USE FROM COOKIE";
			$uniqid = $_COOKIE[$cookieName];
		}

	} else {
		//echo "NEW ID";
		$uniqid = uniqid($start, false);
	}

	$sessSet = !isset($_SESSION["USERNAME"]);

	/*if (!isset( $_COOKIE[$cookieName] )){
		$start = round(microtime(true));
		$uniqid = uniqid($start, false);
		setcookie(
			$cookieName,
			$uniqid,
			time() + (60 * 60 * 24 * 365 * 20)
		);		
	} else {
		$uniqid = $_COOKIE[$cookieName];
	}*/
?>
<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta name="language" content="en">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ThatFlix!</title>
		<link rel="icon" type="image/png" href="/imgs/thatflix.png">
		<script type="text/javascript" src="jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="maulazyload.js"></script>
		<script type="text/javascript" src="appCfg.js"></script>
		<script type="text/javascript" src="app.js"></script>

		
		<link rel="stylesheet" href="/videojs/video-js.min.css" type="text/css">
		<script src="/videojs/video.min.js"> </script>
		<script src="/videojs/ie8/videojs-ie8.min.js"></script>
		<link rel="stylesheet" href="/videojs/videojs-resolution-switcher.css" type="text/css">

		<script src="my-plugin.js"></script>	
		<script type="text/javascript" src="https://www.gstatic.com/cv/js/sender/v1/cast_sender.js"></script>
		<link rel="stylesheet" href="videojs-chromecast.css" type="text/css">
		<script src="videojs-chromecast.js"></script>
	
		<script type="text/javascript">
			/*videojs.setGlobalOptions({
				flash: {
					swf: './videojs/video-js.swf'
				}
			});*/
			videojs.options.flash.swf = "./videojs/video-js.swf"
		</script>

	<!--	
		<script src="/videojs/videojs-resolution-switcher.js"> </script>
		<link href="http://vjs.zencdn.net/5.9.2/video-js.css" rel="stylesheet">
		<script src="http://vjs.zencdn.net/5.9.2/video.js"> </script>
		<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script> -->

		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>	
	<body>
		<div id="initial-screen" style="display: none;">
			<div class="bg-shadow"></div>
			<div class="ini-buttons-holder">
				<div id="logo"></div>
				<div class="inner-buttons-holder">
					<div class="ini-button" id="ibtn-series"> <img src="/imgs/series.png"> <span>Series</span> </div>
					<div class="ini-button second" id="ibtn-movies"> <img src="/imgs/movies.png">  <span>Movies</span> </div>
				</div>
			</div>
			<div class="user-box">
				<div id="user-name"><?php echo $sessSet ? "Login / Register" : $_SESSION["USERNAME"];//"(".$_SESSION["USERNAME"].") ".$_SESSION["EMAIL"]; ?></div>
				<div id="user-photo" class="<?php echo $sessSet ? "nousr":"" ?>">
					<?php echo $sessSet ? "" : $_SESSION["USERNAME"][0]; ?>
				</div>
				<div id="user-logout" class="<?php echo $sessSet ? "forcehidden":"" ?>">Logout</div>
			</div>
		</div>
		<div id="all-series-container" class="serie-container index" style="padding-top: 59px; display: none;">				
			<div id="categories-filter-wrap">
				<div class="btn-pop"><a href="#"></a></div>
				<div id="categories-filter">
					<div class="content-holder">
						<div class="categories">Genres:</div>
						<div id="allgens" class="category-type selected">All Genres</div>
						<!--<div class="category-type">Action</div>
						<div class="category-type">Drama</div>
						<div class="category-type">Comedy</div>
						<div class="category-type">Thriller</div>-->
					</div>
				</div>
			</div>
			<div class="header-holder">
				<div id="" class="search-form" style="margin-bottom: 0px;"> 
					<input type="text" maxlength="250" id="search" name="search" class="search" placeholder="Search">
					<div id="clear-search"></div>
				</div>
				<div class="btn-rows">
					<div class="floatbtn first selected" id="btn-series">Series</div>
					<div class="splitter"></div>
					<div class="floatbtn" id="btn-movies">Movies</div>
				</div>
				<div class="floating-logo1"></div>
			</div>
			<div id="all-series-covers"></div>
			<?php
				/*$qryFindCode = $conn->prepare("SELECT ID, NAME, IMGCOVER FROM SERIES");
				$qryFindCode->execute();
				$foundCodes = $qryFindCode->fetchAll();
				foreach ($foundCodes as $key => $value) {
					echo '<a href="serie.php?sid='.$value['ID'].'">
						<div class="serie-row" name="' . $value['NAME'] . '">
							<div class="img" img-data="/cacheimgs/'.$value['IMGCOVER'].'" style="background-image: url(/imgs/blank.png);"></div>
							<div class="name">'.$value['NAME'].'</div>
						</div>
					</a>';
				}*/
			?>
		</div>
		<div id="serie-seasons-container" style="display: none;">
			<div id="serie-seasons-container-bg"></div>
			<div class="floating-logo2"></div>
			<div id="serieseasons-back" class="floating-backbtn1"></div>
			<div id="img-cover" style="">
				<div id="serieseasons-title" class="serie-row title" style="text-align: center;"></div>
				<div id="serieseasons-description"></div>
				<div style="text-align: center;">
					<!-- <center> -->
						<img id="serieseasons-image" class="serie-image" src="/imgs/blank.png">		
					<!-- </center> -->
				</div>	
			</div>
			<div class="serie-container"></div>
		</div>
		<div id="serie-player" style="display: none;"></div>
		<div id="loading" class="loading" style="display: none;"></div>
		<div id="top-loading" class="loading smallloading" style="display: none;"></div>

		<div id="stream-loading-error" class="center-stream-loading-error" link="da-link-to-report" mediacode="123456789">
			<div class="text centered">We are sorry, but the media that you are trying to access seems currently unavailable<br/>You may wish to: "Try Reload" or "Report Broken Link".</div>

			<div id="submitting-loading" class="loading smallloading" style="display: none;"></div>
			<div id="try-reload" class="floatbtn">Try Reload</div>
			<div id="report-broken-link" class="floatbtn">Report Broken Link</div>
			<div id="get-back-to-list" class="floatbtn">Get Back to List</div>
		</div>
		<div id="user-notice-information" class="center-stream-loading-error" style="display: block;">
			<div class="text-holder">
				<div class="">
					<span style="font-size: 1.8em; color: #F00; display: block;">!!!ATTENTION!!!</span>
					Dear ThatFlix users, this informative is to let you know that ThatFlix DOES NOT hold or upload ANY video or TV show at his own web server and DOES NOT make any upload to any other web site as well.<br/>
					All content in this web site was uploaded by other users into other web sites.<br/>
					ThatFlix is just a collection of movies with some commodities to the users enjoy when chill out.<br/>
					Enjoy ThatFlix.<br/><br/>
					<span style="text-align: right; display: block; font-style: italic;">ThatFlix Staff...  </span>	
				</div>
			</div>
			<span style="font-size: 0.72em; font-style: italic; text-align: left; display: block; bottom: 21px; position: absolute; left: 222px;">By clicking this button, you agree with this conditions</span>	
			<div id="close-user-notice" class="floatbtn">That's All Right!</div>
		</div>

		<div id="user-login" mode="login" class="center-stream-loading-error" style="display: none;"> 
			<div class="type-select">
				<div id="type-login-form" class="selected">Login</div>
				<div id="type-register-form" class="">Register</div>
			</div>
			<div id="errors"> </div>

			<label for="username">Username</label>
			<input type="text" maxlength="8" id="username" name="username" class="search">
			<label for="password">Password</label>
			<input type="password" maxlength="8" id="password" name="password" class="search">
			<div id="register-part" style="display: none;">
				<label for="email">E-mail</label>
				<input type="text" maxlength="250" id="email" name="email" class="search">
			</div>

			<div id="user-login-btn" class="floatbtn">Login</div>
			<div id="user-cancel-btn" class="floatbtn">Cancel</div>			
		</div>

	</body>


	<script type="text/javascript">
		/*document.ready = function (whateva){
			$app.initialize("<?php echo $uniqid ?>");
			/ /$app._cookieID = "<?php echo $uniqid ?>";
		}*/

		$( document ).ready(function() {
			$app.initialize("<?php echo $uniqid ?>");
		});
/*
		window.onresize = function(){
			/ /FUCKING HEAD-ACHE
			$(".search-form-holder").width($("#form-padder")[0].clientWidth);
			/ /$("#search-form input")[0].value = $("#form-padder")[0].clientWidth + " - " + $("#search-form").width();
		};
		window.onready = function (){
			window.onresize(); 	
		}*/
	</script>
</html>
