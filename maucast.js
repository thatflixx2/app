(function() {
    'use strict';
    var player = null;


	var MenuButton = videojs.getComponent('MenuButton');
	var ResolutionMenuButton = videojs.extend(MenuButton, {
		constructor: function() {
			MenuButton.call(this, player);

			this.label = document.createElement('span');
			this.el().setAttribute('aria-label','Quality');
			this.controlText('Quality');
			var staticLabel = document.createElement('span');
			videojs.addClass(staticLabel, 'vjs-menu-icon');
			this.el().appendChild(staticLabel);
		}
	});

	var MenuItem = videojs.getComponent('MenuItem');
	var ResItem = videojs.extend(MenuItem, {
		constructor: function(player, options){
			options.selectable = true;
			MenuItem.call(this, player, options);
			this.src = options.src;
		}
	});
    ResItem.prototype.handleClick = function(event){
		MenuItem.prototype.handleClick.call(this, event);
		if (this.player_.currentRes != undefined && this.options_.label == this.player_.currentRes.options_.label) return;
		this.player_.currentRes = this;
		this.player_.switchToRes(this.options_);
		this.selected(true);
		/*this.selected(this.options_.label);*/
      	/*this.selected(this.options_.label === this.player_.currentRes.options_.label);*/
    };
    ResItem.prototype.update = function(){
      var selection = this.player_.currentResolution();
    };
    //ResItem.registerComponent('ResItem', ResItem);


	ResolutionMenuButton.prototype.createItems = function(){
		player.resMenuItems = [];
		var allSources = player.options_.sources;//player.children()[0].children;

		for (var i = 0; i < allSources.length; i++) {
			if (true){//allSources[i].nodeName == "SOURCE"){
				var menuItm = new ResItem(player, {
					label: allSources[i].label,//allSources[i].getAttribute("label"),
					src: allSources[i].src,//allSources[i].getAttribute("src"),
					selected: false,
				});
				if (player.currentSrc() != undefined && player.currentSrc().substr(player.currentSrc().indexOf("getdata")) == menuItm.options_.src){
					this.player_.currentRes = menuItm;
					menuItm.selected(true);
				}
				player.resMenuItems.push( menuItm );
			}
		}
		return player.resMenuItems;
	};

    var pluginFn = function( options ) {
    	player = this;

    	player.switchToRes = function(opts){
    		for (var i = 0; i < player.resMenuItems.length; i++) {
    			player.resMenuItems[i].selected(false);
    		}
    		/*opts.selected = true;*/
    		var currentTime = this.currentTime();
    		this.pause();
    		/*FRIGGIN IE KEEPS SAYING THAT VIDEO ISNT LOADED SO, FIXED TO  TIMEUPDATE EVENT... 
    			THE OTHER BROWSERS DIDNT COMPAINED ABOUT THAT*/
    		var evtLoad = "loadeddata"; /*this.player_.preload() === 'none' ? "timeupdate" : "loadeddata";  */
    		this.src(opts.src).one(evtLoad, function (){
	    		if (currentTime > 0){
	    			this.currentTime(currentTime);
	    		}
    		});
    		this.load();
	    	this.play();
    	};

    	player.ready(function(){
			var btnInstance = new ResolutionMenuButton();/*player.controlBar.addChild(new ResolutionMenuButton());*/
    		player.controlBar.resolutionSwitcher = player.controlBar.el_.insertBefore(btnInstance.el_, player.controlBar.getChild('subtitlesButton').el_);
			btnInstance.addClass("vjs-resolution-button");
		});
    };
 
    videojs.plugin( 'myplugin', pluginFn );
})();
