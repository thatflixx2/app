<?php 
	include 'utils.php'; 
	include 'config.php'; 
	error_reporting(E_ERROR );


	$SQLFIND = "SELECT * FROM EPISODES";
	//echo $SQLFIND;
	$qryFindCode =$conn->prepare($SQLFIND);
	$qryFindCode->execute();
	$foundCodes = $qryFindCode->fetchAll();


	function formatName($name){	
		$name = str_replace("&#8211;", "?", $name);	
		preg_match_all('/Episode.+\? /', $name, $found);
		//var_dump($found);
		if ( sizeof($found) == 0 )
			return $name;
		else if ( sizeof($found) > 0){
			$epdesc = $found[0][0];
			$epname = trim(str_replace($epdesc, "", $name));
			if ( trim($epname) == "" )
				return trim(str_replace("?", "", $epdesc));
			else
				return trim($epname);
		}
	}
	function replaceSAndMore($name){
		//?t='t // ?s='s // ?l='l // &amp;=& // ?m='m // ?r='r
		$val = $name;
		$val = str_replace("?t", "'t", $val);
		$val = str_replace("?s", "'s", $val);
		$val = str_replace("?l", "'l", $val);
		$val = str_replace("&amp;", "&", $val);
		$val = str_replace("?m", "'m", $val);
		$val = str_replace("?r", "'r", $val);
		return $val;
	}

	foreach ($foundCodes as $key => $value) {
		$original = trim($value['NAME']);
		$fmtdName = replaceSAndMore(trim($value['NAME']));
		$fmtdName = formatName($fmtdName);
		echo $value['NAME'] . "\t\t\t[" . $fmtdName . "]";
		if ($fmtdName != $original){
			$qry =$conn->prepare("UPDATE EPISODES SET NAME = :NAME WHERE ID = :ID");
			$qry->bindParam(':NAME', $fmtdName);
			$id = $value['ID']; 
			$qry->bindParam(':ID', $id);
			$qry->execute();
			echo " - UPDATED";
		}
		echo "\n";
	}