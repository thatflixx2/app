<?php
	$path = '/usr/lib/pear';
	set_include_path(get_include_path() . PATH_SEPARATOR . $path);

	include 'utils.php'; 
	include 'config.php';
	//include 'V8JS.php';

	function prepareSources($arrayInput){
		$result = "";
		foreach ($arrayInput as $key => $value) {
			$result .= ($result==""?"":"\n") . $value;
		}		
		return $result;
	}

///http://www.alluc.ee/stream/flashforward+s01e05+host%3Adaclips.in
	function getVideoLinksFrom($link, $website){
		$decryptThatFlixAlluc = "https://thatflix-decrypt.herokuapp.com/";
		//$decryptFile = file_get_contents("decrypt.js") . "\n\n\n\n"; 
		//$file = file_get_contents2($link, true);
		$file = file_post_contents($decryptThatFlixAlluc."gcloud", array("link"=>$link) );
		//echo "FILE: " . $file;
		$pattern = '/<a.+href="(\/l\/.+?)".+<\/a>/';
		$patternEmbed = "/document.write\(decrypt\('(.+?)', '(.+?)'\)\);/";
		$patternIframeSrc = '/<(?:iframe|IFRAME)(?:.+?)(?:src|SRC)=(?:\'|")(.+?)(?:\'|")/'; //'/<()(?:SRC|src)="(.+?)"/';
		preg_match_all($pattern, $file, $videos, PREG_OFFSET_CAPTURE, 0);
		
		$links = array();
		foreach ($videos[1] as $key => $value) {
			//preg_match_all($pattern2, $value[0], $link, PREG_OFFSET_CAPTURE, 0);
			//echo $value[0] . "\n";
			if (sizeof($links)>3) continue;
			if (!in_array($value[0], $links))
				$links[] = $value[0];
		}
		//$v8 = new V8Js();
		$result = array();
		//echo "fiadaputa";
		foreach ($links as $key => $value) {
			//$file2 = file_get_contents2($website . $value, true);
			$file2 = file_post_contents($decryptThatFlixAlluc."gcloud", array("link"=>$website . $value) );			
			//echo $website . $value . "\n"; //die();
			preg_match_all($patternEmbed, $file2, $videoEmbed, PREG_OFFSET_CAPTURE, 0);
			//var_dump($videoEmbed);
			$crypted = $videoEmbed[1][0][0];
			$crypted = str_replace("\/", "/", $crypted);
			//	echo $crypted . "\n\n"; 
			$key = $videoEmbed[2][0][0];
			//echo $file2."\n\n\n";
			//echo $key."\n\n\n";
			//$decrypted = $v8->executeString( $decryptFile . 'unbox("' . $crypted . '","' . $key . '");' );
			$decrypted = file_post_contents($decryptThatFlixAlluc, array("hash"=>$crypted, "key"=>$key) );
			//echo $decrypted . "\n\n"; 
			
			//var_dump($decrypted);
			preg_match_all($patternIframeSrc, $decrypted, $videoEmbedLink, PREG_OFFSET_CAPTURE, 0);
			//var_dump($decrypted);
			$decrypted = $videoEmbedLink[1][0][0];

			$result[] = $decrypted; 
			//echo $decrypted . "\n\n";
		}
		//var_dump($links);
		return $result;
	}
	if ($argv[1] == "-usedb"){
		$site = $argv[2];
		//$episodeID = $argv[3];
		$episodesID = explode(",", $argv[3]);		
		foreach ($episodesID as $key => $episodeID) {
			$qryEpisode = $conn->prepare("SELECT * FROM EPISODES WHERE ID = :ID");
			$qryEpisode->execute(array("ID"=>$episodeID));		
			$episode = $qryEpisode->fetchAll();
			if ( sizeof($episode) > 0 ){
				$linkRelation = $episode[0]["LINK"];
				$linkRelation = str_replace("-", "+", $linkRelation);
				//HACK TO ACHIEVE ONLY THE DESIRED EPISODES FROM A SPECIFIC SERIE/MOVIE
				$posSeason = strpos($linkRelation, "s".$episode[0]["SEASON"]);
				if ($posSeason !== false){
					$serieTitle = substr($linkRelation, 0, $posSeason);
					$serieTitle = trim( str_replace("+", " ", $serieTitle) );
					$serieTitle = urlencode( str_replace(" ", "+", $serieTitle) );
					$serieLinkRest = substr($linkRelation, $posSeason);
					$linkRelation = "$serieTitle+$serieLinkRest";
					/*echo $linkRelation. "/\n";

					echo $linkRelation. "/\n";
					echo $serieTitle . "/\n";
					echo $serieLinkRest . "/\n";
					die();*/
				}

				$link = "http://www.alluc.ee/stream/" . $linkRelation . "+host%3A" . $site;
				echo "\n" . $link . "\n";
				$website = substr($link, 0, strpos($link, "/", 10));
				$allLinks = getVideoLinksFrom( $link, $website );

				$allLinksDB = prepareSources($allLinks);

				echo $allLinksDB . "\n";
				echo "UPDATE DB FOR '" . $episode[0]["LINK"] . "' ON EPISODE '" . $episode[0]["ID"] . "'? (Y|n)?: ";
				$usrInput = "y";//readline();
				if ($usrInput == "Y" || $usrInput == "y" || trim($usrInput) == ""){
					$qryEpisode = $conn->prepare("UPDATE EPISODES SET VIDEOSOURCEM = :VIDEOSOURCEM WHERE ID = :ID");
					$qryEpisode->execute(array("ID"=>$episodeID, "VIDEOSOURCEM"=>$allLinksDB));		
		
					echo "UPDATED.\n";
				}
				//var_dump($episode);


				echo "\n";
			}
		}
		if ( isset($argv[4]) ){
			echo "------------------------------------------------------------------------------------\n";
			echo "Executing Name Finder...\n";
			$command = 'php findEpisodeName.php "' . $argv[4] . '" "' . $argv[3] . '" 2>&1';
			echo $command."\n";
			system($command);
			//echo shell_exec($command);				
		}
	} else {
		$link = $argv[1];
		$website = substr($link, 0, strpos($link, "/", 10));
		echo "Website: " . $website . "\n";
		$allLinks = getVideoLinksFrom( $link, $website );
		echo prepareSources($allLinks) . "\n";
	}

	//error_reporting(E_ERROR );
//	$dtStartProc = microtime(true);
//	echo "Started Process at: " . date("d-m-Y h:i:s", $dtStartProc ) . "\r\n";
//-------------------------------------------------------------------------------------------------------------------
//	echo ">>>>>>>>FIRST PAGE\n";
//	getMoviesFromLink($conn, 'http://www.movietubeonline.cc/a-z-movies/page/3');
//-------------------------------------------------------------------------------------------------------------------
