<?php 
	include 'utils.php'; 
	include 'config.php'; 

	function file_get_contents2($url, $opt2){
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

	    $data = curl_exec($ch);
	    curl_close($ch);
		return $data;
		/*$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
		return file_get_contents($url,false,$context);*/
	} 

	function stripCategories($file){
		$pattCategories = '/(category tag">)(?:.+?)(?=<\/a>)/';
		preg_match_all($pattCategories, $file, $mCategories, PREG_OFFSET_CAPTURE, 0);
		$result = "";
		if (sizeof($mCategories) > 0 && sizeof($mCategories[0]) > 0){
			$replaceMe = $mCategories[1][0][0];
			foreach ($mCategories[0] as $key => $value) {
				$result .= ($result == "" ? "" : ", ") . str_replace($replaceMe, "", $value[0]);
			}
		}
		return $result;
	}

	function stripWhenNTime($file){
		$pattWhenNtime = '/<li>(?!<)(?:.+)(?=<\/li>)/';
		preg_match_all($pattWhenNtime, $file, $mWhenNtime, PREG_OFFSET_CAPTURE, 0);
		$result = null;
		if (sizeof($mWhenNtime) > 0 && sizeof($mWhenNtime[0]) > 0)
			$replaceMe = "<li>";
			$result = array(str_replace($replaceMe, "", $mWhenNtime[0][0][0]), 
							str_replace($replaceMe, "", $mWhenNtime[0][1][0]));
		return $result;
	}

	function stripDirector($file){
		$pattDirector = '/Director:(.|\n)+?<span class="desc">(.+)?(?=<\/span>)/';
		preg_match_all($pattDirector, $file, $mDirector, PREG_OFFSET_CAPTURE, 0);
		if (sizeof($mDirector) > 0) return $mDirector[2][0][0];
		else return "";
	}

	function stripCast($file){
		$pattDirector = '/Cast:(.|\n)+?<span class="desc">(.+)?(?=<\/span>)/';
		preg_match_all($pattDirector, $file, $mDirector, PREG_OFFSET_CAPTURE, 0);
		if (sizeof($mDirector) > 0) return $mDirector[2][0][0];
		else return "";
	}

	function stripDescription($file){
		$pattDirector = '/<span class="description"><p>(.+)(?=<\/p>)/';
		preg_match_all($pattDirector, $file, $mDescription, PREG_OFFSET_CAPTURE, 0);
		if (sizeof($mDescription) > 0) return $mDescription[1][0][0];
		else return "";
	}

	function stripImage($file){
		$pattDirector = '/<img.+src="(.+)".+class="attachment-single-thumb.+?>/';
		preg_match_all($pattDirector, $file, $mImage, PREG_OFFSET_CAPTURE, 0);
		if (sizeof($mImage) > 0) return $mImage[1][0][0];
		else return "";
	}

	function stripWatchLinks($file){
		$pattLinks = '/<div class="col title">(?:.|\n)+?<a href="(.+)" class="name">/';
		$pattGetEmbbedLink = '/location.href=\'(.+)\'" value="Watch now!"/';
		preg_match_all($pattLinks, $file, $mLinks, PREG_OFFSET_CAPTURE, 0);
		$result = array();
		if (sizeof($mLinks) > 0 && sizeof($mLinks[1])){
			foreach ($mLinks[1] as $key => $value) {
				if ($value[0] == "#") continue;
				$embbedFile = html_entity_decode( htmlspecialchars_decode(file_get_contents2($value[0], true)) , ENT_COMPAT, 'utf-8');
				preg_match_all($pattGetEmbbedLink, $embbedFile, $mNewLink, PREG_OFFSET_CAPTURE, 0);
				if (sizeof($mNewLink) > 0){
					$result[] = $mNewLink[1][0][0];
					//var_dump($result); 
				}
			}
		}
		return $result;
	}

	function enumerateWatchLinks($links){
		$result = "";
		foreach ($links as $key => $value) {
			$result .= ($result == "" ? "" : "\n") . $value;
		}
		return $result;
	}

	function getMovieContent($conn, $link){
		$file = html_entity_decode( htmlspecialchars_decode(file_get_contents2($link, true)) , ENT_COMPAT, 'utf-8');
		$pattName = '/(<h2 class="title">.+<span.+?>)(?:.+)(?=<\/span>)/';
		preg_match_all($pattName, $file, $mName, PREG_OFFSET_CAPTURE, 0);
		$name = str_replace($mName[1][0][0], "", $mName[0][0][0]);
		$finalName = parseName($name);

		$categories = stripCategories($file);
		$director = stripDirector($file);
		$cast = stripCast($file);
		$image = stripImage($file);
		$fname = "/home/monster/ThatFlix/cacheimgs/movies/$finalName.jpg";
		$whenNTime = stripWhenNTime($file);
		$description = trim(stripDescription($file));
		$allLinks = stripWatchLinks($file);
		$enumeratedWatchLinks = enumerateWatchLinks($allLinks);
		//echo $enumeratedWatchLinks;

		echo "---$name => $link\n";
		echo "      Categs:  " . $categories . "\n";
		echo "      Director:" . $director . "\n";
		echo "      Cast:    " . $cast . "\n";
		echo "      Image:   " . substr($image, strrpos($image, "/")+1) . "\n";
		echo "      When:    " . $whenNTime[0] . "\n";
		echo "      Length:  " . $whenNTime[1] . "\n";
		echo "      Descript:" . substr($description, 0, strlen($description) > 70 ? 70 : strlen($description)) . "\n";
		echo "      Links:   " . sizeof($allLinks) . "\n";


		//Check if movie already exists
		$SQLFIND = "SELECT * FROM SERIES WHERE NAME = '$name'";
		//echo $SQLFIND;
		$qryFindCode = $conn->prepare($SQLFIND);
		$qryFindCode->execute();
		$foundCodes = $qryFindCode->fetchAll();
		//var_dump($foundCodes);
		if ( sizeof($foundCodes) <= 0 ){
			$iid = 0;
			$stype = 1;
			$iimg = "movies/".$finalName.'.jpg';
			$qry = $conn->prepare("INSERT INTO SERIES VALUES (:id, :name, :img, :descr, :genre, :daterel, :totaltime, :director, :cast, :stype);");
			$qry->bindParam(':id', $iid);
			$qry->bindParam(':name', $name);
			$qry->bindParam(':img',  $iimg);
			$qry->bindParam(':descr', $description);
			$qry->bindParam(':genre', $categories);
			$qry->bindParam(':daterel', $whenNTime[0]);
			$qry->bindParam(':totaltime', $whenNTime[1]);
			$qry->bindParam(':director', $director);
			$qry->bindParam(':cast', $cast);
			$qry->bindParam(':stype', $stype);
			$qry->execute();
			$lastId = $conn->lastInsertId();

			if (!file_exists($fname) ){
				get_data($image, $fname);
			}

			$SSQL = "INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER, VIDEOSOURCE1, VIDEOSOURCE2, VIDEOSOURCE3, VIDEOSOURCEM) 
								   VALUES (:serieid, :season, :episode, :epname, :link, :epcover, :video1, :video2, :video3, :videom);";

			$qry2 = $conn->prepare($SSQL);
			$vsourceEmpty = "";
			$epFile = "../cacheimgs/" . $iimg;
			//$qry2->bindParam(':id', 0);
			$qry2->bindParam(':serieid', $lastId);
			$qry2->bindParam(':season', $iid);
			$qry2->bindParam(':episode', $iid);
			$qry2->bindParam(':epname', $name);
		    $qry2->bindParam(':link', $finalName);
		    $qry2->bindParam(':epcover', $epFile);  
		    $qry2->bindParam(':video1', $vsourceEmpty);
		    $qry2->bindParam(':video2', $vsourceEmpty); 
		    $qry2->bindParam(':video3', $vsourceEmpty);
		    $qry2->bindParam(':videom', $enumeratedWatchLinks);
			$qry2->execute();
		}
		echo "\n";
	}

	function getMoviesFromLink($conn, $link){
		$file = file_get_contents2($link, true);

		$pattern = '/<div class=\"item\">(.|\n)+?<\/div>/';
		preg_match_all($pattern, $file, $matches, PREG_OFFSET_CAPTURE, 0);	
		foreach ($matches[0] as $key => $value) {
			$val = $value[0];
			preg_match_all('/http.+\/(?=")/', $val, $links, PREG_OFFSET_CAPTURE, 0);	
			///var_dump($links);
			if ( sizeof($links) > 0 ){
				//echo "\t" . $links[0][0][0] . "\n";
				getMovieContent($conn, $links[0][0][0]);
			}

		}
		preg_match_all('/<div class=\"pagination\">(.|\n)+?<\/div>/', $file, $matchNetPage, PREG_OFFSET_CAPTURE, 0);
		if ($matchNetPage != null && sizeof($matchNetPage) >0){
			preg_match_all('/<a.+?<\/a>/', $matchNetPage[0][0][0], $matchPages, PREG_OFFSET_CAPTURE, 0);	
			if ($matchPages != null && sizeof($matchPages) >0){
				foreach ($matchPages[0] as $key => $value) {
					$link = $value[0];
					if ( strpos($link, "NEXT") > 0 ){
						preg_match_all('/http.+?(?=")/', $link, $npLinkMatched, PREG_OFFSET_CAPTURE, 0);	
						echo ">>>>>>>>NEXT PAGE FOUND > " . $npLinkMatched[0][0][0] . "\n";
						//echo $npLinkMatched[0][0][0] . "\n";
						//exit;
						getMoviesFromLink($conn, $npLinkMatched[0][0][0]);
						break;
					}
				}
			}
		}
		//var_dump($matchNetPage);
	}

	//error_reporting(E_ERROR );
	$dtStartProc = microtime(true);
	echo "Started Process at: " . date("d-m-Y h:i:s", $dtStartProc ) . "\r\n";
//-------------------------------------------------------------------------------------------------------------------
	echo ">>>>>>>>FIRST PAGE\n";
	getMoviesFromLink($conn, 'http://www.movietubeonline.cc/a-z-movies/page/3');
//-------------------------------------------------------------------------------------------------------------------
	$dtFinishedProc = microtime(true);
	echo "\r\n\r\nStarted Process at: " . date("d-m-Y h:i:s", $dtStartProc ) . "\r\n";
	echo "Finished Process at: " . date("d-m-Y h:i:s", $dtFinishedProc ) . "\r\n";
	$totalTook = $dtFinishedProc-$dtStartProc;
	$duration = $totalTook;
	$hours = (int)($duration/60/60);
	$minutes = (int)($duration/60)-$hours*60;
	$seconds = (int)$duration-$hours*60*60-$minutes*60;
	echo "Took: $hours:$minutes:$seconds - $totalTook ms\r\n";
?>