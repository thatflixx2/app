﻿1
00:00:07,516 --> 00:00:08,715
D'oh!

2
00:00:08,750 --> 00:00:10,016
(tires screeching)

3
00:00:10,052 --> 00:00:11,685
(grunts)

4
00:00:13,322 --> 00:00:16,456
ANNOUNCER: <i>It's fourth and nine</i>
<i>for the Simpsons.</i>

5
00:00:16,491 --> 00:00:18,892
<i>Homer takes the snap,</i>
<i>looking for a receiver.</i>

6
00:00:18,927 --> 00:00:20,026
<i>They're all covered.</i>

7
00:00:20,062 --> 00:00:22,262
<i>He's gonna sneak it in!</i>

8
00:00:22,297 --> 00:00:23,663
(crowd cheering)

9
00:00:23,699 --> 00:00:28,568
<i>Oh, man, that is why</i>
<i>he gets to control the remote.</i>

10
00:00:28,604 --> 00:00:32,604
<font color="#00FF00">♪ The Simpsons 27x14 ♪</font>
<font color="#00FFFF">Gal of Constant Sorrow</font>
Original Air Date on February 21

11
00:00:32,605 --> 00:00:35,905
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

12
00:00:35,944 --> 00:00:37,077
(Simpsons eating)

13
00:00:37,112 --> 00:00:40,447
Mmm.
Oh, this tile is loose.

14
00:00:40,482 --> 00:00:43,283
I'll have to call a handy man.

15
00:00:43,318 --> 00:00:44,718
Why'd you say it like that?

16
00:00:44,753 --> 00:00:47,954
With a pause between the words.
Are you saying I'm not handy?

17
00:00:47,990 --> 00:00:50,957
That's how you
say it: handy man.

18
00:00:50,993 --> 00:00:52,559
It's handyman.

19
00:00:52,594 --> 00:00:56,196
That's what I said:
handy... man.

20
00:00:56,231 --> 00:00:57,464
(exclaims)
Marjorie,

21
00:00:57,499 --> 00:00:59,733
there is the profession:
handyman,

22
00:00:59,768 --> 00:01:01,268
and there are men who are handy.

23
00:01:01,303 --> 00:01:03,737
Which are you saying I'm not?

24
00:01:03,772 --> 00:01:05,105
Tell me.

25
00:01:05,140 --> 00:01:07,207
A handy...

26
00:01:07,242 --> 00:01:08,441
man.

27
00:01:08,477 --> 00:01:09,809
(exclaims)
Face it, homeboy,

28
00:01:09,845 --> 00:01:11,077
you ain't handy.

29
00:01:11,113 --> 00:01:12,879
Unless we need
a big fat paperweight.

30
00:01:12,915 --> 00:01:14,314
I'll paperweight you!

31
00:01:14,349 --> 00:01:15,815
Oh! (groaning)
(grunting)

32
00:01:15,851 --> 00:01:17,918
(grumbles)
Homie, sweetie,

33
00:01:17,953 --> 00:01:19,319
replacing a tile
is something

34
00:01:19,354 --> 00:01:21,588
you really need a
professional for.

35
00:01:21,623 --> 00:01:23,323
I know how to replace a tile.

36
00:01:23,358 --> 00:01:24,724
All guys do.

37
00:01:24,760 --> 00:01:26,726
We talk about it all the time.

38
00:01:26,762 --> 00:01:28,662
Grout?

39
00:01:28,697 --> 00:01:31,164
There's no stopping
what I've started here.

40
00:01:31,199 --> 00:01:33,600
Here's your tile.
Have fun.

41
00:01:33,635 --> 00:01:36,336
Handy... man.

42
00:01:36,371 --> 00:01:38,672
Lisa, honey, do you have any
idea how to replace a floor...?

43
00:01:38,707 --> 00:01:40,473
Here's a video tutorial
on replacing a tile.

44
00:01:40,509 --> 00:01:42,943
I don't know if I need a
whole tutorial to teach me...

45
00:01:42,978 --> 00:01:45,845
You're tapping. You have to swipe.
I'm-I'm swiping, I'm swiping.

46
00:01:45,881 --> 00:01:47,514
Pretend you're swiping
chocolate icing off a cake.

47
00:01:47,549 --> 00:01:49,316
All right, how's that?
Ah, see? There.

48
00:01:49,351 --> 00:01:50,951
There it is, yeah. Perfect.
There really is icing on here.

49
00:01:50,986 --> 00:01:52,218
All right, then.

50
00:01:53,689 --> 00:01:55,622
Hi there. If you're
watching this video,

51
00:01:55,657 --> 00:01:57,357
you've got a job
that needs doing

52
00:01:57,392 --> 00:01:59,292
but you're too cheap
to pay for it.

53
00:01:59,328 --> 00:02:01,962
Man, this guy's inside my head.

54
00:02:01,997 --> 00:02:03,997
Now, replacing a tile
is a simple task.

55
00:02:04,032 --> 00:02:07,701
As long as the substrate
beneath the tile isn't rotted.

56
00:02:11,540 --> 00:02:13,473
If there is rot
in your substrate,

57
00:02:13,508 --> 00:02:17,143
we will now play loud music
so you can curse.

58
00:02:17,179 --> 00:02:18,945
(salsa music playing,
Homer shouting)

59
00:02:18,981 --> 00:02:21,715
Stupid floor! Why...

60
00:02:21,750 --> 00:02:24,351
Why was I born a homeowner?

61
00:02:24,386 --> 00:02:25,618
On your marks.
On your marks.

62
00:02:25,654 --> 00:02:26,619
Get set.
Get set.

63
00:02:26,655 --> 00:02:28,088
Twins!
Twins!

64
00:02:28,123 --> 00:02:30,657
Loser!

65
00:02:30,692 --> 00:02:31,825
Oh, boy.

66
00:02:31,860 --> 00:02:33,326
(Ralph giggling)

67
00:02:33,362 --> 00:02:36,363
Oh. Why is everyone passing us?

68
00:02:37,766 --> 00:02:41,001
You know how scared I am
of going on a slant.

69
00:02:41,036 --> 00:02:42,502
Oh.
(grunts)

70
00:02:42,537 --> 00:02:43,570
Ow!

71
00:02:43,605 --> 00:02:45,572
Latchkey kids rule!

72
00:02:50,846 --> 00:02:52,245
I can't die now.

73
00:02:52,280 --> 00:02:53,913
I actually did my homework.

74
00:02:53,949 --> 00:02:55,215
(grunts)

75
00:03:01,089 --> 00:03:02,922
(groans)
I'm sorry, ma'am.

76
00:03:02,958 --> 00:03:04,524
Come spring, I'll go
get that for you.

77
00:03:04,559 --> 00:03:05,992
Dang good-for-nothin' cart.

78
00:03:06,028 --> 00:03:08,428
Always fighting to go left
when I wanted to go right.

79
00:03:08,463 --> 00:03:10,463
It was my only friend.
(crying)

80
00:03:17,372 --> 00:03:20,407
Now take your sponge,
wipe away the excess grout

81
00:03:20,442 --> 00:03:21,941
and you're done.

82
00:03:23,912 --> 00:03:25,211
Oh, my God,

83
00:03:25,247 --> 00:03:27,914
it looks like
what it's supposed to look like.

84
00:03:27,949 --> 00:03:30,016
I did man work!

85
00:03:30,052 --> 00:03:31,618
My hero.

86
00:03:31,653 --> 00:03:33,953
I'm happy to say I was wrong.

87
00:03:33,989 --> 00:03:36,423
I'll put Maggie down
and make you a snack.

88
00:03:36,458 --> 00:03:38,258
Something bacon-y.

89
00:03:38,293 --> 00:03:39,526
(whoops)

90
00:03:39,561 --> 00:03:41,594
A bacon apology sandwich.

91
00:03:41,630 --> 00:03:43,630
(meowing)

92
00:03:45,067 --> 00:03:46,366
What? No.

93
00:03:46,401 --> 00:03:47,734
No. No.

94
00:03:48,970 --> 00:03:50,637
(meow)
(scratching)

95
00:03:50,672 --> 00:03:52,305
Oh, I sealed in the cat.

96
00:03:52,340 --> 00:03:54,441
If it dies,
it'll stink up the whole house.

97
00:03:54,476 --> 00:03:56,142
Also, the kids like it.

98
00:03:56,178 --> 00:03:58,378
(meowing)

99
00:03:58,413 --> 00:03:59,979
Stupid genius cat.

100
00:04:00,015 --> 00:04:03,416
She went from under the floor
to inside the wall.

101
00:04:03,452 --> 00:04:05,218
Here, kitty, kitty,
kitty, kitty.

102
00:04:05,253 --> 00:04:06,719
MARGE:
More repairs?

103
00:04:06,755 --> 00:04:09,789
Aah! Marge, I should
tell you something.

104
00:04:09,825 --> 00:04:11,791
I knew it.
You're not a handy...

105
00:04:11,827 --> 00:04:13,827
No. You did not know it.

106
00:04:13,862 --> 00:04:16,830
I was going to say I'm not
stopping with the tile.

107
00:04:16,865 --> 00:04:18,631
Oh, no. Once
you've been bitten

108
00:04:18,667 --> 00:04:21,267
by the repair bug,
you can't quit.

109
00:04:21,303 --> 00:04:24,804
How would you like hot water
in the bathtub again?

110
00:04:24,840 --> 00:04:28,174
(gasps) I'll go get the "H" knob
out of my jewelry box.

111
00:04:28,210 --> 00:04:31,845
Mmm. Don't take long,
handy man.

112
00:04:31,880 --> 00:04:33,279
Handyman.

113
00:04:33,315 --> 00:04:34,914
(giggles)

114
00:04:34,950 --> 00:04:38,818
Okay, cat, I'll get you out
tomorrow, but here's dinner.

115
00:04:38,854 --> 00:04:42,422
Lasagna, which I know cats like.

116
00:04:42,457 --> 00:04:44,424
Oh!

117
00:04:44,459 --> 00:04:46,426
Nothing to see here.
Just kneeling

118
00:04:46,461 --> 00:04:48,094
in front of the
electric outlet,

119
00:04:48,130 --> 00:04:50,263
appreciating Edison's miracle.

120
00:04:50,298 --> 00:04:52,465
Look, Hettie, you
got two choices.

121
00:04:52,501 --> 00:04:54,033
Under the bed
or in the closet.

122
00:04:54,069 --> 00:04:56,603
I suppose I will take

123
00:04:56,638 --> 00:04:58,171
the closet.

124
00:04:58,206 --> 00:05:00,140
Oh, my goodness.
This is like my heyday

125
00:05:00,175 --> 00:05:02,041
when I was livin' in that car.

126
00:05:02,077 --> 00:05:04,144
You know, you get enough
parking tickets on the front,

127
00:05:04,179 --> 00:05:06,012
they act like curtains.

128
00:05:06,982 --> 00:05:09,616
(sniffs)
Mmm.

129
00:05:09,651 --> 00:05:11,618
Hey, mornin', boss.

130
00:05:11,653 --> 00:05:13,553
Hey, what you take
in your coffee?

131
00:05:13,588 --> 00:05:15,388
'Cause I got sugar
and I got something called

132
00:05:15,423 --> 00:05:18,057
"Not for individual sale."

133
00:05:18,093 --> 00:05:20,793
Listen. I don't think you should
get too comfortable here.

134
00:05:20,829 --> 00:05:22,962
Okay, okay.
Thought you might say that,

135
00:05:22,998 --> 00:05:26,299
but, uh, what if
I make it amenable to you?

136
00:05:26,334 --> 00:05:29,235
If you hide me here,
I'll give you a dollar a day.

137
00:05:29,271 --> 00:05:31,371
Here's one week in advance.

138
00:05:31,406 --> 00:05:34,040
Yes, I'm a slumlord.

139
00:05:34,075 --> 00:05:36,576
DRAKE: ♪ Started from the bottom,
now we're here ♪

140
00:05:36,611 --> 00:05:39,045
♪ Started from the bottom,
now the whole team here ♪

141
00:05:39,080 --> 00:05:42,415
♪ Started from the bottom,
now we're here ♪

142
00:05:42,450 --> 00:05:45,518
♪ Started from the bottom,
now the whole team here ♪

143
00:05:45,554 --> 00:05:48,188
♪ Started from the bottom,
now we're here ♪

144
00:05:48,223 --> 00:05:50,723
♪ Started from the bottom,
now my whole team here ♪

145
00:05:50,759 --> 00:05:53,426
♪ Started from the bottom,
now we're here ♪

146
00:05:53,461 --> 00:05:56,429
♪ Started from the bottom,
now my whole team here ♪

147
00:05:56,464 --> 00:05:58,198
♪ Started from the bottom,
now we're here ♪

148
00:05:58,233 --> 00:06:02,302
♪ Started from
the bottom, now we're here ♪

149
00:06:02,337 --> 00:06:04,170
♪ I done kept it real
from the jump ♪

150
00:06:04,206 --> 00:06:07,273
♪ Living at my mama house,
we'd argue every mornin' ♪

151
00:06:07,309 --> 00:06:09,976
♪ I was-- I was trying
to get it on my own ♪

152
00:06:10,011 --> 00:06:12,712
♪ Working all night,
traffic on the way home ♪

153
00:06:12,747 --> 00:06:15,582
♪ And my uncle calling me
like "Where ya at? ♪

154
00:06:15,617 --> 00:06:18,251
♪ I gave you the keys,
told you bring it right back" ♪

155
00:06:18,286 --> 00:06:20,820
♪ I just-- I just think
it's funny how it goes ♪

156
00:06:20,855 --> 00:06:23,823
♪ Now I'm on the road,
half a million for a show ♪

157
00:06:23,858 --> 00:06:25,258
♪ And we started from the... ♪

158
00:06:25,293 --> 00:06:27,427
Hmm.

159
00:06:28,730 --> 00:06:30,797
Sorry, cat.
Had to go to work.

160
00:06:30,832 --> 00:06:33,032
Then there was a freeway chase
on the news.

161
00:06:33,068 --> 00:06:34,567
Had to watch it till the end.

162
00:06:34,603 --> 00:06:36,903
The guy got arrested
in a cul-de-sac.

163
00:06:36,938 --> 00:06:39,105
They never do
what I yell at them to do.

164
00:06:39,140 --> 00:06:41,241
(meowing, scratching)

165
00:06:42,611 --> 00:06:44,611
Oh, you're in the ceiling?

166
00:06:47,449 --> 00:06:49,415
Do not use top step?

167
00:06:49,451 --> 00:06:51,684
Stupid government,
trying to keep us down.

168
00:06:51,720 --> 00:06:55,088
(grunting)

169
00:06:56,291 --> 00:06:58,291
Where is Bart
getting this money?

170
00:07:00,695 --> 00:07:01,995
(exclaims) Lisa?

171
00:07:02,030 --> 00:07:04,931
Just what do you
think you're up to?

172
00:07:04,966 --> 00:07:06,165
Mm...

173
00:07:06,201 --> 00:07:08,334
Come on. You can
tell Dr. Tuna.

174
00:07:08,370 --> 00:07:10,603
Okay, I admit it. I think
Bart's up to something funny,

175
00:07:10,639 --> 00:07:12,605
so I'm snooping in his room.

176
00:07:12,641 --> 00:07:15,108
No snooping. You know what
they say about curiosity.

177
00:07:15,143 --> 00:07:17,110
It killed the cat?
The cat's fine!

178
00:07:17,145 --> 00:07:20,446
Stop asking
about the cat!

179
00:07:20,482 --> 00:07:22,982
MAN: Lose a Kewpie doll
in front of your gal.

180
00:07:23,018 --> 00:07:26,519
(tuning guitar)

181
00:07:26,554 --> 00:07:28,121
Hey, boss.
(clears throat)

182
00:07:28,156 --> 00:07:29,956
We have a situation here.

183
00:07:29,991 --> 00:07:32,358
You owe me three
weeks' back rent.

184
00:07:32,394 --> 00:07:33,793
Aha!

185
00:07:33,828 --> 00:07:35,795
Bart Simpson,
you are cruelly exploiting

186
00:07:35,830 --> 00:07:38,197
a poor,
unfortunate woman.

187
00:07:38,233 --> 00:07:41,501
So, you're gonna tell Mom
and she's gonna kick Hettie out.

188
00:07:41,536 --> 00:07:43,002
Girlie, ain't you got a heart?

189
00:07:43,038 --> 00:07:44,837
Nope. All brain,
no heart.

190
00:07:44,873 --> 00:07:48,274
Yeah. She looks like
a little bitty railroad bull.

191
00:07:48,310 --> 00:07:50,276
Now I got to sing for my supper.

192
00:07:50,312 --> 00:07:52,111
I wrote this one a few days ago.

193
00:07:52,147 --> 00:07:54,914
This is a song about loss.

194
00:07:56,251 --> 00:08:00,053
♪ It was a cart ♪

195
00:08:00,088 --> 00:08:04,223
♪ Meant for shopping ♪

196
00:08:04,259 --> 00:08:07,226
♪ It came to mean ♪

197
00:08:07,262 --> 00:08:11,230
♪ Much more to me ♪

198
00:08:11,266 --> 00:08:15,401
♪ It held the pan ♪

199
00:08:15,437 --> 00:08:18,838
♪ I cook my slop in ♪

200
00:08:18,873 --> 00:08:25,845
♪ And my old PlayStation 3 ♪

201
00:08:25,880 --> 00:08:29,916
♪ It was my home ♪

202
00:08:29,951 --> 00:08:33,920
♪ And my place of worship ♪

203
00:08:33,955 --> 00:08:37,690
♪ It was my home ♪

204
00:08:37,726 --> 00:08:41,594
♪ And it was kinda my car ♪

205
00:08:41,629 --> 00:08:48,601
♪ Now you're just rolling
underwater ♪

206
00:08:48,636 --> 00:08:53,606
♪ While Safeway wonders ♪

207
00:08:53,641 --> 00:08:57,477
♪ Where you are. ♪

208
00:08:59,914 --> 00:09:02,382
Whoa! My roommate
is talented!

209
00:09:03,822 --> 00:09:05,968
If I know
my Smithsonian Folkways,

210
00:09:06,068 --> 00:09:08,300
that was
an Appalachian folk song.

211
00:09:08,302 --> 00:09:10,269
Well, I'm from
Lickskillet, Kentucky,

212
00:09:10,304 --> 00:09:12,171
just shy of the
Appalachia Trail.

213
00:09:12,206 --> 00:09:15,607
Really? Oh, I love, love, love
indigenous mountain music.

214
00:09:15,643 --> 00:09:18,443
(chuckles)
Well, isn't that fascinatin'?

215
00:09:18,479 --> 00:09:21,346
I'm glad you
liked the song.

216
00:09:21,382 --> 00:09:24,116
Yes. I liked it
very, very, very m...

217
00:09:24,151 --> 00:09:26,285
Oh! You want money.

218
00:09:26,320 --> 00:09:28,787
I'm sorry. I don't have any.
(chuckles)

219
00:09:28,823 --> 00:09:31,190
I'm also a musician.

220
00:09:31,225 --> 00:09:33,926
(chuckles)
But you can stay in our house.

221
00:09:33,961 --> 00:09:36,595
Ha-ha! It's a deal!
Back in the boy's closet.

222
00:09:36,630 --> 00:09:38,096
No, Hettie.
You're a human being.

223
00:09:38,132 --> 00:09:40,165
You can sleep in my closet.

224
00:09:40,201 --> 00:09:42,000
Hmm. Well, la-di-da.

225
00:09:42,036 --> 00:09:43,435
How much you charge?

226
00:09:43,470 --> 00:09:46,104
Nothing. Maybe we can talk
a little music.

227
00:09:46,140 --> 00:09:48,607
Ugh. Boy, what's
your price again?

228
00:09:48,642 --> 00:09:51,109
Okay, we'll only talk about it
if you want to!

229
00:09:51,145 --> 00:09:52,344
But it would be an honor.

230
00:09:52,379 --> 00:09:53,612
Okay, let's go.

231
00:09:53,647 --> 00:09:55,013
Oh, you've hurt
yourself.

232
00:09:55,049 --> 00:09:56,615
Uh, nope, that's syrup.

233
00:09:56,650 --> 00:09:58,183
Oh, let me find you
a Wet-Nap.

234
00:09:58,219 --> 00:09:59,952
I just woke up
from a wet nap.

235
00:09:59,987 --> 00:10:01,987
Okay.
Conversation's over.

236
00:10:02,022 --> 00:10:02,821
<i>Check, one, two.</i>

237
00:10:02,857 --> 00:10:04,156
All right.

238
00:10:04,191 --> 00:10:05,691
No, no, no, leave
the windows open.

239
00:10:05,726 --> 00:10:08,227
I like crickets and
night breeze in my music.

240
00:10:08,262 --> 00:10:10,896
They do go well together.
Have you always been musical?

241
00:10:10,931 --> 00:10:13,899
You know, first sound I ever
heard was my daddy fiddlin'.

242
00:10:13,934 --> 00:10:16,468
His name was Bascom Lee Boggs.

243
00:10:16,503 --> 00:10:18,203
(gasps)
Bascom Lee Boggs?!

244
00:10:18,239 --> 00:10:20,706
He played with
some seminal Appalachian bands,

245
00:10:20,741 --> 00:10:22,541
like Snug
and the Cousin Huggers,

246
00:10:22,576 --> 00:10:24,643
Lead Paint Larry
and the Drooly Boys,

247
00:10:24,678 --> 00:10:27,045
Howlin' Sue
and Her Vestigial Organ,

248
00:10:27,081 --> 00:10:29,214
and Bloody Mary
and the Coalmine Canaries.

249
00:10:29,250 --> 00:10:31,568
(inhales) Did your daddy
teach you to fiddle?

250
00:10:31,592 --> 00:10:32,418
No.

251
00:10:32,419 --> 00:10:34,653
He was gonna start me out
on the cigar box banjo,

252
00:10:34,688 --> 00:10:37,723
but, uh, before he could,
he-he lost his sight.

253
00:10:37,758 --> 00:10:39,224
Oh, no. I'm sorry.

254
00:10:39,260 --> 00:10:41,493
Yeah, he got shot
in the face.

255
00:10:41,528 --> 00:10:44,229
(gasps) You know them puzzles
where you slide the tiles?

256
00:10:44,265 --> 00:10:45,998
He looks a little
like that now.

257
00:10:46,033 --> 00:10:48,834
My mother was an amazing
banjo player. Mmm.

258
00:10:48,869 --> 00:10:52,437
Unfortunately, she did also go blind.
Really?

259
00:10:52,473 --> 00:10:54,873
Yeah, it was winter
and she was runnin' a fever

260
00:10:54,909 --> 00:10:57,609
and, um, then she got shot
in the face.

261
00:10:57,645 --> 00:11:00,879
Oh, wow. Hettie,
you've had such a sad life.

262
00:11:00,915 --> 00:11:03,115
But you know what?
That's gonna turn around

263
00:11:03,150 --> 00:11:05,617
when I show the world
what it's missing in you.

264
00:11:05,653 --> 00:11:08,287
Well, I don't know if
the world's gonna get me,

265
00:11:08,322 --> 00:11:11,123
but, child, it sure feels
good to know that you do.

266
00:11:11,158 --> 00:11:12,557
Mmm.
Mmm.

267
00:11:12,593 --> 00:11:13,892
You're having a moment
with someone

268
00:11:13,928 --> 00:11:15,627
who has so few moments.

269
00:11:15,663 --> 00:11:17,696
This will never be forgotten.

270
00:11:17,731 --> 00:11:22,367
Oh, baby girl, I feel like I'm
back home in poorest Appalachia.

271
00:11:22,403 --> 00:11:24,036
Thank you.

272
00:11:24,071 --> 00:11:28,040
Okay. And three, two, one...

273
00:11:28,075 --> 00:11:29,441
(playing guitar)

274
00:11:29,476 --> 00:11:34,446
♪ Oh, Barnes & Noble ♪

275
00:11:34,481 --> 00:11:39,217
♪ Oh, I am sorry to see ♪

276
00:11:39,253 --> 00:11:40,819
♪ That your store ♪

277
00:11:40,854 --> 00:11:45,457
♪ Has been shuttered ♪

278
00:11:45,492 --> 00:11:49,494
♪ I miss your
bathroom policy ♪

279
00:11:49,530 --> 00:11:55,500
♪ I would sleep
in your stalls ♪

280
00:11:55,536 --> 00:12:00,138
♪ For many a long day... ♪

281
00:12:00,174 --> 00:12:01,907
It's lovely,
but I want you to know

282
00:12:01,942 --> 00:12:04,776
I've-I've truly come
to hate music.

283
00:12:04,812 --> 00:12:06,411
That is so sad.

284
00:12:06,447 --> 00:12:08,814
I wanted to arrange a concert
in Springfield Park.

285
00:12:08,849 --> 00:12:11,616
We needed the school's lights
and sound system.

286
00:12:11,652 --> 00:12:15,187
Lights? Sweetie, you don't know
a Fresnel from a gobo.

287
00:12:15,222 --> 00:12:17,589
Well, I think you'll help me
every way you can.

288
00:12:17,624 --> 00:12:19,257
And why, pray tell, is that?

289
00:12:19,293 --> 00:12:21,660
Because deep down, there's
still a part of you

290
00:12:21,695 --> 00:12:24,262
that remembers music
can be magical.

291
00:12:24,298 --> 00:12:25,931
(chuckles)
You're right.

292
00:12:25,966 --> 00:12:28,000
I can't bear
to kill your passion.

293
00:12:28,035 --> 00:12:30,335
It will live at least
until third grade,

294
00:12:30,371 --> 00:12:32,971
when you get Mrs. Ortner.

295
00:12:33,007 --> 00:12:35,173
So, I got back together
with Jeff.

296
00:12:35,209 --> 00:12:38,176
Then, of course, I realized
there was a reason we broke up.

297
00:12:38,212 --> 00:12:39,778
(playing tuba)

298
00:12:39,813 --> 00:12:42,881
That sounds awful,
and you look stupid doing it.

299
00:12:42,916 --> 00:12:46,084
(guitar playing)

300
00:12:56,964 --> 00:12:58,764
(grumbling)

301
00:13:00,701 --> 00:13:03,468
(knocking)

302
00:13:03,504 --> 00:13:05,804
Hettie, Hettie, I have
a surprise for you!

303
00:13:05,839 --> 00:13:08,974
(gasps) Oh, well,
barbecue my bedbugs.

304
00:13:09,009 --> 00:13:10,976
That is glorious.

305
00:13:11,011 --> 00:13:14,212
(chuckles) To pay for it,
I sold my Malibu Stacys.

306
00:13:14,248 --> 00:13:17,816
Which, from a feminist
perspective, was long overdue.

307
00:13:17,851 --> 00:13:21,319
But I have a Ken that I'm
transitioning to a Stacy.

308
00:13:21,355 --> 00:13:22,721
Try it on, try it on,
try it on!

309
00:13:22,756 --> 00:13:24,890
I heard you the second time!

310
00:13:26,260 --> 00:13:28,493
Mmm, mmm. Why,
thank you, Your Majesty,

311
00:13:28,529 --> 00:13:30,429
I will have
another cup of soda.

312
00:13:30,464 --> 00:13:32,864
Lis, Lis, Lis.
(grunting) What?!

313
00:13:32,900 --> 00:13:35,267
I've seen you like this
before, and it ends badly.

314
00:13:35,302 --> 00:13:36,935
She is gonna
break your heart.

315
00:13:36,970 --> 00:13:38,937
And your heart's as
tough as a soap bubble.

316
00:13:38,972 --> 00:13:41,106
Remember how upset you got
over that dead plant?

317
00:13:41,141 --> 00:13:44,109
Oh. It's the seven-month
anniversary of that.

318
00:13:44,144 --> 00:13:45,610
(crying)

319
00:13:45,646 --> 00:13:47,279
Oh, geez, here
we go again.

320
00:13:47,314 --> 00:13:49,714
Ugh. You know what, Bart?
Just get out!

321
00:13:49,750 --> 00:13:52,117
And get Dad's keys and
drive us to the radio station.

322
00:13:52,152 --> 00:13:54,119
And then get out!
Wait until we're done.

323
00:13:54,154 --> 00:13:57,255
Then drive us back.
Then get out!

324
00:13:57,291 --> 00:13:59,257
(hammering, sawing)

325
00:13:59,293 --> 00:14:00,859
(crash)
HOMER: Damn it!

326
00:14:00,894 --> 00:14:02,861
(knocking)
(footsteps approaching)

327
00:14:02,896 --> 00:14:04,696
Hey.

328
00:14:04,731 --> 00:14:06,965
Hello, Homer.
I need to do the laundry.

329
00:14:07,000 --> 00:14:09,768
Uh, uh, I'll do it!
Oh.

330
00:14:09,803 --> 00:14:12,637
HOMER: <i>You idiot. Now we'll always</i>
<i>have to do the laundry!</i>

331
00:14:12,673 --> 00:14:14,272
(laughs)

332
00:14:14,308 --> 00:14:16,608
I'll do it wrong
so she'll never ask again.

333
00:14:16,643 --> 00:14:18,810
<i>Homer, you're a genius!</i>

334
00:14:18,846 --> 00:14:20,545
Thanks, brain!

335
00:14:20,581 --> 00:14:22,781
<i>Oh, I'm not your brain.</i>
<i>I'm a blood clot.</i>

336
00:14:24,952 --> 00:14:26,918
Okay. I'm goin' in.

337
00:14:26,954 --> 00:14:29,354
Thank God I never put in
that insulation.

338
00:14:31,525 --> 00:14:33,225
(grunting)

339
00:14:35,662 --> 00:14:37,062
Whoo-hoo!

340
00:14:37,097 --> 00:14:39,831
I glide silently
through the wall.

341
00:14:39,867 --> 00:14:43,668
Silently, ever silently.

342
00:14:43,704 --> 00:14:45,804
(Snowball II mewing)

343
00:14:45,839 --> 00:14:47,239
(grunting)

344
00:14:47,274 --> 00:14:49,307
Aah! Ooh, rogue nails! Oh.

345
00:14:49,343 --> 00:14:50,909
(hissing)
Hot pipe!

346
00:14:50,944 --> 00:14:52,477
(panting)

347
00:14:52,513 --> 00:14:54,279
Gah. It's... Oh.

348
00:14:54,314 --> 00:14:56,381
Rough tongue! Aah!

349
00:14:59,653 --> 00:15:01,186
Oh, that's just...

350
00:15:01,221 --> 00:15:02,687
(Santa's Little Helper howling)

351
00:15:02,723 --> 00:15:04,156
Aw, crap.

352
00:15:04,191 --> 00:15:06,158
(barking)

353
00:15:06,193 --> 00:15:09,261
Sure am glad
we don't have a horse.

354
00:15:09,296 --> 00:15:10,996
♪ ♪

355
00:15:11,171 --> 00:15:13,171
Welcome back to<i> Mountain Trax.</i>

356
00:15:13,206 --> 00:15:16,441
I'm here today with the one
and only Miss Hettie Mae Boggs,

357
00:15:16,476 --> 00:15:18,910
the baby of the Boggs Family.

358
00:15:18,945 --> 00:15:21,980
(laughing): Well, I'm a little big
for a baby.

359
00:15:22,015 --> 00:15:24,482
Although my mother did have
a 21-pounder.

360
00:15:24,518 --> 00:15:26,318
Charming.
Does anyone on this station talk

361
00:15:26,353 --> 00:15:27,986
like they're<i> not</i> at a funeral?

362
00:15:28,021 --> 00:15:30,088
No. I'm afraid we all do.

363
00:15:30,123 --> 00:15:32,424
So, Hettie, you're doing
a concert tonight.

364
00:15:32,459 --> 00:15:33,792
Yes, she is.

365
00:15:33,827 --> 00:15:35,593
Could you tell our listeners

366
00:15:35,629 --> 00:15:37,429
why you didn't perform with your
older brothers and sisters?

367
00:15:37,464 --> 00:15:39,197
I mean, they would ask
me to. They would...

368
00:15:39,232 --> 00:15:40,665
They'd say,
"Hettie, come over here,

369
00:15:40,701 --> 00:15:42,967
pick up a banjo
and play a song with us."

370
00:15:43,003 --> 00:15:44,703
And, um, I don't know.

371
00:15:44,738 --> 00:15:47,339
I'd say, "No, I'm happy just
to sit down by the creek

372
00:15:47,374 --> 00:15:49,040
and do my heroin." You know?

373
00:15:49,076 --> 00:15:50,442
Wait. What?

374
00:15:50,477 --> 00:15:52,377
Is it also true
you've been discovered by,

375
00:15:52,412 --> 00:15:53,878
and then turned your back on,

376
00:15:53,914 --> 00:15:56,114
well-meaning supporters
time and time again?

377
00:15:56,149 --> 00:15:58,316
Uh, yes, yeah,
that is a pattern I adhere to,

378
00:15:58,352 --> 00:15:59,651
and do you have any OxyContin?

379
00:15:59,686 --> 00:16:01,886
Now, if you got some,
you don't have to say yes.

380
00:16:01,922 --> 00:16:03,321
You just got to blink.

381
00:16:03,357 --> 00:16:04,989
Maybe we should take a break.

382
00:16:05,025 --> 00:16:07,058
For the record,
I'm not blinking.

383
00:16:07,094 --> 00:16:09,828
He's holding. He
just won't share.

384
00:16:09,863 --> 00:16:12,330
No, no, Hettie, please tell me
that you're rehabilitated.

385
00:16:12,366 --> 00:16:14,499
Please tell me that
you're talking about heroin

386
00:16:14,534 --> 00:16:16,901
so that no one else will follow
in your footsteps.

387
00:16:16,937 --> 00:16:19,704
Please reassure me
because I am frightened.

388
00:16:19,740 --> 00:16:22,073
Oh, baby girl, I
wouldn't let you down.

389
00:16:22,109 --> 00:16:24,175
You and me-- we get
each other, yeah?

390
00:16:24,211 --> 00:16:25,910
But if I don't get
something sweet,

391
00:16:25,946 --> 00:16:28,313
I might shoot someone in the face.
(gasps)

392
00:16:28,348 --> 00:16:30,882
Are you saying that you shot
your parents in the face?

393
00:16:30,917 --> 00:16:32,584
I don't know.

394
00:16:32,619 --> 00:16:34,686
That's not really the
kind of thing you remember.

395
00:16:34,721 --> 00:16:36,054
You know what I mean?

396
00:16:36,089 --> 00:16:37,856
All right,
I'm gonna go now. Bye.

397
00:16:37,891 --> 00:16:40,525
Lis, if she shoots
your face off tomorrow,

398
00:16:40,560 --> 00:16:44,195
just remember
this is what it looks like.

399
00:16:46,266 --> 00:16:47,665
(high-pitched squeak)

400
00:16:47,701 --> 00:16:49,567
She is not going
to let me down!

401
00:16:49,603 --> 00:16:51,102
She is not!

402
00:16:51,138 --> 00:16:52,604
I am saving her.

403
00:16:52,639 --> 00:16:54,205
Okay, you've convinced me.

404
00:16:54,241 --> 00:16:56,107
You're out of your mind.

405
00:16:56,143 --> 00:16:58,144
So, how'd it go at the co-ed
kickball championship?

406
00:16:58,334 --> 00:16:59,412
All a lie!

407
00:16:59,413 --> 00:17:01,880
Lisa's been hiding a dangerous
heroin addict in her closet!

408
00:17:01,915 --> 00:17:04,349
(gasps) You were hiding her
in your closet, too!

409
00:17:04,384 --> 00:17:06,384
I was renting
week-to-week!

410
00:17:10,014 --> 00:17:13,782
(grumbling) Homer, do you know what
was going on in our house?

411
00:17:13,818 --> 00:17:16,118
Why are you asking meow--
I mean, me now?

412
00:17:16,153 --> 00:17:17,786
Homer, I know about the cat.

413
00:17:17,822 --> 00:17:19,888
I also know about the dog.
He's fine.

414
00:17:19,924 --> 00:17:21,623
(whistles)
(panting)

415
00:17:21,659 --> 00:17:24,193
I've been taking care of everything.
You knew?

416
00:17:24,228 --> 00:17:27,029
It was pretty sexy
pretending you were toolsy.

417
00:17:27,064 --> 00:17:30,466
But Lisa let a strange
Kentucky lady live in our house

418
00:17:30,501 --> 00:17:32,468
without our permission.

419
00:17:32,503 --> 00:17:35,337
Well, now I don't know where
she is, and I believed in her,

420
00:17:35,372 --> 00:17:37,840
and we have a concert
in 30 minutes.

421
00:17:37,875 --> 00:17:39,441
Oh, honey, we'll find her.

422
00:17:39,477 --> 00:17:41,343
You entertain the audience
till we get there.

423
00:17:41,378 --> 00:17:43,979
Okay, I'll play my sax!

424
00:17:44,014 --> 00:17:45,714
Well, that'll turn
a disappointed crowd

425
00:17:45,749 --> 00:17:46,749
into a lynch mob.

426
00:17:46,784 --> 00:17:47,850
Shut up.

427
00:17:47,885 --> 00:17:49,818
Okay, if you want
to find this woman,

428
00:17:49,854 --> 00:17:52,988
and I recommend you don't,
here's a little clue.

429
00:17:53,023 --> 00:17:56,325
Hettie usually smells
like radiator booze.

430
00:17:56,360 --> 00:17:57,693
To Cletus country!

431
00:17:57,728 --> 00:17:59,161
♪ ♪

432
00:17:59,196 --> 00:18:01,563
Hettie, we need
to get you up,

433
00:18:01,599 --> 00:18:04,800
run you through a car wash
and get you to that concert.

434
00:18:04,835 --> 00:18:06,535
You hush up now!

435
00:18:06,570 --> 00:18:09,004
Fortunately for you,
I'm fluent in drunk.

436
00:18:09,039 --> 00:18:10,572
(slurring his words):
Now, come on, get up.

437
00:18:10,608 --> 00:18:12,007
(speaking gibberish)

438
00:18:12,042 --> 00:18:13,408
You're gonna
get up and...

439
00:18:13,444 --> 00:18:15,577
(both speaking gibberish)

440
00:18:15,613 --> 00:18:17,012
And backtalk.

441
00:18:17,047 --> 00:18:20,315
(both speaking gibberish)

442
00:18:20,351 --> 00:18:22,351
Nobody tell me what to do.

443
00:18:22,386 --> 00:18:25,487
Not you, not the po-lice.

444
00:18:25,523 --> 00:18:27,389
Not even the police.

445
00:18:27,424 --> 00:18:29,925
Cletus, why did you
give her a shotgun?

446
00:18:29,960 --> 00:18:33,495
Hey, I do not<i> give</i>
anyone firearms.

447
00:18:33,531 --> 00:18:36,832
Now, she must've taken that
out the umbrella stand.

448
00:18:36,867 --> 00:18:39,501
Oh, your little girl's
been so good to me.

449
00:18:39,537 --> 00:18:41,603
I can't shoot you
in the face.

450
00:18:41,639 --> 00:18:43,372
I'll just make you deaf
for a week.

451
00:18:43,407 --> 00:18:45,340
Oh! I have church tomorrow.

452
00:18:45,376 --> 00:18:46,775
Can you do the other?

453
00:18:46,810 --> 00:18:48,210
Thank you!

454
00:18:48,245 --> 00:18:50,512
(playing lively jazz)

455
00:18:50,548 --> 00:18:54,049
Give it up!
She isn't coming!

456
00:18:54,084 --> 00:18:56,785
(song ends)

457
00:18:56,820 --> 00:19:00,722
You've just made
a very powerful enemy-- NPR.

458
00:19:00,758 --> 00:19:04,493
Our revenge is made possible
by listeners like you.

459
00:19:08,098 --> 00:19:11,233
Hello, Springfield!

460
00:19:11,268 --> 00:19:15,504
They're gone, and I am never,
ever gonna forgive you.

461
00:19:18,342 --> 00:19:24,046
♪ As I went down
in the river to pray ♪

462
00:19:24,081 --> 00:19:28,317
♪ Slipped in the mud
and lost my way ♪

463
00:19:28,352 --> 00:19:32,721
♪ Found a bag of chips
and half a Twix ♪

464
00:19:32,756 --> 00:19:37,993
♪ Thank you, Lord,
for this day ♪

465
00:19:38,028 --> 00:19:42,631
♪ Oh, honey, I've been down ♪

466
00:19:42,666 --> 00:19:47,669
♪ Bread bag shoes
and a Burger King crown ♪

467
00:19:47,705 --> 00:19:52,140
♪ Oh, honey, I've been down ♪

468
00:19:52,176 --> 00:19:56,745
♪ And down is where
I think I'm gonna stay. ♪

469
00:19:56,780 --> 00:19:59,147
(crying)

470
00:19:59,183 --> 00:20:03,151
Ugh. Okay, you can sleep
it off on our couch.

471
00:20:03,187 --> 00:20:05,320
Well, thank you, sweetheart.

472
00:20:05,356 --> 00:20:07,289
Hey, what time do you
do your couch gags?

473
00:20:07,324 --> 00:20:10,015
Around 11:00 in the
morning. You'll be fine.

474
00:20:10,016 --> 00:20:16,516
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

475
00:20:19,753 --> 00:20:22,921
(to "Big Rock Candy Mountain"):
♪ In the house next door to Flanders ♪

476
00:20:22,957 --> 00:20:26,458
♪ There's a devil boy
named Bart ♪

477
00:20:26,493 --> 00:20:29,394
♪ Fatso's lost his catso ♪

478
00:20:29,430 --> 00:20:32,564
♪ And the girl has
a great big heart ♪

479
00:20:32,600 --> 00:20:36,068
♪ The curtains
all have corncobs ♪

480
00:20:36,103 --> 00:20:39,171
♪ There's a sailboat
on the wall ♪

481
00:20:39,206 --> 00:20:42,307
♪ There's Selma and a Patty
and a grampa who is batty ♪

482
00:20:42,343 --> 00:20:45,978
♪ Apu and his Squishy,
the three-eyed fishie ♪

483
00:20:46,013 --> 00:20:48,580
♪ And they ain't aware
that I took the silverware ♪

484
00:20:48,616 --> 00:20:51,950
♪ From the house
next door to Flanders... ♪

485
00:20:54,388 --> 00:20:57,756
Okay, everyone
into the tunnel.

486
00:20:57,791 --> 00:21:01,093
♪ In the house
next door to Flanders ♪

487
00:21:01,128 --> 00:21:03,762
♪ Mom's hairdo scrapes the sky ♪

488
00:21:03,797 --> 00:21:08,000
♪ The baby don't say nothin' ♪

489
00:21:08,035 --> 00:21:12,271
♪ And no one wonders why... ♪

490
00:21:12,306 --> 00:21:16,041
Oh, this place is dirtier
than the tunnel.

491
00:21:16,660 --> 00:21:18,660
Shh!

