<?php
	include 'utils.php';
	include 'config.php';
	/*var_dump($_POST);
	die();*/

	$eid = $_POST['episodeid'];

	$qryFindEpisodes = $conn->prepare("SELECT * FROM EPISODES WHERE ID = $eid");
	$qryFindEpisodes->execute();
	$episode = $qryFindEpisodes->fetchAll()[0];

	$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n" .
			"Cookie: foo=bar\r\n" .
			"User-agent: BROWSER-DESCRIPTION-HERE\r\n".
			"X-Requested-With:ShockwaveFlash/21.0.0.216\r\n".
			"Referer: " . $episode["VIDEOSOURCE1"] . "\r\n"
		)
	);
	$context = stream_context_create($opts);
	$file2 = file_get_contents($episode["VIDEOSOURCE1"], true);
	
	preg_match_all('/"file" : ".+"/', $file2, $files);
	//preg_match_all('/"image" : ".+"/', $file2, $image, PREG_OFFSET_CAPTURE, 0);

	/*$movie = $files[0][0][0];
	$pos2 = strpos($movie, '"file" :"');
	$posend2 = strpos($movie, '"', $pos2+10);
	$movie = substr($movie, $pos2+10, $posend2-$pos2-10);*/



	/*$img = $image[0][0][0];
	$pos2 = strpos($img, '"image" :"');
	$posend2 = strpos($img, '"', $pos2+11);
	$img = substr($img, $pos2+11, $posend2-$pos2-11);*/

	//echo $movie."</br>";
	//echo $img;

?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8" />
		<title>ThatFlix! - <?php echo $_POST['title'] ." - " . $episode['EPISODE']; ?></title>
		<script src="jquery.js"></script>
		<link href="http://vjs.zencdn.net/5.9.2/video-js.css" rel="stylesheet">
		<script src="http://vjs.zencdn.net/5.9.2/video.js"> </script>
		<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>	
	<body>
		<video id="video-player" class="video-js" data-setup='{ "controls": true, "autoplay": true, "preload": "auto" }' width="640" height="264"
			poster="getdata.php?sender=<?php echo $value?>&remote=<?php echo $episode['VIDEOCOVER']?>" data-setup="{}">
			
			<?php 
				//var_dump($files[0][0]);
				for ($i=0; $i < sizeof($files[0]); $i++) { 
					//var_dump($files[0][$i]);
					$movie = $files[0][$i];
					$pos2 = strpos($movie, '"file" :"');
					$posend2 = strpos($movie, '"', $pos2+10);
					$movie = substr($movie, $pos2+10, $posend2-$pos2-10);
			?>
					<source src="getdata.php?sender=<?php echo $value?>&remote=<?php echo $movie?>" type='video/mp4'>
			<?php		
				}?>

			
			<track kind="subtitles" src="/subtitles/<?php echo $episode['LINK']?>.vtt" srclang="en" label="English" default>
			
		</video>
		<form id="playform" name="playform" method="POST" action="play.php" style="display: none;">
			<input type="text" id="serieid" name="serieid" value="<?php echo $_POST['serieid']; ?>"></input>
			<input type="text" id="episodeid" name="episodeid" value=""></input>
			<input type="text" id="title" name="title" value=""></input>
			<input type="text" id="data" name="data" value=""></input>
		<!--	<input type="text" id="season" name="season" value=""></input>
			<input type="text" id="episode" name="episode" value=""></input>
			<input type="text" id="episodecover" name="episodecover" value=""></input>
			<input type="text" id="name" name="name" value=""></input>
			<input type="text" id="link" name="link" value=""></input>-->
		</form>

		<script type="text/javascript">
			videojs("video-player").ready(function(){
				var player = videojs("video-player");
				var previousTime = 0;
				var currentTime = 0;
				var movingToNext = false;
				player.on('timeupdate', function(evt, a, b, c) {
					$('.floating-title1 .nowtime').html(new Date().toLocaleString());
					var nextep = $('.floating-next-episode');
					if ( player.duration() > 0 && player.currentTime() >= player.duration()-30 && opts.selected > 0 ){
						var nextepimg = $('.floating-next-episode .image');
						var nexteptxt = $('.floating-next-episode .title');
						var nextepleft = $('.floating-next-episode .timeleft');
						var next = opts.data[opts.selected-1];
						nextepimg.attr("style", "background-image: url(" + next.episodecover + ");");
						nexteptxt.html("Se" + next.season + "-Ep" + next.episode + " - " + next.name);
						var whenGoToNext = player.duration()-20;
						var timeLeft = parseInt(whenGoToNext - player.currentTime());
						if (timeLeft >= 0){
							nextepleft.html("Next episode in " + timeLeft + "s..");
						}
						nextep.attr("style", "display: block;");
						if (timeLeft <= 0 && !movingToNext && opts.selected > 0){
							movingToNext = true;
							setTimeout(function(){
								$("#playform #title").attr("value", opts.title );
								$("#playform #episodeid").attr("value", next.episodeid );
								//$("#playform #serie").attr("value", $(this).attr("serie") );
								$("#playform #data").attr("value", JSON.stringify(opts.data) );
								$("#playform").submit();
							},100);
						}
					}

					//previousTime = currentTime;
					//currentTime = player.currentTime();
					//$('.floating-title1 .title').html(player.currentTime());
				});
			/*	player.on('seeking', function(evt, a, b, c) {
					//console.log(evt);
					evt
					//console.log('seeking from', previousTime, 'to', currentTime, '; delta:', currentTime - previousTime);
				});

				player.on('progress', function(evt, a, b, c){
					//console.log(evt);
				});
				player.addEvent('loadeddata', player.currentTime(seekTime));
				player.addEvent('loadedalldata', player.currentTime(seekTime));
				player.addEvent('progress', player.currentTime(seekTime));*/
				player.play();
				var cbar = $('.vjs-control-bar');
				cbar.append("<div class=\"floating-logo1\"></div>");
				cbar.append("<div class=\"floating-backbtn2\"></div>");
				cbar.append("<div class=\"floating-title1\"> <div class=\"title\"><?php echo $_POST['title']; ?></div> <div class=\"episode\"><?php echo "Se".$episode['SEASON'] . "-Ep" . $episode['EPISODE'] . " - " . $episode['NAME']; ?></div> <div class=\"nowtime\">00:00:00</div></div>");
				cbar.parent().append("<div class=\"floating-next-episode\" style=\"display:none;\"><div class=\"timeleft\"></div><div class=\"image\"></div><div class=\"title\"></div> </div>");
				$(".floating-backbtn2").click(function (){
					window.location = "serie.php?sid=" + opts.serieid;
				});
				//setTimeout(function(){set_video_time(player,120);},100);
			});
			function set_video_time(cvideo,destination_time){
				cvideo.currentTime(destination_time);
			}
			/*$( document ).ready(function() {
				console.log( "ready!" );
				videojs("my-player").ready(function(){
					myPlayer.play();
				});
			});
			*/
		</script>
		<script type="text/javascript">
			var opts = {
				episodeid : "<?php echo $_POST['episodeid']; ?>",
				title : "<?php echo $_POST['title']; ?>",
				serieid : "<?php echo $_POST['serieid']; ?>",
				selected : 0,
				data : <?php echo $_POST['data']; ?>
			};
			for (var i = opts.data.length - 1; i >= 0; i--) {
				if (opts.data[i].episodeid == opts.episodeid){
					opts.selected = i;
					break;
				} 
			}
		</script>
	</body>
</html>