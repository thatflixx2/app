var allImgs = null;
function loadAllImages(selector){
	allImgs = $(selector);
}

function elementInViewport(el) {
	var rect = el.getBoundingClientRect();

	return (rect.top >= 0
		&& rect.left >= 0
		&& rect.top  <= (window.innerHeight || document.documentElement.clientHeight));
}

function proccessScroll(evt){
	for (var i = 0; i < allImgs.length; i++) {
		//if (!elementInViewport(allImgs[i])) return;
		if (elementInViewport(allImgs[i]) && $(allImgs[i]).css("display") != "none" && $(allImgs[i]).attr("img-data")!=""){
			$(allImgs[i]).attr("style","background-image: url(" + $(allImgs[i]).attr("img-data") + ");");
			$(allImgs[i]).attr("img-data", "");
			$(allImgs[i]).addClass("fadein");
		}
	}
};