<?php
	include 'utils.php'; 
	include 'config.php';
	//include 'V8JS.php';

	function file_get_contents2($url, $opt2){
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

	    $data = curl_exec($ch);
	    curl_close($ch);
		return $data;
	} 

	$site = $argv[1];
	//$episodeID = $argv[2];
	$episodesID = explode(",", $argv[2]);	
	$allNames = file_get_contents2( $site, true );
	$pattern = '/class="summary".+?>"(.+)"<\/td>/';
	$patternHref = '/<a.+?>(.+)<\/a>/';
			preg_match_all($pattern, $allNames, $names, PREG_OFFSET_CAPTURE, 0);
			$finalNames = array();
			foreach ($names[1] as $key => $value) {
				if ( strpos($value[0], "a href") !== false)
				{
					preg_match_all($patternHref, $value[0], $insideHREF);
					//var_dump($insideHREF);
					//die();
					$finalNames[] = $insideHREF[1][0];
				} else 
					$finalNames[] = $value[0];
			}

/*	var_dump($finalNames);
	die();
*/
	foreach ($episodesID as $key => $episodeID) {
		$qryEpisode = $conn->prepare("SELECT E.*, S.NAME SERIENAME FROM EPISODES E
									  INNER JOIN SERIES S ON (E.SERIE_ID = S.ID)
									  WHERE E.ID = :ID");
		$qryEpisode->execute(array("ID"=>$episodeID));		
		$episode = $qryEpisode->fetchAll();
		if ( sizeof($episode) > 0 ){
			echo "\n";
			//echo "------------------------------------------------------------------------------------\n";
			echo $episode[0]["SERIENAME"] . " - Season: " . $episode[0]["SEASON"] . " - Episode: " . $episode[0]["EPISODE"] . "\n";
			//$newName = $finalNames[ $episode[0]["EPISODE"]-1 ];
			$wikiName = html_entity_decode( htmlspecialchars_decode( $finalNames[ $episode[0]["EPISODE"]-1 ]) , ENT_COMPAT, 'utf-8');
			echo "Name: " . $wikiName . "\n";
			echo "UPDATE DB FOR '" . $episode[0]["LINK"] . 
				 "' ON EPISODE '" . $episode[0]["ID"] . 
				 " TO: " . $wikiName . "'? (Y|n)?: ";
			$usrInput = (isset($argv[3]) && $argv[3] == "-skipdb") ? "n" : "y";//readline();
			if ($usrInput == "Y" || $usrInput == "y" || trim($usrInput) == ""){
				if ($wikiName != ""){					
					$qryEpisode = $conn->prepare("UPDATE EPISODES SET NAME = :NAME WHERE ID = :ID");
					$qryEpisode->execute(array("ID"=>$episodeID, "NAME"=> $wikiName ));		
					echo "UPDATED.\n";
				} else {
					echo "SKIPPED.\n";
				}
			}

			$filename = "./newimages/" . $episode[0]["VIDEOCOVER"];
			if (!file_exists($filename)){
				echo "Getting Video Image...\n";
				$videos = "http://127.0.0.1:999/seriesdata.php?act=episode&eid=" . $episodeID;
				$video = json_decode(file_get_contents($videos));

				if ( sizeof($video) > 0 ){
					$hiResLink = "";
					$lastRes = 0;
					foreach ($video as $k => $videoL) 
						if ($videoL[1] == null || $videoL[1] > $lastRes){
							$lastRes = $videoL[1];
							$hiResLink = urldecode($videoL[0]); 
						}

					//echo $hiResLink;
					//die();
					//echo "[$hiResLink][$lastRes]";
					//var_dump($video);
					/*$cmd = "ffmpeg -i \"$hiResLink\" -deinterlace -an -ss 1 -t 00:00:22 -r 1 -y -vcodec mjpeg -f mjpeg \"$filename\" 2>&1";
					$do = `$cmd`;
					//$cmdLine = "START /usr/bin/ffmpeg";// -vcodec jpg -i \"" . $hiResLink . "\" -ss  00:00:15 -vframes frames " . $filename;
					echo $cmd . "\n";
					exec($do); */
					$secs = rand(12, 59);
					shell_exec("ffmpeg -i \"$hiResLink\" -deinterlace -ss 00:00:$secs -r 1 -y \"$filename\" 2>&1");
					echo "Image $filename saved.\n";					
				}
			} else {
				echo "SKIPPED.\n";
			}
			echo "\n";

		}
	}
	echo "------------------------------------------------------------------------------------";
	echo "\n";
