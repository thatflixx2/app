<?php
	if(ini_get('zlib.output_compression')) {
		ini_set('zlib.output_compression', 'Off'); 
	}

	function flush_buffers(){
		ob_end_flush();
		ob_flush();
		flush();
		ob_start();
	}

	$remoteLink = $_GET['remote'];
	preg_match('/http(?:s?):\/\/(.+?)\//', $remoteLink, $matchesRefer);
	$referLink = $_GET['sender']; 
	$headersRemote = array_change_key_case(get_headers($remoteLink, TRUE));

	$headersLocal = getallheaders();
	$range = $headersLocal['Range']; 


	$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=>"Accept:*/*\r\n" .
					  "Accept-Encoding:gzip, deflate, sdch\r\n".
					  "Accept-Language:pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4\r\n".
					  //"Connection:keep-alive\r\n".
					  "Host: " . $hostLink . "\r\n" .
					  "Referer: " . $remoteLink . "\r\n" .
					  "Range: " . $range . "\r\n" .
					  "User-Agent: " . $headersLocal["User-Agent"] . "\r\n" .
					  "X-Requested-With:ShockwaveFlash/22.0.0.216\r\n"
		)
	);
	/*echo "<pre>";
	var_dump($headersLocal);
	echo "</pre>";
	echo "<br/><br/><br/>";
	echo "<pre>";
	var_dump($headersRemote);
	echo "</pre>";
	echo "<br/><br/><br/>";
	echo "<pre>";
	var_dump($_GET);
	echo "</pre>";
	die();*/

	$context = stream_context_create($opts);
	$contentLength = $headersRemote['content-length'];
	$begin = 0;
	$end = $contentLength;
	$multiplier = 8;

	if ( isset($headersLocal['Range']) ){
		$range = $headersLocal['Range']; 
		if(preg_match('/[0-9]+/', $range, $matches)){ 
			$begin=intval($matches[0]);
			if(!empty($matches[1]))
				$end=intval($matches[1]);
		}
	} else {
		// header('HTTP/1.1 200 OK');
	}
	$totalminus1 = $contentLength-1; 

	ob_clean();
	//header('Date: ' . $headersRemote['date']);
	//header("Content-Type: application/octet-stream", false);
	//header("Content-Disposition: attachment; filename=v.mp4");
	//header('Last-Modified: ' . $headersRemote['last-modified']);
	//header('Connection: close');
	//header('Access-Control-Allow-Origin:*');
	//header('Access-Control-Allow-Methods:HEAD, GET, OPTIONS');
	//header('Access-Control-Expose-Headers:Content-Range, Date, Etag');
	//header('Access-Control-Allow-Headers:Content-Type, Origin, Accept, Range, Cache-Control');
	//header('Access-Control-Max-Age:600');
	//header("Timing-Allow-Origin: *");
	//header('Access-Control-Allow-Credentials:true');
	$buffer = 1024 * $multiplier;//1024 * 30; //102400
	header("Content-Type: video/mp4", false);
	if(isset($_SERVER['HTTP_RANGE'])){
		header('HTTP/1.1 206 Partial Content');
		header("Accept-Ranges: bytes");//0-$totalminus1");
		header('Content-Length: '.intval($totalminus1-$begin));
		header("Content-Range: bytes $begin-$totalminus1/$contentLength"); 
		$fp = fopen($remoteLink, 'rb'); //, false, $context
		fseek($fp, $begin);
		while(true){
			// Check if we have outputted all the data requested
			if(ftell($fp) >= $contentLength){
				break;
			}
			// Output the data
			echo fread($fp, $buffer);
			// Flush the buffer immediately
			@ob_flush();
			flush();
		}
	} else {
		header('Content-Length: '.intval($contentLength));
        @readfile($path);
        @ob_flush();
        flush();
	}
	//header('ETag: ' . $headersRemote['etag']);
	

    //header("Cache-Control: max-age=2592000, public");
    //header('Content-Disposition: attachment ');
	//header('Pragma: no-cache');
	//header('Keep-Alive: timeout=33, max=1000');

	///echo "HERE"; die();
	/*set_time_limit(0);
	while(!feof($fp) && ($p = ftell($fp)) <= $end) {
	    if ($p + $buffer > $end) {
	        $buffer = $end - $p + 1;
	    }
	    echo fread($fp, $buffer);
	   // ob_flush();
	    flush_buffers();
	}
	fclose($fp);
	exit();*/


die();
