$app = {
	_USE_VISUAL_DEBUG: false,

	_jsonSeries 	: undefined,
	_jsonEpisode 	: undefined,
	_jsonEpisodes 	: undefined,
	_fadeSpeed		: 240,
	_whenGoToNext   : 10,
	_goToNext 		: 0,
	_title 			: "ThatFlix!",
	_cookieID		: "",
	_lastPlaying	: "",
	_realPlayer		: null,
	_messageBrokenLinkReported : "All Right!<br/>Thanks for reporting a broken link.<br/>Please return soon and hopefully this media will be available again!",
	_messageMediaUnavailable : 'We are sorry, but the media that you are trying to access seems currently unavailable<br/>You may wish to: "Try Reload" or "Report Broken Link".',

	_body 			: function (){ return $(document.body); },
	_series  		: function (){ return $("#all-series-container"); },
	_seasons 		: function (){ return $("#serie-seasons-container"); },
	_player  		: function (){ return $("#serie-player"); },
	_loading 		: function (){ return $("#loading"); },
	_topLoading 	: function (){ return $("#top-loading"); },
	_seriesCovers	: function (){ return $("#all-series-covers"); },
	_initialScreen	: function (){ return $("#initial-screen"); },
	_screenHeight	: function (){ return Math.max(document.documentElement.clientHeight, window.innerHeight || 0); },
	_screenWidth	: function (){ return Math.max(document.documentElement.clientWidth, window.innerWidth || 0); },
	
	GUID: function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	},

	setWndTitle: function(newTitle){
		try {
			var theNewTitle = $app._title + (newTitle == "" ? "" : " - ") + newTitle; 
			if (top.document != document)
				top.document.title = theNewTitle;
			else 
				document.title = theNewTitle;
		} catch(Exception){
			console.log(Exception.message);
		}
	},

	setWndImg: function(newSrc){
		try {
			var hhead = (top.document != document ? top.document.head : document.head);
			var favIcon = $(hhead).children("link[rel=icon]");
			var newSrcType = newSrc.substr(newSrc.indexOf(".")+1);
			if (favIcon.length > 0){
				favIcon.attr("href", newSrc);
				favIcon.attr("type", "image/"+newSrcType);
			} else {
				/*var link = document.createElement('link'),
				link.id = 'dynamic-favicon';
				link.rel = 'icon';
				link.href = src;
				hhead.appendChild(link);*/
				$(hhead).append('<link rel="icon" type="image/' + newSrcType + '" href="' + newSrc + '">');
			}
		} catch(Exception){
			console.log(Exception.message);
		}
	},

	fadeIn : function(where){
		$(where).removeClass("ffadeout");
		$(where).addClass("ffadein");
		$(where).css("z-index", "9999");
	},
	fadeOut : function(where){
		$(where).removeClass("ffadein");
		$(where).addClass("ffadeout");
		$(where).css("z-index", "0");
	},

	usrFeedBack : function(eid){
		var req = $.ajax({
			url: "/seriesdata.php?act=usrfeedback&usr=" + $app._cookieID + "&eid=" + eid,
			method: "GET",
			dataType: "html",
		});
		req.done(function (result){
			var data = JSON.parse(result);
			$app._lastPlaying =	data[0];		
		});
	},
	usrFeedBackUpd : function(time){
		var req = $.ajax({
			url: "/seriesdata.php?act=usrfeedbackupd&time=" + time + "&rid=" + $app._lastPlaying,
			method: "GET",
			dataType: "html",
		});
		req.done(function (result){});
	},

	showScreens : function(loading, player, seasons, series, iniScreen) {
		this._loading().css(		"display", 	loading ? "block" : "none");
		this._player().css(			"display", 	player 	? "block" : "none");
		this._seasons().css(		"display", 	seasons ? "block" : "none");
		this._series().css(			"display", 	series  ? "block" : "none");
		this._initialScreen().css(	"display", 	iniScreen  ? "block" : "none");
	},

	popPlayer : function (){
		if ($app._player().children().length > 0){
			if ($app._realPlayer != undefined && $app._realPlayer != null){
				$app._realPlayer.dispose();
			}
			$app._player().children().remove();
		}
	},

	mountVideoUrl: function(track, vid){
		return "video.mp4?vid="+vid+"&size=" + track[1];		
		//return "video.php?sender="+track[2]+"&remote=" + track[0];
	},
	
	mountSubtitleUrl: function(subtitle){
		return "getSubtitle.php?sub="+subtitle[0];
	},

	inflatePlayer : function(track, cover, subtitles, vid){
		/*var videoSource = "getdata.php?sender=" +ep.VIDEOSOURCE1+ "&remote=" + track[0][0];/data.length-2*/                                      /**/
		$htmlPlayer =	'	<video id="video-player" controls preload="auto" class="video-js vjs-default-skin vjs-big-play-centered" data-setup=\'{ "controls": true, "autoplay": true, "preload": "auto", "techOrder": ["html5","flash"] }\' poster="' + cover + '">';
		if (track.length == 0)
			track.push( ["failed", "0"] );
		for (var i = track.length - 1; i >= 0; i--) {
			var vSource = $app.mountVideoUrl(track[i], vid);
			var extension = "mp4";	//track[i][0].split('.').pop().contains;
			$htmlPlayer += '	<source src="' + vSource + '" type="video/' + extension + '; codecs=avc1.42E01E,mp4a.40.2" label="' + track[i][1] + '" />';
		}
		for (var i = subtitles.length - 1; i >= 0; i--) {
			var subtitle = $app.mountSubtitleUrl(subtitles[i]);//"getSubtitle.php?sub=" + subtitles[i][0];
			var subDefault = subtitles[i][1] == "EN" ? "default" : ""; 
			$htmlPlayer += '	<track kind="subtitles" src="' + subtitle + '" srclang="' + subtitles[i][1] + '" label="' + subtitles[i][1] + '" ' + subDefault + '/>';
		}
		$htmlPlayer +=	/*'		<track kind="subtitles" src="' + subtitle + '" srclang="en" label="English" default>' + */
						'	</video>';
		$app.popPlayer();
		$app._player().append($htmlPlayer);
	},

	loadEpisode : function(eid, fullscreen){
		var ep = undefined;
		for (var i = 0; i < $app._jsonEpisodes.EPISODES.length; i++) {
			ep = $app._jsonEpisodes.EPISODES[i];
			$app._jsonEpisodes.selected = i;
			if (ep.ID == eid) break;
		}
		$("#stream-loading-error").attr("link", ep.LINK);
		$("#stream-loading-error").attr("mediacode", ep.ID);

		var videoObj = $("video");
		var sourceObj = $("source");
		var trackObj = $("track");

		var req = $.ajax({
			url: "/seriesdata.php?act=episode&eid=" + eid,
			method: "GET",
			dataType: "html",
		});
		/*REQUEST MEDIA DATA*/
		req.done(function (result){
			var data = JSON.parse(result);
			/*REQUEST SUBTITLES DATA*/
			var req = $.ajax({
				url: "/seriesdata.php?act=subtitles&mid=" + eid,
				method: "GET",
				dataType: "html",
			});
			req.done(function (result2){
				var subtitlesData = JSON.parse(result2);
				var browTitle = ""
				if ($app._jsonEpisodes.STYPE == 0){
					browTitle = $app._jsonEpisodes.NAME + " - " +  "Se" + ep.SEASON + "-Ep" + ep.EPISODE + " - " + ep.NAME;
				} else {
					browTitle = $app._jsonEpisodes.NAME;// + " - " +  "Se" + next.SEASON + "-Ep" + next.EPISODE + " - " + ep.NAME;
				}
				$app.setWndTitle(browTitle);
				/*var videoSource = "getdata.php?sender=" +ep.VIDEOSOURCE1+ "&remote=" + data[0][0];/data.length-2 */
				var epCover = $appCfg.imgsLocation+"/episodeimgs/" + ep.VIDEOCOVER;
				/*var epSubtitles = "/subtitles/" + ep.LINK + ".vtt";*/
				/*sourceObj.attr("src", videoSource);*/
				/*videoObj.attr("poster", $appCfg.imgsLocation+"/episodeimgs/" + ep.VIDEOCOVER);*/
				/*trackObj.attr("src", "/subtitles/" + ep.LINK + ".vtt");*/

				$app.inflatePlayer(data, epCover, subtitlesData, ep.ID);
				$app._player().append('<div id="player-back" class="floating-backbtn2"></div>');
				/*$(document.body).append('<script type="text/javascript" src="/videojs/videojs-resolution-switcher.js"> </script>');*/
				$app.showScreens(false, true, false, false, false);
				videojs("video-player", { plugins : { myplugin : {} }, daSources: data, subsSources: subtitlesData }, function(){
					var player = videojs("video-player");
					$app._realPlayer = player;
					var previousTime = 0;
					var currentTime = 0;
					var movingToNext = false;
					player.on('timeupdate', function(evt, a, b, c) {
						$('.floating-title1 .nowtime').html(new Date().toLocaleString());
						var nextep = $('.floating-next-episode');
						if (player.duration() > 0 && player.currentTime() > 0 && $app._player().children(".floating-backbtn2").length > 0){
							$app.usrFeedBack(eid);
							var backBtn = $app._player().children(".floating-backbtn2").detach();
							$('.vjs-control-bar').append(backBtn);
						}
						if ( player.currentTime() > 0 && player.duration() > 0 && player.currentTime() >= player.duration()-$app._whenGoToNext && $app._jsonEpisodes.selected > 0 ){
							var nextepimg = $('.floating-next-episode .image');
							var nexteptxt = $('.floating-next-episode .title');
							var nextepleft = $('.floating-next-episode .timeleft');
							var thisEp = $app._jsonEpisodes.EPISODES[$app._jsonEpisodes.selected];
							var next = $app._jsonEpisodes.EPISODES[$app._jsonEpisodes.selected-1];
							nextepimg.attr("style", "background-image: url("+$appCfg.imgsLocation+"/episodeimgs/" + next.VIDEOCOVER + ");");
							nexteptxt.html("Se" + next.SEASON + "-Ep" + next.EPISODE + "<br/>" + next.NAME);
							var whenGoToNext = player.duration()-$app._goToNext;
							var timeLeft = parseInt(whenGoToNext - player.currentTime());
							if (timeLeft >= 0){
								nextepleft.html("Next episode in " + timeLeft + "s..");
							}
							nextep.attr("style", "display: block;");
							if (timeLeft <= 0 && !movingToNext && $app._jsonEpisodes.selected > 0){
								movingToNext = true;
								setTimeout(function(){
									$app.usrFeedBackUpd(player.duration());
									thisEp.WATCHED = player.duration();
									$("#ep"+thisEp.ID+" .serie-row").append('<div class="watched"></div>');
									/*videoObj.attr("poster", $appCfg.imgsLocation+"/episodeimgs/" + next.VIDEOCOVER);
									trackObj.attr("src", "/subtitles/" + next.LINK + ".vtt");
									player.src({"type":"video/mp4", "src":videoSource});
									player.play();*/
									$(".vjs-control-bar .floating-logo1").remove();
									$("#serie-player .floating-backbtn2").remove();
									$(".vjs-control-bar .floating-title1").remove();
									$(".floating-next-episode").remove();
									player.pause();
									$app.loadEpisode( next.ID, player.isFullscreen() );
								},100);
							}
						}
					});
					player.hasOccurredError = false;
					player.on('error', function(evt, a, b, c) {
						/*alert("fodeo");*/
						$app.visualDebug("File Error." + evt.toStr);
						/*player.hasOccurredError = true;*/
						$("#player-back").css("display", "block");
						$("#try-reload").css("display", "block");
						$("#report-broken-link").css("display", "block");
						$("#get-back-to-list").css("display", "none");
						$("#submitting-loading").css("display", "none");
						$("#stream-loading-error .text").html($app._messageMediaUnavailable);
						$("#stream-loading-error").css("display", "block");

					});
					player.on('loadeddata', function(evt, a, b, c) {
						$app.visualDebug("Playing2.");
						/*videojs("video-player").play();*/
						/*document.getElementById("video-player").play();*/
					});
					player.on('canplaythrough', function() {
						$app.visualDebug("CanPlayThrough.");
						/*document.getElementById("video-player").play();*/
					});
					player.on('useractive', function() {
						$("#player-logo").css("display", "block");
						$("#player-title").css("display", "block");
						$("#player-back").css("display", "block");
					});
					player.on('userinactive', function() {
						$("#player-logo").css("display", "none");
						$("#player-title").css("display", "none");
						/*if (!player.hasOccurredError) {*/
							$("#player-back").css("display", "none");
						/*}*/
					});


					$app.visualDebug("Loading...Video.MP4");
					var cbar = $('.vjs-control-bar');	
					cbar.append('<div id="player-logo" class="floating-logo2"></div>');
					/*cbar.append('<div class="floating-backbtn2"></div>');*/
					cbar.append('<div id="player-title" class="floating-title1"> <div class="title">'+$app._jsonEpisodes.NAME+'</div> <div class="episode">Se'+ep.SEASON+'-Ep'+ep.EPISODE+' - '+ep.NAME+'</div> <div class="nowtime">00:00:00</div></div>');
					cbar.parent().append('<div class="floating-next-episode" style="display:none;"><div class="timeleft"></div><div class="image"></div><div class="title"></div> </div>');
					$(".floating-backbtn2").click(function (){
						$app.showScreens(false, false, true, false, false);
						$("#stream-loading-error").css("display", "none");
						setTimeout(function(){
							$(".vjs-control-bar .floating-logo1").remove();
							$(".vjs-control-bar .floating-backbtn2").remove();
							$(".vjs-control-bar .floating-title1").remove();
							$(".floating-next-episode").remove();
							player.pause();
							$app.setWndTitle($app._jsonEpisodes.NAME);
							loadAllImages(".serie-row .img-episode");
							proccessScroll();
							$app.popPlayer();
							var thisEp = $app._jsonEpisodes.EPISODES[$app._jsonEpisodes.selected];
							$app.scrollToEpisode(thisEp.ID);
						}, 100);
					});
					/*player.load();*/
					//try {
						$app.visualDebug("Loading media333...");
						player.currentSrc( $app.mountVideoUrl(data[0], ep.ID) );//{"type":"video/mp4", "src": $app.mountVideoUrl(data[0]) });
						$app.visualDebug("Force Load");
						//player.load();
						$app.visualDebug("Force Loaded");
						player.play();
					//} catch (Exception){ }
					/*$(".vjs-big-play-button").click();
					$(".vjs-big-play-button").trigger('touchstart');
					$(".vjs-big-play-button").trigger('touchend');
					if (fullscreen){
						player.requestFullscreen();
					}*/
					$app.visualDebug("Loading Finished");
				});			

				/*for (var i = opts.data.length - 1; i >= 0; i--) {
					if (opts.data[i].episodeid == opts.episodeid){
						opts.selected = i;
						break;
					} 
				}*/
			});
		});
	},

	loadEpisodes : function(id){
		var req = $.ajax({
			url: "/seriesdata.php?act=episodes&sid=" + id + "&uid=" + $app._cookieID,
			method: "GET",
			dataType: "html",
		});
		req.done(function (result){
			$app._jsonEpisodes = JSON.parse(result);
			$app.setWndTitle($app._jsonEpisodes.NAME);
			var seasonsCnt = $("#serie-seasons-container .serie-container");
			seasonsCnt.html("");
			$("#serie-seasons-container-bg").css("background-image", "url('"+$appCfg.imgsLocation+"/cacheimgs/"+$app._jsonEpisodes.IMGCOVER+"')");
			var lastSeason = "";
			var lastWatched = "";
			var lastWatchedSeason = "";
			$app.setWndImg($appCfg.imgsLocation+"/cacheimgs/"+$app._jsonEpisodes.IMGCOVER);
			if ($app._jsonEpisodes.STYPE == "1"){
				$("#serieseasons-title").html("");
				$("#serieseasons-description").html("");
				$("#serieseasons-image").attr("src", "/imgs/blank.png");
				$("#serieseasons-image").css("background-image", "url('"+$appCfg.imgsLocation+"/cacheimgs/"+$app._jsonEpisodes.IMGCOVER+"')");
				$("#img-cover").addClass("movies");
				seasonsCnt.addClass("movies");
				seasonsCnt.append('<div class="serie-row skip title">' + $app._jsonEpisodes.NAME + '</div>');	
				seasonsCnt.append('<div class="serie-row skip description genre">' + $app._jsonEpisodes.GENRE);	 
				seasonsCnt.append('<div class="serie-row skip description genre">' + $app._jsonEpisodes.TOTALTIME + ' - ' + $app._jsonEpisodes.DATERELEASED + '</div>');	
				seasonsCnt.append('<div class="serie-row skip description">Director:  ' + $app._jsonEpisodes.DIRECTOR + '</div>');
				seasonsCnt.append('<div class="serie-row skip description">Cast:      ' + $app._jsonEpisodes.CAST + '</div>');
				seasonsCnt.append('<div class="serie-row skip description btop">' + $app._jsonEpisodes.DESCRIPTION + '</div>');
				/*seasonsCnt.append('<div class="serie-row description">: ' + $app._jsonEpisodes. + '</div>');	*/
				var ep = $app._jsonEpisodes.EPISODES[0];
				seasonsCnt.append(	'<a class="link-episode" style="margin-bottom: 18px;" href="#" eid="' +ep.ID+ '" id="ep' +ep.ID+ '">' +
									'	<div class="serie-row">' +
									'		<div class="img-episode" img-data="/imgs/play.png" style="background-image: url(/imgs/blank.png);"></div>' +
									'		<div class="episode-nfos-holder">' +
									'	 		<div class="episode-title"><span>Play</span></div>' +
									'		</div>' +
									'	</div>' +
									'</a>');
			} else {
				$("#serieseasons-image").attr("src", $appCfg.imgsLocation+"/cacheimgs/"+$app._jsonEpisodes.IMGCOVER);
				$("#serieseasons-image").css("background-image", "");
				$("#serieseasons-title").html($app._jsonEpisodes.NAME);
				$("#serieseasons-description").html($app._jsonEpisodes.DESCRIPTION);
				$("#img-cover").removeClass("movies");
				seasonsCnt.removeClass("movies");
				var seasonObj = null;
				var seasonObjContainer = null;
				for (var i = 0; i < $app._jsonEpisodes.EPISODES.length; i++) {
					var ep = $app._jsonEpisodes.EPISODES[i];

					if (lastSeason != ep.SEASON){
						lastSeason = ep.SEASON;
						var seasonName = 'Season ' +lastSeason;
						seasonsCnt.append('<div id="season' + lastSeason + '" class="season-container hidden">' +
										  '		<div class="serie-row title">' + seasonName + '<span class="btn-collapse"></span></div>' +
										  '		<div class="content"></div>' +	
										  '</div>');
						seasonObj = $('#season' + lastSeason);
						seasonObjContainer = $('#season' + lastSeason + ' .content');
					}

					seasonObjContainer.append(	'<a class="link-episode" href="#" eid="' +ep.ID+ '" id="ep' +ep.ID+ '">' +
												'	<div class="serie-row">' +
												'		<div class="img-episode" img-data="'+$appCfg.imgsLocation+'/episodeimgs/' +ep.VIDEOCOVER+ '" style="background-image: url(/imgs/blank.png);"></div>' +
												(ep.WATCHED == 0 ? "" :
												'		<div class="watched"></div>' )+
												'		<div class="episode-nfos-holder">' +
												'	 		<div class="episode-title"><span>Se:'+ep.SEASON+'-Ep:'+ep.EPISODE+'</span>' +
												'				<span class="italic">'+ep.NAME+'</span></div>' +
												/*'	 		<div class="episode-title">Ep:'+ep.EPISODE+'</div>' +*/
												/*'			<div class="episode-desc">'+ep.NAME+'</div>' +*/
												'		</div>' +
												'	</div>' +
												'</a>');
					if (ep.WATCHED != 0 && lastWatched == ""){
						lastWatched = ep.ID;
						lastWatchedSeason = ep.SEASON;
					} 
					//seasonObj.addClass("hidden");
					if (i == $app._jsonEpisodes.EPISODES.length-1 && lastWatched == ""){
						lastWatched = ep.ID;
						lastWatchedSeason = ep.SEASON;
					}
				}	
				$('#season' + lastWatchedSeason).removeClass("hidden");
				$(".season-container .serie-row.title").click(function(sender){ /*.btn-collapse*/
					var parent = $(sender.target.parentElement/*.parentElement*/);
					if (parent.is(".serie-row.title")) parent = parent.parent();
					if (parent.hasClass("hidden")){
						parent.removeClass("hidden");
						proccessScroll();				
					} else {
						parent.addClass("hidden");	
					}
				});
			}
			$(".link-episode").click(function (evt){  
				$app.showScreens(true, false, false, false, false);
				setTimeout(function(){
					$app.loadEpisode( $(evt.currentTarget).attr("eid"), false );
				}, 100);
				return false;
			});
			$app.showScreens(false, false, true, false, false);
			loadAllImages(".serie-row .img-episode");
			proccessScroll();
			
			if (lastWatched != ""){
				$app.scrollToEpisode(lastWatched);
			}
		});
	},

	scrollToEpisode : function(epId){
		var obj = $("#ep"+epId+" .serie-row");
		var screenHalf = ($app._screenHeight() / 2) - (obj.height() / 2); 
		window.scrollTo(0, obj.offset().top - screenHalf);
		/*$("html body").animate({
			scrollTop: (obj.offset().top - screenHalf) + 'px'
		}, 'fast');*/
	},

	loadSeries : function (movies){
		var compl = "";
		if ($app._cookieID != ""){
			compl = "&usr=" + $app._cookieID;
		}

		var req = $.ajax({
			url: "/seriesdata.php?act=" + (movies ? "movies" : "series") + compl,
			method: "GET",
			dataType: "html",
		});
		req.done(function (result){
			$app._jsonSeries = JSON.parse(result);
			$app._seriesCovers().html("");
			$app.showScreens(false, false, false, true, false);
			for (var i = 0; i < $app._jsonSeries.length; i++) {
				var serie = $app._jsonSeries[i];
				$app._seriesCovers().append('<a class="serie-link" href="#" id="' + serie.ID + '" name="' + serie.NAME + '">' + 
											'	<div class="serie-row"  genres="' + serie.GENRE + '" name="' + serie.NAME + '">' + 
											'		<div class="img" img-data="'+$appCfg.imgsLocation+'/cacheimgs/' + serie.IMGCOVER + '" style="background-image: url(/imgs/blank.png);"></div>' +
											'		<div class="name-bg"></div>' +
											'		<div class="name">' + serie.NAME + '</div>' +
											(serie.FAV == 0 ? "" :
											'		<div class="favorite"></div>' )+
											'	</div>' +
											'</a>');
			}
			$(".serie-link").click(function (evt){  
				$app.showScreens(true, false, false, false, false);
				setTimeout(function(){
					$app.loadEpisodes( $(evt.currentTarget).attr("id") );
				}, 100);
				return false;
			});
			loadAllImages(".serie-row .img");
			proccessScroll();
		});		
	},

	loadGenres : function(){
		/*$(".search-form input").value("asd");*/
		/*<div class="category-type">Drama</div>*/
		var req = $.ajax({
			url: "/seriesdata.php?act=genres",
			method: "GET",
			dataType: "html",
		});
		req.done(function (result){
			var genres = JSON.parse(result);
			for (var i = 0; i < genres.length; i++) {
				var genre = genres[i];
				$("#categories-filter .content-holder").append('<div class="category-type">' + genre + '</div>');
			}
			$app.bindGenreCategories();
		});			
	},


	bindGenreCategories : function(){
		$app.visualDebug("Categories Btn Start...");
		$(".category-type").click(function (evt){
			$app.showTopLoading(true);
			setTimeout(function(){
				$(".category-type").removeClass("selected");
				$(evt.currentTarget).addClass("selected");
				var usrSelected = $(evt.currentTarget).html().toString().toLowerCase();
				$(".serie-row").each(function(index, eu){
					if (eu == undefined || $(eu).attr("genres") == undefined) return;
					var genres = $(eu).attr("genres").toString().toLowerCase();
					$(eu).css("display", genres.indexOf(usrSelected) > -1 || usrSelected == "all genres" ? "block" : "none");
				});
				loadAllImages(".serie-row .img");
				proccessScroll();
				$app.showTopLoading(false);
			},333);
			evt.stopPropagation();
			evt.stopImmediatePropagation();
		});
		$app.visualDebug("Categories btn pop");
		var btnPopEvt =function (evt){
			if ( $("#categories-filter-wrap").css("width") == "42px" ){
				$("#categories-filter-wrap").css("width", "243px");
				$("#categories-filter-wrap").addClass("visi");
				$("#categories-filter-wrap .btn-pop").addClass("visi");
			}
			else {
				$("#categories-filter-wrap").css("width", "42px");
				$("#categories-filter-wrap").removeClass("visi");
				$("#categories-filter-wrap .btn-pop").removeClass("visi");
			}
			evt.stopPropagation();
			evt.stopImmediatePropagation();
			return true;
		}; 

		$("#categories-filter-wrap .btn-pop").click(btnPopEvt);
		$app.visualDebug("Body categories click");

		$(document.body).click(function (evt){
			if ( $("#categories-filter-wrap").css("width") == "243px" ){
				$("#categories-filter-wrap").css("width", "42px");
				$("#categories-filter-wrap").removeClass("visi");
				$("#categories-filter-wrap .btn-pop").removeClass("visi");
			}
			evt.stopPropagation();
			evt.stopImmediatePropagation();
			return true;
		});
	},

	loadInitialScreen: function(){
		$app.showScreens(false, false, false, false, true);
	},

	showTopLoading : function(show){
		this._topLoading().css("display", show ? "block" : "none");
	},

	visualDebug: function(text){
		if ( $("#visual-debug").length > 0 )
			$("#visual-debug").html($("#visual-debug").html() + "<br/>" + text);
	},

	loadVisualDebug: function(){
		$(document.body).append('<div id="visual-debug" style="color: #F00; text-align: center; background-color: #000; z-index:999999; position: fixed; top: 0; right: 0;">#Visual Console#</div>');
	},

	initialize : function(cookieID){
		if ($app._USE_VISUAL_DEBUG){
			$app.loadVisualDebug();
		}
		this.setWndTitle("");
		this.setWndImg("/imgs/thatflix.png");
		this._cookieID = cookieID;
		$(".search-form input").bind('keyup',function (self){
			var typed = this.value.toLowerCase();
			$(".serie-row").each(function(index, eu){
				if (eu == undefined || $(eu).attr("name") == undefined) return;
				var name = $(eu).attr("name").toString().toLowerCase();
				$(eu).css("display", name.indexOf(typed) > -1 ? "block" : "none");
			});
			loadAllImages(".serie-row .img");
			proccessScroll();
		});


		$("#clear-search").click(function (){
			var el = $(".search-form input");
			el.val("");
			el.keyup();
			el.focus();
		});

		$("#serieseasons-back").click(function (){
			$app.showScreens(true, false, false, false);
			$app.setWndImg("/imgs/thatflix.png");
			setTimeout(function(){
				$app.showScreens(false, false, false, true, false);
				window.scrollTo(0, 0);
				$app.setWndTitle("");
				loadAllImages(".serie-row .img");
				proccessScroll();
			}, 100);
		});
		

		var fnBtnSeriesClick = function(){
			$(".floatbtn#btn-series").addClass("selected");
			$(".floatbtn#btn-movies").removeClass("selected");
			$app.showScreens(true, false, false, false);
			setTimeout(function(){
				$app.loadSeries(false);
			}, 100);
		};
		var fnBtnMoviesClick = function(){
			$(".floatbtn#btn-movies").addClass("selected");
			$(".floatbtn#btn-series").removeClass("selected");
			$app.showScreens(true, false, false, false, false);
			setTimeout(function(){
				$app.loadSeries(true);
			}, 100);
		};

		$("#btn-series").click(fnBtnSeriesClick);
		$("#ibtn-series").click(fnBtnSeriesClick);
		$("#btn-movies").click(fnBtnMoviesClick);
		$("#ibtn-movies").click(fnBtnMoviesClick);

		$("#try-reload").click(function (){
			var linkData = $("#stream-loading-error").attr("link");
			var mediaCode = $("#stream-loading-error").attr("mediacode");
			$("#stream-loading-error").css("display", "none");
			$app.showScreens(true, false, false, false, false);
			setTimeout(function(){
				$app.loadEpisode( mediaCode, $app._realPlayer == null? false : $app._realPlayer.isFullscreen() );
			}, 10);
		});
		$("#report-broken-link").click(function (){
			var linkData = $("#stream-loading-error").attr("link");
			var mediaCode = $("#stream-loading-error").attr("mediacode");
			$("#submitting-loading").css("display", "block");
			$("#try-reload").css("display", "none");
			$("#report-broken-link").css("display", "none");
			var req = $.ajax({
				url: "/seriesdata.php?act=brokenlink&mid=" + mediaCode + "&uid=" + $app._cookieID + "&link=" + linkData,
				method: "GET",
				dataType: "html",
			});
			req.done(function (result){
				setTimeout(function(){
					$("#submitting-loading").css("display", "none");
					$("#try-reload").css("display", "none");
					$("#report-broken-link").css("display", "none");
					$("#get-back-to-list").css("display", "block");
					$("#stream-loading-error .text").html($app._messageBrokenLinkReported);
					$("#get-back-to-list").click(function (){
						$("#stream-loading-error").css("display", "none");
						$(".floating-backbtn2").click();
					});
				}, 10);
			});	

		});	


		$("#close-user-notice").click(function (){
			$("#user-notice-information").css("display", "none");
		});

		$("#type-login-form").click(function(sender){
			$("#type-register-form").removeClass("selected");	
			$("#type-login-form").addClass("selected");	
			$("#user-login").attr("mode", "login");
			$("#user-login #register-part").css("display", "none");
			$("#user-login-btn").html("Login");
			$("#user-login #errors").removeClass("success");
			$("#user-login #errors").html("");
		});
		$("#type-register-form").click(function(sender){
			$("#type-login-form").removeClass("selected");	
			$("#type-register-form").addClass("selected");	
			$("#user-login").attr("mode", "register");
			$("#user-login #register-part").css("display", "block");
			$("#user-login-btn").html("Register");
			$("#user-login #errors").removeClass("success");
			$("#user-login #errors").html("");
		});

		$("#user-login-btn").click(function (){
			$("#user-login #errors").html("");
			var dataForm = {};
			dataForm["username"] = $("input#username").val();
			dataForm["password"] = $("input#password").val();

			if ($("input#username").val().trim() == ""){
				$("#user-login #errors").html("Please fill USERNAME field.");
				return;
			}
			if ($("input#password").val().trim() == ""){
				$("#user-login #errors").html("Please fill PASSWORD field.");
				return;
			}

			if ($("#user-login").attr("mode") == "register"){
				if ($("input#email").val().trim() == ""){
					$("#user-login #errors").html("Please fill EMAIL field.");
					return;
				}
				dataForm["email"] = $("input#email").val();
				dataForm["cookieID"] = $app._cookieID;
			}
			$.ajax({
				url: 'seriesdata.php?act=usr',
				type: 'post',
				dataType: 'json',
				data: dataForm,
				success: function(data) {
					var dt = data;//JSON.parse(data);
					if (dt.mode == "login"){
						if (dt.result == "false")
							$("#user-login #errors").html("Invalid Username or Password.");
						else {
							$app._cookieID = dt.result;
							$(".user-box #user-name").html( dt.username );/*"(" + dt.username + ") " + dt.email);*/
							$(".user-box #user-photo").html(dt.username[0]);
							$(".user-box #user-photo").removeClass("nousr");
							$(".user-box #user-logout").removeClass("forcehidden");
							$("#user-login").css("display", "none");
						}
					} else 
					if (dt.mode == "register"){
						if (dt.result == "error=useralreadyexists")
							$("#user-login #errors").html("Username Already Exists.");
						else if (dt.result == "error=emailalreadyexists")
							$("#user-login #errors").html("Email Already Registered.");
						else {
							$("#user-login #errors").addClass("success");
							$("#user-login #errors").html("You are successfully registered, please do login to continue.<br/>Enjoy ThatFlix!");
							//$app._cookieID = dt.result;
						}
					}
				}
			});
		});
		$("#user-cancel-btn").click(function (){
			$("#user-login").css("display", "none");
		});
		var fnUserLogout = function (){
			var req = $.ajax({
				url: "/seriesdata.php?act=usrlogout",
				method: "GET",
				dataType: "html",
			});
			req.done(function (result){
				$(".user-box #user-name").html("Login / Register");
				$(".user-box #user-photo").html("");
				$(".user-box #user-photo").addClass("nousr");
				$(".user-box #user-logout").addClass("forcehidden");
				$app._cookieID = $app.GUID();
			});	
		};

		$("#user-logout").click(fnUserLogout);
		$(".user-box").click(function (){
			if ( $(".user-box #user-logout").hasClass("forcehidden") ){
				$("#user-login").css("display", "block");
				$("input#username").focus();
			} else {
				fnUserLogout();
			}
		});		

		this.loadGenres();
		this.showScreens(true, false, false, false, false);
		this.loadInitialScreen();

		window.onscroll = proccessScroll;

		/*document.ready = function (whateva){
			loadAllImages(".serie-row .img");
			window.onscroll = proccessScroll;
			proccessScroll();
		}*/
	}
};

/*
	var req = $.ajax({
		url: "/days/reportTable/uid/"+uid,
		method: "GET",
		dataType: "html",
	});
	req.done(function (result){
		$("#form-user-days").html(result);
	});
*/
/*
 var baseLogFunction = console.log;
    console.log = function(){
        baseLogFunction.apply(console, arguments);

        var args = Array.prototype.slice.call(arguments);
        for(var i=0;i<args.length;i++){
            var node = createLogNode(args[i]);
        }

    }

    function createLogNode(message){
        $app.visualDebug(message);
    }

    window.onerror = function(message, url, linenumber) {
        console.log("JavaScript error: " + message + " on line " +
            linenumber + " for " + url);
    }


	sudo service avahi-daemon stop
	sudo stop network-manager
	airodump-ng wlan0
	airodump-ng -c 7 -w wpa2 --bssid 94:77:2B:EE:DD:34 --ivs mon0
	aireplay-ng --deauth 0 -a 50:A7:2B:AB:A1:7C -c 2C:37:31:0D:56:CD mon0
    */
