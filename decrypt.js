	function base64_decode(r) {
	    var e, t, o, n, a, i, d, h, c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	        f = 0,
	        u = 0,
	        l = "",
	        s = [];
	    if (!r) return r;
	    r += "";
	    do 
	    	n = c.indexOf(r.charAt(f++)), 
	    	a = c.indexOf(r.charAt(f++)), 
	    	i = c.indexOf(r.charAt(f++)), 
	    	d = c.indexOf(r.charAt(f++)), 
	    	h = n << 18 | a << 12 | i << 6 | d, 
	    	e = h >> 16 & 255, t = h >> 8 & 255, 
	    	o = 255 & h, 
	    	64 == i ? s[u++] = String.fromCharCode(e) : 64 == d ? s[u++] = String.fromCharCode(e, t) : s[u++] = String.fromCharCode(e, t, o); 
	    	while (f < r.length);
	    return l = s.join(""), l.replace(/\0+$/, "")
	}

	function ord(r) {
	    var e = r + "",
	        t = e.charCodeAt(0);
	    if (t >= 55296 && 56319 >= t) {
	        var o = t;
	        if (1 === e.length) return t;
	        var n = e.charCodeAt(1);
	        return 1024 * (o - 55296) + (n - 56320) + 65536
	    }
	    return t >= 56320 && 57343 >= t ? t : t
	}

	function decrypt(r, e) {
	    //if (window.callPhantom || window._phantom) return "http://wiki.alluc.to/API-access?ohHi=There&APIalsoWorksForPronTV=justAsk";
	    var t = "";
	    r = base64_decode(r);
	    var o = 0;
	    for (o = 0; o < r.length; o++) {
	        var n = r.substr(o, 1),
	            a = e.substr(o % e.length - 1, 1);
	        n = Math.floor(ord(n) - ord(a)), n = String.fromCharCode(n), t += n
	    }
	    return t
	}

	function unbox(content, key){
		return decrypt(content, key);
	}