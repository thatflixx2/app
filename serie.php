<?php
	include 'utils.php';
	include 'config.php';

	$serieID = $_GET['sid'];
	$qryFindCode =$conn->prepare("SELECT ID, NAME, IMGCOVER, DESCRIPTION FROM SERIES WHERE ID = $serieID");
	$qryFindCode->execute();
	$serie = $qryFindCode->fetchAll();

	$qryFindEpisodes =$conn->prepare("SELECT * FROM EPISODES WHERE SERIE_ID = $serieID");
	$qryFindEpisodes->execute();
	$episodes = $qryFindEpisodes->fetchAll();

	//var_dump($serie[0]["NAME"]);
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8" />
		<title>ThatFlix! - <?php echo $serie[0]["NAME"]; ?></title>
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="maulazyload.js"></script>
	</head>	
	<body >
		<div class="floating-logo1"></div>
		<div class="floating-backbtn1"></div>
		<div id="img-cover" style="">
			<div class="serie-row title">
				<center>
					<?php echo $serie[0]['NAME']; ?>
				</center>
			</div>
			<?php
				$name = $serie[0]["DESCRIPTION"]; 
				if ($name[1] == ".")
					$name[1] = " ";
				echo trim($name);
 			?>
			<div>
				<center>
					<img class="serie-image" src="/cacheimgs/<?php echo $serie[0]["IMGCOVER"]; ?>">		
				</center>
			</div>	
		</div>
		<div class="serie-container">				
			<?php
				$lastSeason = "";
				foreach ($episodes as $key => $value) {

					if ($lastSeason != $value['SEASON']){
						$lastSeason = $value['SEASON'];
						echo "<div class=\"serie-row title\">Season $lastSeason</div>";	
					}

					$episodeid = $value['ID'];
					$season = $value['SEASON'];
					$episode = $value['EPISODE'];
					$name = $value['NAME'];
					$link = $value['LINK'];
					$cover = $value['VIDEOCOVER'];
					$videoSource1 = $value['VIDEOSOURCE1'];
					echo "<a class=\"link-episode\" href=\"#\" eid=\"$episodeid\">
							<div class=\"serie-row\">
								<div class=\"img-episode\" img-data=\"/episodeimgs/$cover\" style=\"background-image: url(/imgs/blank.png);\"></div>
								<div class=\"episode-title\">Ep$episode - $name</div>
							</div>
						</a>";

					$allEpisodes[] = array( "season"=>$season, 
											"episode"=>$episode, 
											"episodeid"=>$episodeid, 
											"episodecover"=> "/episodeimgs/$cover",
											"name"=>utf8_encode($name), 
											"link"=>$link);
					//$allEpisodes[] = array("season"=>$lastSeason, "where"=>$link, "episode"=>$finalSerieName, "episodecover"=> "/episodeimgs/".$finalName.".jpg");
					
				}
			?>
		</div>
		<form id="playform" name="playform" method="POST" action="play.php" style="display: none;">
			<input type="text" id="serieid" name="serieid" value="<?php echo $serie[0]['ID']; ?>"></input>
			<input type="text" id="episodeid" name="episodeid" value=""></input>
			<input type="text" id="title" name="title" value=""></input>
			<input type="text" id="data" name="data" value=""></input>
		<!--	<input type="text" id="season" name="season" value=""></input>
			<input type="text" id="episode" name="episode" value=""></input>
			<input type="text" id="episodecover" name="episodecover" value=""></input>
			<input type="text" id="name" name="name" value=""></input>
			<input type="text" id="link" name="link" value=""></input>-->
		</form>
		<?php //var_dump($allEpisodes); ?>
		<script type="text/javascript">
			var opts = {
				title : "<?php echo $serie[0]['NAME']; ?>",
				data : <?php echo json_encode($allEpisodes); ?>
			};
			$(".floating-backbtn1").click(function (){
				location.href = "/index.php"
			});
			$(".link-episode").click(function (){	
				var eid = $(this).attr("eid");		
				for (var i = 0; i < opts.data.length; i++) 
					if (opts.data[i].episodeid == eid){
						var epp = opts.data[i];
						$("#playform #episodeid").attr("value", eid );
						/*$("#playform #season").attr("value", epp.season );
						$("#playform #episode").attr("value", epp.episode );
						$("#playform #episodecover").attr("value", epp.episodecover );
						$("#playform #name").attr("value", epp.name );
						$("#playform #link").attr("value", epp.link );*/
						$("#playform #title").attr("value", opts.title );

						$("#playform #data").attr("value", JSON.stringify(opts.data) );
						$("#playform").submit();
						break;
					}

				return false;
			});

			document.ready = function (whateva){
				loadAllImages(".serie-row .img-episode");
				window.onscroll = proccessScroll;
				proccessScroll();
			}
		</script>
	</body>
</html>


<?php 
	//var_dump($allEpisodes);
//echo json_encode($allEpisodes);
/*
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    } */?>