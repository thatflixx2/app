<?php 
	include "utils.php";
	include "config.php";


	function loadVideoSourceFromLink($videoSources){
		if ( strpos($videoSources, "[DIRECT]") !== false){
			$result = str_replace("[DIRECT]", "", $videoSources);
			return array(array($result, "SINGLE"));
		}
		$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));

		$file2 = file_get_contents($videoSources, false, $context);
		/*echo $file2;
		echo  "<textarea> " . htmlentities($file2) . "</textarea>";
		die();*/

		$posSources = strpos($file2, 'sources');
		if ($posSources !== false){
			$posSourcesEnd = strpos($file2, ']', $posSources);
			$sources = substr($file2, $posSources, $posSourcesEnd-$posSources+1 );
			
		} else {
			$sources = $file2;
		}
		///"file" : ".+"/
		preg_match_all('/file(\'|")?( )*:( )*.+?(\'|")/', $sources, $files);
		preg_match_all('/label(\'|")?( )*:( )*.+?(\'|")/', $sources, $labels);
		
		$result = array();
		//var_dump($labels);
		//die();
		for ($i=0; $i < sizeof($files[ sizeof($files)-1 ]); $i++) { 
			//var_dump($files[0][$i]);
			$movie = $files[0][$i];
			$label = parseOnlyNumbers($labels[0][$i]);
			$pos2 = strpos($movie, 'http');
			$movie = substr($movie, $pos2, strlen($movie)-$pos2-1 );
			/*$pos2 = strpos($movie, '"file" :"');
			$posend2 = strpos($movie, '"', $pos2+10);
			$movie = substr($movie, $pos2+10, $posend2-$pos2-10);*/
			if ( strpos( strtolower($movie), ".mp4") > 0 ){
				if ( strpos($movie, "thevideo.me") > 0){
					$patternJWKeyFile = "/(?:http(?:s*):\/\/.+?)*\/(?:jwv|jwjsv)\/(.+?)\"/";
					$patternJWKeyFile2 = "/var (?:tk|mpri_Key).*='(.+?)'/";
					$patternJWKey = "/(?:jwConfig|function)\|([a-z|0-9]{18,}?)\|/";
					//$patternKeyLink = "/(https://thevideo.me/jwjsv/(.|\\n)+?)\"/";
					//preg_match_all($patternKeyLink, $file2, $linkKey);
					/*$posKeyFile = strpos($file2, "https://thevideo.me/jwjsv/");
					if ($posKeyFile === false)
						$posKeyFile = strpos($file2, "/jwv/");
					$posKeyFileEnd = strpos($file2, "\"", $posKeyFile);*/
					$linkKey = "";
					preg_match_all($patternJWKeyFile, $file2, $linkKeyX);
					preg_match_all($patternJWKeyFile2, $file2, $linkKeyY);
					if ( sizeof($linkKeyX) > 0 && sizeof($linkKeyX[0]) > 0 ){
						if ( strpos($linkKeyX[1][0], "thevideo.me") !== false )
							$linkKey = $linkKeyX[1][0];
						else 
							$linkKey = "http://thevideo.me/" . $linkKeyX[1][0];
					} else if ( sizeof($linkKeyX) > 1) {
						$linkKey = "http://thevideo.me/jwv/" . $linkKeyY[1][0];
					}


					/*echo "<pre>";
					var_dump($linkKeyX);
					var_dump($linkKeyY);
					var_dump($linkKey);
					echo "</pre>";


					die();*/

					$linkKey = htmlentities( $linkKey ); //substr($file2, $posKeyFile, $posKeyFileEnd-$posKeyFile) );
					$fileLinkKey = file_get_contents( $linkKey );
					preg_match_all($patternJWKey, $fileLinkKey, $keyFromFile);
					/*echo "<pre>";
					var_dump($videoSources);
					var_dump($linkKey);
					var_dump($keyFromFile);
					echo "</pre>";
					echo "<textarea>" .  htmlentities($file2) . "</textarea>";
					//echo $fileLinkKey;
					die();*/
					$movie .= "?direct=false&ua=1&vt=" . $keyFromFile[1][0];
				}

				$result[] = array( urlencode($movie) , $label, urlencode($videoSources) );
			}
		}
		return $result;
	}

	if ($_GET["act"] == "usrfeedback"){	
		$usr = $_GET['usr'];
		$eid = $_GET['eid'];	
		$qryFindUsrFeedBack = $conn->prepare("SELECT * FROM userwatched WHERE reference=:REFERENCE AND uid=:UID");
		$qryFindUsrFeedBack->execute(array('REFERENCE'=>$eid, 'UID'=>$usr));
		$foundFeedBack = $qryFindUsrFeedBack->fetchAll();
		if ( sizeof($foundFeedBack) > 0 ){
			$lastId = $foundFeedBack[0]["id"];
		} else {
			$qryFindCode = $conn->prepare("INSERT INTO userwatched (REFERENCE, REFTYPE, TOTALTIME, UID) VALUES (:REFERENCE, :REFTYPE, :TOTALTIME, :UID)");
			$qryFindCode->execute(array('REFERENCE'=>$eid, 'REFTYPE'=>0, 'TOTALTIME'=>0, 'UID'=>$usr));
			$lastId = $conn->lastInsertId();
		}
		echo json_encode( array($lastId) );
	}
	else 
	if ($_GET["act"] == "usrfeedbackupd"){	
		$rid = $_GET['rid'];
		$time = $_GET['time'];	
		$qryFindCode = $conn->prepare("UPDATE userwatched SET TOTALTIME = :TOTALTIME WHERE ID = :RID");
		$qryFindCode->execute(array('TOTALTIME'=>$time, 'RID'=>$rid));
	}
	if ($_GET["act"] == "brokenlink"){	
		$mid = $_GET['mid'];
		$uid = $_GET['uid'];
		$link = $_GET['link'];	
		$qryFindCode = $conn->prepare("INSERT INTO BROKENLINKS (LINK, IDMEDIA, USER, WHENHAPPENED) VALUES (:LINK, :IDMEDIA, :USER, NOW())");
		$qryFindCode->execute(array( "LINK" => $link,
									 "IDMEDIA" => $mid,
									 "USER" => $uid)
								);
	}
	else 
	if ($_GET["act"] == "series" || $_GET["act"] == "movies"){
		$typeToFind = $_GET["act"] == "series" ? "0" : "1";
		$alreadyWatched = "-1";
		$results = array();
		if ( isset($_GET['usr']) ){
			$uid = $_GET['usr'];
			$qryFindCode = $conn->prepare("SELECT distinct(S.ID), S.NAME, S.IMGCOVER, S.STYPE, S.GENRE, '1' as FAV FROM SERIES S  
											INNER JOIN EPISODES EP ON (S.ID = EP.SERIE_ID) 
											INNER JOIN userwatched UW ON (UW.reference = EP.ID) 
											WHERE UW.uid = :usr AND S.STYPE = $typeToFind
											ORDER BY S.NAME");
			$qryFindCode->execute(array('usr' => $uid));
			$foundCodes = $qryFindCode->fetchAll();
			foreach ($foundCodes as $key => $value) {
				$alreadyWatched .= ($alreadyWatched == "" ? "" : ", ") . $value["ID"];
				//$results[] = $value;
				$results[] = array(	"ID"=>$value["ID"], 
									"NAME"=>$value["NAME"], 
									"IMGCOVER"=>$value["IMGCOVER"], 
									"GENRE"=>$value["GENRE"], 
									"STYPE"=>$value["STYPE"], 
									"FAV"=>$value["FAV"]);
			}		
		}
		//echo "asd"; die();	
		$sqlComplement = "";// DATERELEASED, TOTALTIME, DIRECTOR, CAST,";
		$qryFindCode = $conn->prepare("SELECT ID, NAME, IMGCOVER, STYPE, GENRE," . $sqlComplement . " '0' as FAV 
										FROM SERIES WHERE ID NOT IN (" . $alreadyWatched . ") AND STYPE = $typeToFind");
		$qryFindCode->execute();
		$foundCodes = $qryFindCode->fetchAll();
		foreach ($foundCodes as $key => $value) {
			//$results[] = $value;
			/*if ($_GET["act"] == "movies"){
				$results[] = array( "ID"=>$value["ID"], 
									"NAME"=>$value["NAME"], 
									"IMGCOVER"=>$value["IMGCOVER"], 
									"GENRE"=>$value["GENRE"], 
									"DATERELEASED"=>$value["DATERELEASED"], 
									"TOTALTIME"=>$value["TOTALTIME"], 
									"DIRECTOR"=>$value["DIRECTOR"], 
									"CAST"=>$value["CAST"], 
									"STYPE"=>$value["STYPE"], 
									"FAV"=>$value["FAV"]);
			} else {*/		
				$results[] = array( "ID"=>$value["ID"], 
									"NAME"=>$value["NAME"], 
									"IMGCOVER"=>$value["IMGCOVER"],  
									"GENRE"=>$value["GENRE"], 
									"STYPE"=>$value["STYPE"], 
									"FAV"=>$value["FAV"]);
			//}
		}		
		//echo json_encode( $foundCodes );
		echo json_encode( $results );
	}
	else
	if ($_GET["act"] == "serie"){
		$serieID = $_GET['sid'];
		$qryFindCode = $conn->prepare("SELECT * FROM SERIES WHERE ID = :ID");
		$qryFindCode->execute(array("ID"=>$serieID));
		$foundCodes = $qryFindCode->fetchAll();
		if ( sizeof($foundCodes) <= 0 )
			echo "{}";
		else 		
			echo json_encode( $foundCodes[0] );
	}
	else
	if ($_GET["act"] == "episodes"){
		$serieID = $_GET['sid'];
		$userID = $_GET['uid'];
		$qryFindCode =$conn->prepare("SELECT ID, NAME, IMGCOVER, DESCRIPTION, GENRE, DATERELEASED, TOTALTIME, DIRECTOR, CAST, STYPE FROM SERIES WHERE ID = :SID");
		$qryFindCode->execute(array('SID'=>$serieID));
		$serie = $qryFindCode->fetchAll()[0];
		$qryFindEpisodes =$conn->prepare("SELECT EP.*,
										(SELECT UW.totaltime FROM userwatched UW WHERE UW.reference = EP.ID AND UW.totaltime > 0 AND UW.uid = :UID LIMIT 1) AS WATCHED 
										FROM EPISODES EP
										WHERE EP.SERIE_ID = :SID
										ORDER BY EP.SEASON DESC, EP.EPISODE DESC");
		//$qryFindEpisodes =$conn->prepare("SELECT * FROM EPISODES WHERE SERIE_ID = $serieID ORDER BY SEASON DESC, EPISODE DESC");
		$qryFindEpisodes->execute(array('SID'=>$serieID, 'UID'=>$userID));
		$episodes = $qryFindEpisodes->fetchAll();
		$epresults = array();
		foreach ($episodes as $key => $value) {
			$epresults[] = array("ID" => $value["ID"],
								 "SERIE_ID" => $value["SERIE_ID"],
								 "SEASON" => $value["SEASON"],
								 "EPISODE" => $value["EPISODE"],
								 "NAME" => $value["NAME"],
								 "LINK" => $value["LINK"],
								 "VIDEOCOVER" => $value["VIDEOCOVER"],
								 /*"VIDEOSOURCE1" => $value["VIDEOSOURCE1"],
								 "VIDEOSOURCE2" => $value["VIDEOSOURCE2"],
								 "VIDEOSOURCE3" => $value["VIDEOSOURCE3"],*/
								 "WATCHED" => $value["WATCHED"] == null ? "0" : $value["WATCHED"]);
		}

		$finalDesc = trim($serie['DESCRIPTION']);
		if ($finalDesc[0] == ".")
			$finalDesc[0] = " ";
		$finalDesc = trim($finalDesc);
		$result = array("NAME"=>$serie['NAME'],
						"ID"=>$serie['ID'],
						"IMGCOVER"=>$serie['IMGCOVER'],
						"DESCRIPTION"=>$finalDesc,
						"GENRE"=>$serie['GENRE'],
						"DATERELEASED"=>$serie['DATERELEASED'],
						"TOTALTIME"=>$serie['TOTALTIME'],
						"DIRECTOR"=>$serie['DIRECTOR'],
						"CAST"=>$serie['CAST'],
						"STYPE"=>$serie['STYPE'],
						"EPISODES"=>$epresults);
		echo json_encode($result);
		///var_dump($result);
		//echo json_last_error();
	}
	else
	if ($_GET["act"] == "episode"){
		$eid = $_GET['eid'];
		$qryFindEpisodes = $conn->prepare("SELECT * FROM EPISODES WHERE ID = :eid");
		$qryFindEpisodes->execute(array('eid' => $eid));
		$episode = $qryFindEpisodes->fetchAll()[0];
		$result = array();
		$linkRelation = $episode["LINK"];
		$link = "http://www.alluc.ee/stream/" . $linkRelation . "+host%3Aopenload.co";
		$execRes = exec("findLinks.php " . $link);
		$videosss = str_replace("\n", "|", $execRes);
		$videosSources = explode('|', $videosss);


		if ( sizeof($videosSources) > 0) {
			var_dump($videosSources);
			die();
			$result = loadVideoSourceFromLink($videosSources[0]);
		} else {
			if ($episode["VIDEOSOURCE1"] != ""){
				$result = loadVideoSourceFromLink($episode["VIDEOSOURCE1"]);
			} 
			if (sizeof($result) == 0 && $episode["VIDEOSOURCE2"] != ""){
				$result = loadVideoSourceFromLink($episode["VIDEOSOURCE2"]);
			}  
			if (sizeof($result) == 0 && $episode["VIDEOSOURCE3"] != ""){
				$result = loadVideoSourceFromLink($episode["VIDEOSOURCE3"]);
			} 
			if (sizeof($result) == 0 && $episode["VIDEOSOURCEM"] != ""){
				//echo "got_here" ;
				$videosss = $episode["VIDEOSOURCEM"];
				$videosss = str_replace("\n", "|", $videosss);
				$videosSources = explode('|', $videosss);
				foreach ($videosSources as $key => $value) {
					$result = loadVideoSourceFromLink($value);
					if ( sizeof($result) > 0 ) break;
				}
				if ( isset($_SESSION["COOKIEID"]) ){
					$cid = $_SESSION["COOKIEID"];
					$myfile = fopen("./videoids/$cid.txt", "w");
					fwrite($myfile, json_encode($result));
					fclose($myfile);
				}			
			} 
		}

		if ( sizeof($result) > 0 && !isset($_GET['ns']) )
			for ($i=0; $i<sizeof($result); $i++){
				$size = $result[$i][1];
				$result[$i][0] = "http://thatflix.tk/video.php?vid=$eid&size=$size";
			}


		echo json_encode($result);
	}
	else
	if ($_GET["act"] == "genres"){
		$qryGenres = $conn->prepare("SELECT DISTINCT(GENRE) FROM SERIES");
		$qryGenres->execute();		
		$genres = $qryGenres->fetchAll();
		$results = array();
		foreach ($genres as $key => $value) {
			if ( $value == null || $value == "" ) continue;
			$explodedGenres = explode(',', $value["GENRE"]);
			//var_dump($explodedGenres);
			foreach ($explodedGenres as $kGenre => $daGenre) {
				if (trim($daGenre) != "" && !in_array(trim($daGenre), $results))
					$results[] = trim($daGenre);
			}
		}
		sort($results);
		echo json_encode($results);
	}
	else
	if ($_GET["act"] == "subtitles"){
		$eid = $_GET['mid'];
		$qryFindEpisodes = $conn->prepare("SELECT * FROM EPISODES WHERE ID = :eid");
		$qryFindEpisodes->execute(array('eid' => $eid));
		$episode = $qryFindEpisodes->fetchAll()[0];

		$subtitles = scandir("./subtitles", 1);
		$results = array();
		//echo $episode['LINK'] . "<br/><br/>";

		//var_dump($subtitles);
		foreach ($subtitles as $key => $value) {
			//echo $value . "-" . strpos($value, $episode['LINK']) . "<br/>";
			if (strpos($value, $episode['LINK']) !== false){
				$lang = $value;
				$lang = str_replace($episode['LINK'], "", $lang);
				$lang = str_replace(".vtt", "", $lang);
				if ($lang == "")
					$lang = "EN";
				if ($lang[0] == "-")
					$lang = substr($lang, 1);

				$results[] = array($value, $lang);
			}
		}
		echo json_encode($results);
	}
	else 
	if ($_GET["act"] == "usr"){
		$username = $_POST["username"];
		$password = $_POST["password"];
		$result = array();

		if ( isset($_POST["email"]) ){
			$result["mode"] = "register";
			$email = $_POST["email"];
			$cookieID = $_POST["cookieID"];

			$qryUser = $conn->prepare("SELECT * FROM USERS WHERE USERNAME = :USERNAME OR LOWER(EMAIL) = LOWER(:EMAIL)");
			$qryUser->execute(array("USERNAME"=>$username, "EMAIL"=>$email));		
			$user = $qryUser->fetchAll();
			if ( sizeof($user) > 0 ){
				if ( strtolower($user[0]["USERNAME"]) == strtolower($username) )
					$result["result"] = "error=useralreadyexists";
				else	
					$result["result"] = "error=emailalreadyexists";
			} else {
				$qryUser = $conn->prepare("INSERT INTO USERS (USERNAME, PASSWORD, EMAIL, COOKIEID, ENABLED) VALUES (:USERNAME, :PASSWORD, :EMAIL, :COOKIEID, 1)");
				$qryUser->execute(array("USERNAME"=>$username, 
										"PASSWORD"=>$password, 
										"EMAIL"=>$email,
										"COOKIEID"=>$cookieID));		
				$result["result"] = "allgood";
			}

		} else {
			$result["mode"] = "login";
			$qryUser = $conn->prepare("SELECT * FROM USERS WHERE USERNAME = :USERNAME AND PASSWORD = :PASSWORD");
			$qryUser->execute(array("USERNAME"=>$username, "PASSWORD"=>$password));		
			$user = $qryUser->fetchAll();
			
			if ( sizeof($user) > 0 ){
				$result["result"] = $user[0]["COOKIEID"];
				$result["email"] = $user[0]["EMAIL"];
				$result["username"] = $user[0]["USERNAME"];
				$_SESSION["USERNAME"] = $user[0]["USERNAME"];
				$_SESSION["COOKIEID"] = $user[0]["COOKIEID"];
				$_SESSION["EMAIL"] = $user[0]["EMAIL"];
				setcookie(
					$cookieName,
					$_SESSION["COOKIEID"],
					time() + (60 * 60 * 24 * 365 * 20)
				);
			} else {
				$result["result"] = "false";
			}
		}
		echo json_encode($result);
	}
	else 
	if ($_GET["act"] == "usrlogout"){
		if ( isset($_SESSION["USERNAME"]) ){
			unset($_SESSION["USERNAME"]);
			unset($_SESSION["COOKIEID"]);
			unset($_SESSION["EMAIL"]);
			session_destroy();
		}
	}

//		var_dump($episode);
//		echo $episode["VIDEOSOURCE1"];	
		/*$opts = array(
			'http'=>array(
				'method'=>"GET",
				'header'=>"Accept-language: en\r\n" .
			//	"Cookie: foo=bar\r\n" .
				"User-agent: BROWSER-DESCRIPTION-HERE\r\n".
				"X-Requested-With:ShockwaveFlash/21.0.0.216\r\n".
				"Referer: " . $episode["VIDEOSOURCE1"] . "\r\n"
			)
		);
		$context = stream_context_create($opts);




php findLinks.php -usedb thevideo.me "14277,14278,14279,14280,14281,14282,14283,14284,14285,14286,14287,14288,14289,14290,14291,14292,14293,14294,14295,14296,14297,14298,14299"




INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 1, "Episode 1",   "the-simpsons-s0e1",  "the-simpsons-s0e1.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 2, "Episode 2",   "the-simpsons-s0e2",  "the-simpsons-s0e2.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 3, "Episode 3",   "the-simpsons-s0e3",  "the-simpsons-s0e3.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 4, "Episode 4",   "the-simpsons-s0e4",  "the-simpsons-s0e4.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 5, "Episode 5",   "the-simpsons-s0e5",  "the-simpsons-s0e5.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 6, "Episode 6",   "the-simpsons-s0e6",  "the-simpsons-s0e6.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 7, "Episode 7",   "the-simpsons-s0e7",  "the-simpsons-s0e7.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 8, "Episode 8",   "the-simpsons-s0e8",  "the-simpsons-s0e8.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 9, "Episode 9",   "the-simpsons-s0e9",  "the-simpsons-s0e9.jpg"); 
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 10, "Episode 10", "the-simpsons-s0e10", "the-simpsons-s0e10.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 11, "Episode 11", "the-simpsons-s0e11", "the-simpsons-s0e11.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 12, "Episode 12", "the-simpsons-s0e12", "the-simpsons-s0e12.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 13, "Episode 13", "the-simpsons-s0e13", "the-simpsons-s0e13.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 14, "Episode 14", "the-simpsons-s0e14", "the-simpsons-s0e14.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 15, "Episode 15", "the-simpsons-s0e15", "the-simpsons-s0e15.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 16, "Episode 16", "the-simpsons-s0e16", "the-simpsons-s0e16.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 17, "Episode 17", "the-simpsons-s0e17", "the-simpsons-s0e17.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 18, "Episode 18", "the-simpsons-s0e18", "the-simpsons-s0e18.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 19, "Episode 19", "the-simpsons-s0e19", "the-simpsons-s0e19.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 20, "Episode 20", "the-simpsons-s0e20", "the-simpsons-s0e20.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 21, "Episode 21", "the-simpsons-s0e21", "the-simpsons-s0e21.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 22, "Episode 22", "the-simpsons-s0e22", "the-simpsons-s0e22.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 23, "Episode 23", "the-simpsons-s0e23", "the-simpsons-s0e23.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 24, "Episode 24", "the-simpsons-s0e24", "the-simpsons-s0e24.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 25, "Episode 25", "the-simpsons-s0e25", "the-simpsons-s0e25.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 26, "Episode 26", "the-simpsons-s0e26", "the-simpsons-s0e26.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 27, "Episode 27", "the-simpsons-s0e27", "the-simpsons-s0e27.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 28, "Episode 28", "the-simpsons-s0e28", "the-simpsons-s0e28.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 29, "Episode 29", "the-simpsons-s0e29", "the-simpsons-s0e29.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 30, "Episode 30", "the-simpsons-s0e30", "the-simpsons-s0e30.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 31, "Episode 31", "the-simpsons-s0e31", "the-simpsons-s0e31.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 32, "Episode 32", "the-simpsons-s0e32", "the-simpsons-s0e32.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 33, "Episode 33", "the-simpsons-s0e33", "the-simpsons-s0e33.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 34, "Episode 34", "the-simpsons-s0e34", "the-simpsons-s0e34.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 35, "Episode 35", "the-simpsons-s0e35", "the-simpsons-s0e35.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 36, "Episode 36", "the-simpsons-s0e36", "the-simpsons-s0e36.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 37, "Episode 37", "the-simpsons-s0e37", "the-simpsons-s0e37.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 38, "Episode 38", "the-simpsons-s0e38", "the-simpsons-s0e38.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 39, "Episode 39", "the-simpsons-s0e39", "the-simpsons-s0e39.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 40, "Episode 40", "the-simpsons-s0e40", "the-simpsons-s0e40.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 41, "Episode 41", "the-simpsons-s0e41", "the-simpsons-s0e41.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 42, "Episode 42", "the-simpsons-s0e42", "the-simpsons-s0e42.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 43, "Episode 43", "the-simpsons-s0e43", "the-simpsons-s0e43.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 44, "Episode 44", "the-simpsons-s0e44", "the-simpsons-s0e44.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 45, "Episode 45", "the-simpsons-s0e45", "the-simpsons-s0e45.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 46, "Episode 46", "the-simpsons-s0e46", "the-simpsons-s0e46.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 47, "Episode 47", "the-simpsons-s0e47", "the-simpsons-s0e47.jpg");
INSERT INTO EPISODES (SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER) VALUES (1539, 0, 48, "Episode 48", "the-simpsons-s0e48", "the-simpsons-s0e48.jpg");

http://thevideo.me/embed-.html


UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-lkrxjnjjonoj.html", NAME="Good Night" WHERE ID = 14756;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-r675ceu580k8.html", NAME="Watching TV" WHERE ID = 14757;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-ted3mhdfebmk.html", NAME="Jumping Bart" WHERE ID = 14758;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-4vsm2doym3hw.html", NAME="Babysitting Maggie" WHERE ID = 14759;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-d9ymda4l9nih.html", NAME="The Pacifier" WHERE ID = 14760;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-2so611vzp32l.html", NAME="Burping Contest" WHERE ID = 14761;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-ucjncozzqcym.html", NAME="Dinnertime" WHERE ID = 14762;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-uuzoja91nu78.html", NAME="Making Faces" WHERE ID = 14763;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-znozz2w4pdax.html", NAME="The Funeral" WHERE ID = 14764;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-k4hshpmp267u.html", NAME="Maggie's Brain" WHERE ID = 14765;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-xvz01cs1tytn.html", NAME="Football" WHERE ID = 14766;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-c6r6ud7tzl0o.html", NAME="House of Cards" WHERE ID = 14767;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-hd8snntf56ma.html", NAME="Bart and Dad Eat Dinner" WHERE ID = 14768;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-m6kywd5r7s0e.html", NAME="Space Patrol" WHERE ID = 14769;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-tzty4w7i6z8s.html", NAME="Bart's Haircut" WHERE ID = 14770;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-it6ssjlsynm5.html", NAME="World War III" WHERE ID = 14771;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-b3b388584rd9.html", NAME="The Perfect Crime" WHERE ID = 14772;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-yph59f44canz.html", NAME="Scary Stories" WHERE ID = 14773;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-utu79knfpcox.html", NAME="Grampa and the Kids" WHERE ID = 14774;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-639sf6pglq3j.html", NAME="Gone Fishin" WHERE ID = 14775;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-c7utk1exbdwm.html", NAME="Skateboarding" WHERE ID = 14776;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-yva6y0a3dicc.html", NAME="The Pagans" WHERE ID = 14777;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-zrz1yunbuva7.html", NAME="Closeted" WHERE ID = 14778;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-gea56jb05par.html", NAME="The Aquarium" WHERE ID = 14779;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-6qpbg8two9ww.html", NAME="Family Portrait" WHERE ID = 14780;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-8sij1lln51kx.html", NAME="Bart's Hiccups" WHERE ID = 14781;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-tpjp3t9vr0df.html", NAME="The Money Jar" WHERE ID = 14782;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-2hns38tyfy64.html", NAME="The Art Museum" WHERE ID = 14783;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-ray4b0xm82gs.html", NAME="Zoo Story" WHERE ID = 14784;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-8y8jjd52xbpx.html", NAME="Shut Up, Simpsons" WHERE ID = 14785;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-tuu4pegyfkd9.html", NAME="Shell Game" WHERE ID = 14786;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-dm50c7ncf4ar.html", NAME="The Bart Simpson Show" WHERE ID = 14787;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-lpkiyqynvqu7.html", NAME="Punching Bag" WHERE ID = 14788;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-bya136zorqab.html", NAME="Simpson Christmas" WHERE ID = 14789;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-bmbrmbbanagi.html", NAME="The Krusty the Clown Show" WHERE ID = 14790;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-cpsfqthi67iy.html", NAME="Bart the Hero" WHERE ID = 14791;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-7f9jsij8qgqu.html", NAME="Bart's Little Fantasy" WHERE ID = 14792;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-k6f92sqsdlua.html", NAME="Scary Movie" WHERE ID = 14793;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-4tzgwrzyedm2.html", NAME="Home Hypnotism" WHERE ID = 14794;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-t52uaocw96rl.html", NAME="Shoplifting" WHERE ID = 14795;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-0bofs4dvwn0q.html", NAME="Echo Canyon" WHERE ID = 14796;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-vffx8upd333l.html", NAME="Bathtime" WHERE ID = 14797;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-opewog1nrc62.html", NAME="Bart's Nightmare" WHERE ID = 14798;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-rg3id5whv5rc.html", NAME="Bart of the Jungle" WHERE ID = 14799;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-tcz9wof2t11c.html", NAME="Family Therapy" WHERE ID = 14800;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-n83kghssgbmf.html", NAME="Maggie In Peril: Episode 1" WHERE ID = 14801;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-26wvjrlhpxqg.html", NAME="Maggie In Peril: Episode 2" WHERE ID = 14802;
UPDATE EPISODES SET VIDEOSOURCE1="", VIDEOSOURCEM="http://thevideo.me/embed-qi4l2x15bm65.html", NAME="TV Simpsons" WHERE ID = 14803;
;
;
UPDATE EPISODES SET VIDEOSOURCE1 = "" WHERE ID = 14804





14758,14759,14760,14761,14762,14763,14764,14765,14766,14767,14768,14769,14770,14771,14772,14773,14774,14775,14776,14777,14778,14779,14780,14781,14782,14783,14784,14785,14786,14787,14788,14789,14790,14791,14792,14793,14794,14795,14796,14797,14798,14799,14800,14801,14802,14803










		*/
?>

