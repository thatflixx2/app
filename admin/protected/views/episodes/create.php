<?php
$this->breadcrumbs=array(
	'Episodes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EPISODES', 'url'=>array('index')),
	array('label'=>'Manage EPISODES', 'url'=>array('admin')),
);
?>

<h1>Create EPISODES</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>