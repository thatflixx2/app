<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SERIE_ID'); ?>
		<?php echo $form->textField($model,'SERIE_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SEASON'); ?>
		<?php echo $form->textField($model,'SEASON'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EPISODE'); ?>
		<?php echo $form->textField($model,'EPISODE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>60,'maxlength'=>123)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LINK'); ?>
		<?php echo $form->textField($model,'LINK',array('size'=>60,'maxlength'=>456)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIDEOCOVER'); ?>
		<?php echo $form->textField($model,'VIDEOCOVER',array('size'=>60,'maxlength'=>456)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIDEOSOURCE1'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE1',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIDEOSOURCE2'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE2',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIDEOSOURCE3'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE3',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIDEOSOURCEM'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCEM',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->