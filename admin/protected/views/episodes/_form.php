<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'episodes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'SERIE_ID'); ?>
		<?php echo $form->textField($model,'SERIE_ID'); ?>
		<?php echo $form->error($model,'SERIE_ID'); ?>
		<label id="LABELL_EPISODES_SERIE_ID" for="EPISODES_SERIE_ID"></label>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SEASON'); ?>
		<?php echo $form->textField($model,'SEASON'); ?>
		<?php echo $form->error($model,'SEASON'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EPISODE'); ?>
		<?php echo $form->textField($model,'EPISODE'); ?>
		<?php echo $form->error($model,'EPISODE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>60,'maxlength'=>123)); ?>
		<?php echo $form->error($model,'NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LINK'); ?>
		<?php echo $form->textField($model,'LINK',array('size'=>60,'maxlength'=>456)); ?>
		<?php echo $form->error($model,'LINK'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VIDEOCOVER'); ?>
		<?php echo $form->textField($model,'VIDEOCOVER',array('size'=>60,'maxlength'=>456)); ?>
		<?php echo $form->error($model,'VIDEOCOVER'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VIDEOSOURCE1'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE1',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'VIDEOSOURCE1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VIDEOSOURCE2'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE2',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'VIDEOSOURCE2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VIDEOSOURCE3'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCE3',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'VIDEOSOURCE3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VIDEOSOURCEM'); ?>
		<?php echo $form->textArea($model,'VIDEOSOURCEM',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'VIDEOSOURCEM'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	var theEpisode = null;

	$("#EPISODES_SERIE_ID").bind("focusout", function(sender){
		var serieID = $(sender.srcElement).val();
		var req = $.ajax({
			url: "/seriesdata.php?act=serie&sid=" + serieID,
			method: "GET",
			dataType: "json",
			success: function (result){
				$("#LABELL_EPISODES_SERIE_ID").html(result.NAME);
				theEpisode = result;
			} 
		});
	});
	$("#EPISODES_EPISODE").bind("focusout", function(sender){
		var serieID = $("#EPISODES_SERIE_ID").val();
		var episode = $("#EPISODES_EPISODE").val();
		var season  = $("#EPISODES_SEASON").val();
		var link = theEpisode.NAME.toString().toLowerCase().replace(" ", "-") + "-s" + season + "e" + episode;

		if ( $("#EPISODES_NAME").val() == "" )
			$("#EPISODES_NAME").val("Episode " + episode);
		
		if ( $("#EPISODES_LINK").val() == "" )
			$("#EPISODES_LINK").val(link);
		
		if ( $("#EPISODES_VIDEOCOVER").val() == "" )
			$("#EPISODES_VIDEOCOVER").val(link + ".jpg");
	});
	$("#EPISODES_SERIE_ID").focus();
</script>