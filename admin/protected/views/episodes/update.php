<?php
$this->breadcrumbs=array(
	'Episodes'=>array('index'),
	$model->NAME=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List EPISODES', 'url'=>array('index')),
	array('label'=>'Create EPISODES', 'url'=>array('create')),
	array('label'=>'View EPISODES', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage EPISODES', 'url'=>array('admin')),
);
?>

<h1>Update EPISODES <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>