<?php
$this->breadcrumbs=array(
	'Episodes'=>array('index'),
	$model->NAME,
);

$this->menu=array(
	array('label'=>'List EPISODES', 'url'=>array('index')),
	array('label'=>'Create EPISODES', 'url'=>array('create')),
	array('label'=>'Update EPISODES', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete EPISODES', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EPISODES', 'url'=>array('admin')),
);
?>

<h1>View EPISODES #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'SERIE_ID',
		'SEASON',
		'EPISODE',
		'NAME',
		'LINK',
		'VIDEOCOVER',
		'VIDEOSOURCE1',
		'VIDEOSOURCE2',
		'VIDEOSOURCE3',
		'VIDEOSOURCEM',
	),
)); ?>
