<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SERIE_ID')); ?>:</b>
	<?php echo CHtml::encode($data->SERIE_ID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SEASON')); ?>:</b>
	<?php echo CHtml::encode($data->SEASON); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EPISODE')); ?>:</b>
	<?php echo CHtml::encode($data->EPISODE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAME')); ?>:</b>
	<?php echo CHtml::encode($data->NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LINK')); ?>:</b>
	<?php echo CHtml::encode($data->LINK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VIDEOCOVER')); ?>:</b>
	<?php echo CHtml::encode($data->VIDEOCOVER); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('VIDEOSOURCE1')); ?>:</b>
	<?php echo CHtml::encode($data->VIDEOSOURCE1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VIDEOSOURCE2')); ?>:</b>
	<?php echo CHtml::encode($data->VIDEOSOURCE2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VIDEOSOURCE3')); ?>:</b>
	<?php echo CHtml::encode($data->VIDEOSOURCE3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VIDEOSOURCEM')); ?>:</b>
	<?php echo CHtml::encode($data->VIDEOSOURCEM); ?>
	<br />

	*/ ?>

</div>