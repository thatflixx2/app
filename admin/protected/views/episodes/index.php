<?php
$this->breadcrumbs=array(
	'Episodes',
);

$this->menu=array(
	array('label'=>'Create EPISODES', 'url'=>array('create')),
	array('label'=>'Manage EPISODES', 'url'=>array('admin')),
);
?>

<h1>Episodes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
