<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>60,'maxlength'=>123)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IMGCOVER'); ?>
		<?php echo $form->textField($model,'IMGCOVER',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DESCRIPTION'); ?>
		<?php echo $form->textArea($model,'DESCRIPTION',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'GENRE'); ?>
		<?php echo $form->textField($model,'GENRE',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DATERELEASED'); ?>
		<?php echo $form->textField($model,'DATERELEASED',array('size'=>60,'maxlength'=>66)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TOTALTIME'); ?>
		<?php echo $form->textField($model,'TOTALTIME',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DIRECTOR'); ?>
		<?php echo $form->textField($model,'DIRECTOR',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CAST'); ?>
		<?php echo $form->textArea($model,'CAST',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'STYPE'); ?>
		<?php echo $form->textField($model,'STYPE'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->