<?php
$this->breadcrumbs=array(
	'Series'=>array('index'),
	$model->NAME,
);

$this->menu=array(
	array('label'=>'List SERIES', 'url'=>array('index')),
	array('label'=>'Create SERIES', 'url'=>array('create')),
	array('label'=>'Update SERIES', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete SERIES', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SERIES', 'url'=>array('admin')),
);
?>

<h1>View SERIES #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'NAME',
		'IMGCOVER',
		'DESCRIPTION',
		'GENRE',
		'DATERELEASED',
		'TOTALTIME',
		'DIRECTOR',
		'CAST',
		'STYPE',
	),
)); ?>
