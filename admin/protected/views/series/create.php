<?php
$this->breadcrumbs=array(
	'Series'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SERIES', 'url'=>array('index')),
	array('label'=>'Manage SERIES', 'url'=>array('admin')),
);
?>

<h1>Create SERIES</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>