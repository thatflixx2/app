<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'series-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>60,'maxlength'=>123)); ?>
		<?php echo $form->error($model,'NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IMGCOVER'); ?>
		<?php echo $form->textField($model,'IMGCOVER',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'IMGCOVER'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DESCRIPTION'); ?>
		<?php echo $form->textArea($model,'DESCRIPTION',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'DESCRIPTION'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'GENRE'); ?>
		<?php echo $form->textField($model,'GENRE',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'GENRE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DATERELEASED'); ?>
		<?php echo $form->textField($model,'DATERELEASED',array('size'=>60,'maxlength'=>66)); ?>
		<?php echo $form->error($model,'DATERELEASED'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TOTALTIME'); ?>
		<?php echo $form->textField($model,'TOTALTIME',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'TOTALTIME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DIRECTOR'); ?>
		<?php echo $form->textField($model,'DIRECTOR',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'DIRECTOR'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CAST'); ?>
		<?php echo $form->textArea($model,'CAST',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'CAST'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'STYPE'); ?>
		<?php echo $form->textField($model,'STYPE'); ?>
		<?php echo $form->error($model,'STYPE'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->