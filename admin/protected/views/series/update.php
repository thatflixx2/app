<?php
$this->breadcrumbs=array(
	'Series'=>array('index'),
	$model->NAME=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List SERIES', 'url'=>array('index')),
	array('label'=>'Create SERIES', 'url'=>array('create')),
	array('label'=>'View SERIES', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage SERIES', 'url'=>array('admin')),
);
?>

<h1>Update SERIES <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>