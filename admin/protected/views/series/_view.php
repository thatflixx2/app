<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAME')); ?>:</b>
	<?php echo CHtml::encode($data->NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IMGCOVER')); ?>:</b>
	<?php echo CHtml::encode($data->IMGCOVER); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DESCRIPTION')); ?>:</b>
	<?php echo CHtml::encode($data->DESCRIPTION); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GENRE')); ?>:</b>
	<?php echo CHtml::encode($data->GENRE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DATERELEASED')); ?>:</b>
	<?php echo CHtml::encode($data->DATERELEASED); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TOTALTIME')); ?>:</b>
	<?php echo CHtml::encode($data->TOTALTIME); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('DIRECTOR')); ?>:</b>
	<?php echo CHtml::encode($data->DIRECTOR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CAST')); ?>:</b>
	<?php echo CHtml::encode($data->CAST); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('STYPE')); ?>:</b>
	<?php echo CHtml::encode($data->STYPE); ?>
	<br />

	*/ ?>

</div>