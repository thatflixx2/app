<?php
$this->breadcrumbs=array(
	'Broken Links',
);

$this->menu=array(
	array('label'=>'Create BrokenLinks', 'url'=>array('create')),
	array('label'=>'Manage BrokenLinks', 'url'=>array('admin')),
);
?>

<h1>Broken Links</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
