<?php
$this->breadcrumbs=array(
	'Broken Links'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List BrokenLinks', 'url'=>array('index')),
	array('label'=>'Create BrokenLinks', 'url'=>array('create')),
	array('label'=>'View BrokenLinks', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage BrokenLinks', 'url'=>array('admin')),
);
?>

<h1>Update BrokenLinks <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>