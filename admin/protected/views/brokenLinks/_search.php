<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LINK'); ?>
		<?php echo $form->textArea($model,'LINK',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IDMEDIA'); ?>
		<?php echo $form->textField($model,'IDMEDIA'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'USER'); ?>
		<?php echo $form->textArea($model,'USER',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'WHENHAPPENED'); ?>
		<?php echo $form->textField($model,'WHENHAPPENED'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->