<?php
$this->breadcrumbs=array(
	'Broken Links'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BrokenLinks', 'url'=>array('index')),
	array('label'=>'Manage BrokenLinks', 'url'=>array('admin')),
);
?>

<h1>Create BrokenLinks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>