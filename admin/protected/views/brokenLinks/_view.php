<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LINK')); ?>:</b>
	<?php echo CHtml::encode($data->LINK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IDMEDIA')); ?>:</b>
	<?php echo CHtml::encode($data->IDMEDIA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('USER')); ?>:</b>
	<?php echo CHtml::encode($data->USER); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('WHENHAPPENED')); ?>:</b>
	<?php echo CHtml::encode($data->WHENHAPPENED); ?>
	<br />


</div>