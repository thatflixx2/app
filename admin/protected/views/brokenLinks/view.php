<?php
$this->breadcrumbs=array(
	'Broken Links'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List BrokenLinks', 'url'=>array('index')),
	array('label'=>'Create BrokenLinks', 'url'=>array('create')),
	array('label'=>'Update BrokenLinks', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete BrokenLinks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BrokenLinks', 'url'=>array('admin')),
);
?>

<h1>View BrokenLinks #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'LINK',
		'IDMEDIA',
		'USER',
		'WHENHAPPENED',
	),
)); ?>
