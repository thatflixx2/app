<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'broken-links-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'LINK'); ?>
		<?php echo $form->textArea($model,'LINK',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'LINK'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IDMEDIA'); ?>
		<?php echo $form->textField($model,'IDMEDIA'); ?>
		<?php echo $form->error($model,'IDMEDIA'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'USER'); ?>
		<?php echo $form->textArea($model,'USER',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'USER'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'WHENHAPPENED'); ?>
		<?php echo $form->textField($model,'WHENHAPPENED'); ?>
		<?php echo $form->error($model,'WHENHAPPENED'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->