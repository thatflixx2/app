<?php

/**
 * This is the model class for table "EPISODES".
 *
 * The followings are the available columns in table 'EPISODES':
 * @property integer $ID
 * @property integer $SERIE_ID
 * @property integer $SEASON
 * @property integer $EPISODE
 * @property string $NAME
 * @property string $LINK
 * @property string $VIDEOCOVER
 * @property string $VIDEOSOURCE1
 * @property string $VIDEOSOURCE2
 * @property string $VIDEOSOURCE3
 * @property string $VIDEOSOURCEM
 */
class EPISODES extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EPISODES the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EPISODES';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SERIE_ID, SEASON, EPISODE', 'numerical', 'integerOnly'=>true),
			array('NAME', 'length', 'max'=>123),
			array('LINK, VIDEOCOVER', 'length', 'max'=>456),
			array('VIDEOSOURCE1, VIDEOSOURCE2, VIDEOSOURCE3, VIDEOSOURCEM', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, SERIE_ID, SEASON, EPISODE, NAME, LINK, VIDEOCOVER, VIDEOSOURCE1, VIDEOSOURCE2, VIDEOSOURCE3, VIDEOSOURCEM', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'SERIE_ID' => 'Serie',
			'SEASON' => 'Season',
			'EPISODE' => 'Episode',
			'NAME' => 'Name',
			'LINK' => 'Link',
			'VIDEOCOVER' => 'Videocover',
			'VIDEOSOURCE1' => 'Videosource1',
			'VIDEOSOURCE2' => 'Videosource2',
			'VIDEOSOURCE3' => 'Videosource3',
			'VIDEOSOURCEM' => 'Videosourcem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('SERIE_ID',$this->SERIE_ID);
		$criteria->compare('SEASON',$this->SEASON);
		$criteria->compare('EPISODE',$this->EPISODE);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('LINK',$this->LINK,true);
		$criteria->compare('VIDEOCOVER',$this->VIDEOCOVER,true);
		$criteria->compare('VIDEOSOURCE1',$this->VIDEOSOURCE1,true);
		$criteria->compare('VIDEOSOURCE2',$this->VIDEOSOURCE2,true);
		$criteria->compare('VIDEOSOURCE3',$this->VIDEOSOURCE3,true);
		$criteria->compare('VIDEOSOURCEM',$this->VIDEOSOURCEM,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}