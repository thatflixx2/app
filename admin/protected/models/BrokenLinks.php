<?php

/**
 * This is the model class for table "BROKENLINKS".
 *
 * The followings are the available columns in table 'BROKENLINKS':
 * @property integer $ID
 * @property string $LINK
 * @property integer $IDMEDIA
 * @property string $USER
 * @property string $WHENHAPPENED
 */
class BrokenLinks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return BrokenLinks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BROKENLINKS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IDMEDIA', 'numerical', 'integerOnly'=>true),
			array('LINK, USER, WHENHAPPENED', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LINK, IDMEDIA, USER, WHENHAPPENED', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LINK' => 'Link',
			'IDMEDIA' => 'Idmedia',
			'USER' => 'User',
			'WHENHAPPENED' => 'Whenhappened',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LINK',$this->LINK,true);
		$criteria->compare('IDMEDIA',$this->IDMEDIA);
		$criteria->compare('USER',$this->USER,true);
		$criteria->compare('WHENHAPPENED',$this->WHENHAPPENED,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}