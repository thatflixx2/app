<?php

/**
 * This is the model class for table "SERIES".
 *
 * The followings are the available columns in table 'SERIES':
 * @property integer $ID
 * @property string $NAME
 * @property string $IMGCOVER
 * @property string $DESCRIPTION
 * @property string $GENRE
 * @property string $DATERELEASED
 * @property string $TOTALTIME
 * @property string $DIRECTOR
 * @property string $CAST
 * @property integer $STYPE
 */
class SERIES extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SERIES the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SERIES';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('STYPE', 'numerical', 'integerOnly'=>true),
			array('NAME', 'length', 'max'=>123),
			array('IMGCOVER', 'length', 'max'=>60),
			array('GENRE, DIRECTOR', 'length', 'max'=>100),
			array('DATERELEASED', 'length', 'max'=>66),
			array('TOTALTIME', 'length', 'max'=>30),
			array('DESCRIPTION, CAST', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, NAME, IMGCOVER, DESCRIPTION, GENRE, DATERELEASED, TOTALTIME, DIRECTOR, CAST, STYPE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NAME' => 'Name',
			'IMGCOVER' => 'Imgcover',
			'DESCRIPTION' => 'Description',
			'GENRE' => 'Genre',
			'DATERELEASED' => 'Datereleased',
			'TOTALTIME' => 'Totaltime',
			'DIRECTOR' => 'Director',
			'CAST' => 'Cast',
			'STYPE' => 'Stype',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('IMGCOVER',$this->IMGCOVER,true);
		$criteria->compare('DESCRIPTION',$this->DESCRIPTION,true);
		$criteria->compare('GENRE',$this->GENRE,true);
		$criteria->compare('DATERELEASED',$this->DATERELEASED,true);
		$criteria->compare('TOTALTIME',$this->TOTALTIME,true);
		$criteria->compare('DIRECTOR',$this->DIRECTOR,true);
		$criteria->compare('CAST',$this->CAST,true);
		$criteria->compare('STYPE',$this->STYPE);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}